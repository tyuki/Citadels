/*
 * CitadelsApp.java
 */

package citadels;

import citadels.control.Organizer;
import citadels.gui.IconContainer;
import org.jdesktop.application.Application;
import org.jdesktop.application.SingleFrameApplication;
import sound.SoundContainer;

/**
 * The main class of the application.
 */
public class CitadelsApp extends SingleFrameApplication {

    private org.jdesktop.application.ResourceMap _resourceMap;

    private CitadelsView _view;
    private Organizer _organizer;

    /**
     * At startup create and show the main frame of the application.
     */
    @Override
    protected void startup() {
        _resourceMap = org.jdesktop.application.Application.getInstance(citadels.CitadelsApp.class).getContext().getResourceMap(CitadelsApp.class);
        IconContainer.loadDirectory("./images/district/", IconContainer.IMAGE_TYPE.DISTRICT);
        IconContainer.loadDirectory("./images/district/beautified", IconContainer.IMAGE_TYPE.DISTRICT);
        IconContainer.loadDirectory("./images/role/label/", IconContainer.IMAGE_TYPE.ROLE_LABEL);
        IconContainer.loadDirectory("./images/role/card/", IconContainer.IMAGE_TYPE.ROLE_CARD);
        IconContainer.loadDirectory("./images/control/", IconContainer.IMAGE_TYPE.CONTROL);
        IconContainer.loadDirectory("./images/", IconContainer.IMAGE_TYPE.OTHER);

        SoundContainer.addFile("turn", "sound/turn.wav");
        _organizer = new Organizer();
        _view  = new CitadelsView(this, _organizer);
        _organizer.setViewer(_view);
        _organizer.setController(_view);
        show(_view);
    }


    /**
     * This method is to initialize the specified window by injecting resources.
     * Windows shown in our application come fully initialized from the GUI
     * builder, so this additional configuration is not needed.
     */
    @Override protected void configureWindow(java.awt.Window root) {
    }

    /**
     * A convenient static getter for the application instance.
     * @return the instance of CitadelsApp
     */
    public static CitadelsApp getApplication() {
        return Application.getInstance(CitadelsApp.class);
    }

    /**
     * Main method launching the application.
     */
    public static void main(String[] args) {
        launch(CitadelsApp.class, args);
    }
    
}
