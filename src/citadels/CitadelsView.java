/*
 * CitadelsView.java
 */

package citadels;

import citadels.ai.AIContainer;
import citadels.control.CitadelsController;
import citadels.control.CitadelsViewer;
import citadels.control.Organizer;
import citadels.game.Character.CHARACTER;
import citadels.game.Characters;
import citadels.game.Character;
import citadels.game.District;
import citadels.game.District.EFFECT;
import citadels.game.Exceptions.GameException;
import citadels.game.GameOptions;
import citadels.game.GameState;
import citadels.game.GameState.BUILD_QUERY_RESPONSE;
import citadels.game.GameState.DISTRICT_INFLUENCE_RESPONSE;
import citadels.game.Player;
import citadels.game.Progress;
import citadels.gui.PlayerPanel;
import citadels.gui.dialogs.ConnectServerBox;
import citadels.gui.dialogs.DiplomatDialog;
import citadels.gui.dialogs.EmperorTributeDialog;
import citadels.gui.dialogs.GameResultDialog;
import citadels.gui.dialogs.MultipleDistrictSelectionBox;
import citadels.gui.dialogs.MultipleDistrictSelectionBox.MDS_MODE;
import citadels.gui.dialogs.PlayerSelectionBox;
import citadels.gui.dialogs.PlayerSelectionBox.PS_MODE;
import citadels.gui.dialogs.SingleCharacterSelectionBox;
import citadels.gui.dialogs.SingleCharacterSelectionBox.SCS_MODE;
import citadels.gui.dialogs.SingleDistrictSelectionBox;
import citadels.gui.dialogs.SingleDistrictSelectionBox.SDS_MODE;
import citadels.gui.dialogs.StartGameBox;
import citadels.gui.dialogs.StartServerBox;
import org.jdesktop.application.Action;
import org.jdesktop.application.ResourceMap;
import org.jdesktop.application.FrameView;
import org.jdesktop.application.TaskMonitor;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.util.LinkedList;
import java.util.List;
import javax.swing.Timer;
import javax.swing.Icon;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.text.SimpleAttributeSet;
import javax.swing.text.StyleConstants;
import sound.SoundContainer;

/**
 * The application's main frame.
 */
public class CitadelsView extends FrameView implements CitadelsViewer, CitadelsController {

    private org.jdesktop.application.ResourceMap _resourceMap;

    private Organizer _organizer;

    private int _playerID = -1;
    private List<PlayerPanel> _playerPanels;

    private String playerListString = "";

    private StartServerBox _startServerBox = null;
    private StartGameBox _startGameBox = null;
    private ConnectServerBox _connectServerBox = null;
    private SingleCharacterSelectionBox _scsBox = null;
    private SingleDistrictSelectionBox _sdsBox = null;
    private MultipleDistrictSelectionBox _mdsBox = null;
    private PlayerSelectionBox _psBox = null;
    private DiplomatDialog _diplomatDialog = null;

    public CitadelsView(CitadelsApp app, Organizer organizer) {
        super(app);
        initComponents();
        
        //Initializing
        _organizer = organizer;
        _resourceMap = org.jdesktop.application.Application.getInstance(citadels.CitadelsApp.class).getContext().getResourceMap(CitadelsApp.class);

        //startGameMenu.setText(_resourceMap.getString("startGameMenu.text"));
       // startGameMenu.setText("temp");
        
        chatPanel.setView(this);
        rolePanel.setView(this);
        handsPanel.setView(this);
        controlPanel.setView(this);
        
        _playerPanels = new LinkedList<PlayerPanel>();
        _playerPanels.add(playerPanel1);
        _playerPanels.add(playerPanel2);
        _playerPanels.add(playerPanel3);
        _playerPanels.add(playerPanel4);
        _playerPanels.add(playerPanel5);
        _playerPanels.add(playerPanel6);
        _playerPanels.add(playerPanel7);
        _playerPanels.add(playerPanel8);
        for (int i = 0; i < _playerPanels.size(); i++) {
            _playerPanels.get(i).setView(this);
        }

        this.getFrame().setResizable(false);

        // status bar initialization - message timeout, idle icon and busy animation, etc
        ResourceMap resourceMap = getResourceMap();
        int messageTimeout = resourceMap.getInteger("StatusBar.messageTimeout");
        messageTimer = new Timer(messageTimeout, new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                statusMessageLabel.setText("");
            }
        });
        messageTimer.setRepeats(false);
        int busyAnimationRate = resourceMap.getInteger("StatusBar.busyAnimationRate");
        for (int i = 0; i < busyIcons.length; i++) {
            busyIcons[i] = resourceMap.getIcon("StatusBar.busyIcons[" + i + "]");
        }
        busyIconTimer = new Timer(busyAnimationRate, new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                busyIconIndex = (busyIconIndex + 1) % busyIcons.length;
                statusAnimationLabel.setIcon(busyIcons[busyIconIndex]);
            }
        });
        idleIcon = resourceMap.getIcon("StatusBar.idleIcon");
        statusAnimationLabel.setIcon(idleIcon);
        progressBar.setVisible(false);

        // connecting action tasks to status bar via TaskMonitor
        TaskMonitor taskMonitor = new TaskMonitor(getApplication().getContext());
        taskMonitor.addPropertyChangeListener(new java.beans.PropertyChangeListener() {
            public void propertyChange(java.beans.PropertyChangeEvent evt) {
                String propertyName = evt.getPropertyName();
                if ("started".equals(propertyName)) {
                    if (!busyIconTimer.isRunning()) {
                        statusAnimationLabel.setIcon(busyIcons[0]);
                        busyIconIndex = 0;
                        busyIconTimer.start();
                    }
                    progressBar.setVisible(true);
                    progressBar.setIndeterminate(true);
                } else if ("done".equals(propertyName)) {
                    busyIconTimer.stop();
                    statusAnimationLabel.setIcon(idleIcon);
                    progressBar.setVisible(false);
                    progressBar.setValue(0);
                } else if ("message".equals(propertyName)) {
                    String text = (String)(evt.getNewValue());
                    statusMessageLabel.setText((text == null) ? "" : text);
                    messageTimer.restart();
                } else if ("progress".equals(propertyName)) {
                    int value = (Integer)(evt.getNewValue());
                    progressBar.setVisible(true);
                    progressBar.setIndeterminate(false);
                    progressBar.setValue(value);
                }
            }
        });

        this.getFrame().addWindowListener(new WindowListener() {

            public void windowOpened(WindowEvent e) {}
            public void windowClosing(WindowEvent e) {CitadelsApp.getApplication().quit(null);}
            public void windowClosed(WindowEvent e) {}
            public void windowIconified(WindowEvent e) {}
            public void windowDeiconified(WindowEvent e) {}
            public void windowActivated(WindowEvent e) { refreshWindow(); }
            public void windowDeactivated(WindowEvent e) {}
        });

    }

    private void refreshWindow() {
        this.getFrame().repaint();
    }

    @Action
    public void showAboutBox() {
        if (aboutBox == null) {
            JFrame mainFrame = CitadelsApp.getApplication().getMainFrame();
            aboutBox = new CitadelsAboutBox(mainFrame);
            aboutBox.setLocationRelativeTo(mainFrame);
        }
        CitadelsApp.getApplication().show(aboutBox);
    }

    public void reset() {
        _playerID = -1;
        rolePanel.reset();
        startServerMenu.setEnabled(false);
        setGameStartMenues(false);
        joinGameMenu.setEnabled(true);
        resetGameMenu.setEnabled(false);
        addAIMenu.setEnabled(false);
        removeAIMenu.setEnabled(false);
    }


    public void setID(int id) {
        _playerID = id;
    }

    public void setGameState(GameState game) {

        updatePlayers(game.getPlayers(), game.getProgress(), game.getCharacters());
        rolePanel.setCharacters(game.getCharacters());
        if (_playerID >= 0) {
            handsPanel.setHand(game.getPlayer(_playerID).getHand());
        }
        
        if (game.getProgress().ended) {
            JFrame mainFrame = CitadelsApp.getApplication().getMainFrame();
            GameResultDialog grd = new GameResultDialog(mainFrame, false, game, game.getPlayers());
             grd.setLocationRelativeTo(mainFrame);
             CitadelsApp.getApplication().show(grd);
        }
    }

    private void updatePlayers(List<Player> players, Progress progress, Characters characters) {
        boolean firstPlayer = false;
        for (int i = 0; i < players.size(); i++) {
            updatePlayer(players.get(i), progress, characters, i);
            if (players.get(i).getName().compareTo(_organizer.getPlayerName()) == 0) {
                joinGameMenu.setEnabled(false);
                if (i == 0 && !progress.started) {
                    firstPlayer = true;
                }
            }
        }

        if (firstPlayer) {
            removeAIMenu.setEnabled(false);
            setGameStartMenues(true);
            resetGameMenu.setEnabled(true);

            for (Player player : players) {
                if (player.AI) {
                    removeAIMenu.setEnabled(true);
                    break;
                }
            }
            if (players.size() >= 8) {
                addAIMenu.setEnabled(false);
            } else {
                addAIMenu.setEnabled(true);
            }
        }

        for (int i = players.size(); i < _playerPanels.size(); i++) {
            _playerPanels.get(i).setPlayer(null, null, null, -1);

        }
    }

    private void updatePlayer(Player player, Progress progress, Characters characters, int loc) {
        PlayerPanel panel = _playerPanels.get(loc);
        if (panel != null) {
            panel.setPlayer(player, progress, characters, _playerID);
        }
    }

    public void setConnectedNames(List<String> names) {
        StringBuffer sb = new StringBuffer(_resourceMap.getString("control.playerlist") + "<BR>");
        for (String name : names) {
            sb.append(name+"<BR>");
        }
        playerListString = sb.toString();
        controlPanel.setControlPanelDescription(playerListString);
    }

    public void addChatMessage(String text, SimpleAttributeSet attr) {
        chatPanel.addNewLine(text, attr);
    }

    public void errorLog(String msg) {
        System.err.println(msg);
    }

    /**
     * Method that uses the inputs from start server dialog to start a server.
     *
     * @return true if server successfully started
     */
    public boolean startServer() {
        if (_startServerBox == null) return false;

        //Port
        int port = 8080;
        try {
            port = Integer.parseInt(_startServerBox.getPortText());
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, _resourceMap.getString("error.InvalidPort"));
            return false;
        }
        if (port < 0 || port > 65535) {
            JOptionPane.showMessageDialog(null, _resourceMap.getString("error.InvalidPortRange") + " : " + port);
            return false;
        }

        boolean success = _organizer.startServer(port);

        //
//        boolean use9th = _startServerBox.getUse9th();
//        boolean shortenedGame = _startServerBox.getIsShortGame();
//        int[] expansionCards = _startServerBox.getExpansionCards();
//        //EFFECT[] expansionDistricts = _startServerBox.getSelectedExtraDistrictCards()
//
//        boolean success;
//
//        if (_startServerBox.getExtraDistrictCardsSelected()) {
//            success = _organizer.startServer(port, use9th, shortenedGame, expansionCards, _startServerBox.getSelectedExtraDistrictCards());
//        } else {
//            success = _organizer.startServer(port, use9th, shortenedGame, expansionCards, _startServerBox.getNumberOfRandomExtraCards());
//        }

        if (success) {
            SimpleAttributeSet attr = new SimpleAttributeSet();
            StyleConstants.setBold(attr, true);

            chatPanel.addNewLine("Server Started", attr);
            startServerMenu.setEnabled(false);
            setGameStartMenues(false);
            joinGameMenu.setEnabled(true);
            addAIMenu.setEnabled(false);
            return true;
        } else {
            return false;
        }
    }

    public void showConnectServerBox(String playerName, String host, int port) {
        JFrame mainFrame = CitadelsApp.getApplication().getMainFrame();
        _connectServerBox= new ConnectServerBox(this, mainFrame, true, playerName, host, port);
        _connectServerBox.setLocationRelativeTo(mainFrame);
        CitadelsApp.getApplication().show(_connectServerBox);
    }

    /**
     * Method that uses the inputs from connect server dialog to connect to a server.
     *
     * @return true if server successfully started
     */
    public boolean connectServer() {
        if (_connectServerBox == null) return false;

        //Port
        int port = 8080;
        try {
            port = Integer.parseInt(_connectServerBox.getPort());
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, _resourceMap.getString("error.InvalidPort"));
            return false;
        }
        if (port < 0 || port > 65535) {
            JOptionPane.showMessageDialog(null, _resourceMap.getString("error.InvalidPortRange") + " : " + port);
            return false;
        }

        String name = _connectServerBox.getPlayerName();
        String host = _connectServerBox.getHostName();

        if (name.length() < 1 || name.length() > 20) {
            JOptionPane.showMessageDialog(null, _resourceMap.getString("error.InvalidNameRange"));
            return false;
        }

        if (name.matches("^\\s*$")) {
            JOptionPane.showMessageDialog(null, _resourceMap.getString("error.InvalidName"));
            return false;
        }

        if (_organizer.connectServer(name, host, port)) {
            SimpleAttributeSet attr = new SimpleAttributeSet();
            StyleConstants.setBold(attr, true);
            chatPanel.addNewLine("Connected to Server", attr);
            startServerMenu.setEnabled(false);
            connectAsClientMenu.setEnabled(false);
            joinGameMenu.setEnabled(true);
            return true;
        } else {
            return false;
        }
    }

    public boolean startGame() {
        if (_startGameBox == null) return false;

        boolean use9th = _startGameBox.getUse9th();
        boolean shortenedGame = _startGameBox.getIsShortGame();

        //Expansion card
        int[] expansionCards;
        if (_startGameBox.isRandomExpansionCardsSelected()) {
            expansionCards = GameOptions.selectExpansionCharactersRandomly(use9th, _startGameBox.getNumberOfRandomCharacterCards());
        } else {
            expansionCards = _startGameBox.getExpansionCards();
        }

        //Initialize GameOptions based on how random districts are selected
        GameOptions options;
        if (_startGameBox.isRandomDistrictCardsSelected()) {
            int numRandomDistricts = _startGameBox.getNumberOfRandomExtraCards();
            options = new GameOptions(use9th, shortenedGame, expansionCards, numRandomDistricts);
        } else {
            EFFECT[] expansionDistricts = _startGameBox.getSelectedExtraDistrictCards();
            options = new GameOptions(use9th, shortenedGame, expansionCards, expansionDistricts);
        }

        if ( _organizer.startGame(options)) {
            setGameStartMenues(false);
            joinGameMenu.setEnabled(false);
            addAIMenu.setEnabled(false);
            removeAIMenu.setEnabled(false);
            return true;
        } else {
            return false;
        }
    }
    
    private static enum PREDEFINED_GAMES {
        RANDOM_DISTRICTS,
        RANDOM_DISTRICTS_WITH_ONE_EXPANSION_CHARACTER,
        RANDOM_DISTRICTS_WITH_TWO_EXPANSION_CHARACTERS
                
    }

    public void startPredefinedGame(PREDEFINED_GAMES game) {

        boolean use9th = _organizer.getCurrentGameState().getPlayers().size() >= 8;
        boolean shortenedGame = false;

        final int numExpansionCards;
        final int numDistrictCards;
        switch (game) {
            case RANDOM_DISTRICTS:
                numExpansionCards = 0;
                numDistrictCards = 3;
                break;
            case RANDOM_DISTRICTS_WITH_ONE_EXPANSION_CHARACTER:
                numExpansionCards = 1;
                numDistrictCards = 3;
                break;
            case RANDOM_DISTRICTS_WITH_TWO_EXPANSION_CHARACTERS:
                numExpansionCards = 2;
                numDistrictCards = 3;
                break;
            default:
                throw new RuntimeException("Unexpected game setting");
        }
        //Expansion card
        int[] expansionCards = GameOptions.selectExpansionCharactersRandomly(use9th, numExpansionCards);

        //Initialize GameOptions based on how random districts are selected
        GameOptions options = new GameOptions(use9th, shortenedGame, expansionCards, numDistrictCards);

        if ( _organizer.startGame(options)) {
            setGameStartMenues(false);
            joinGameMenu.setEnabled(false);
            addAIMenu.setEnabled(false);
            removeAIMenu.setEnabled(false);
        }
    }


    private void setGameStartMenues(boolean enabled) {
            startGameMenu.setEnabled(enabled);
            predefinedGamesMenu.setEnabled(enabled);
            randomDistrictsSubMenu.setEnabled(enabled);
            randomOneCharacterAndDistrictsSubMenu.setEnabled(enabled);
            randomTwoCharactersAndDistrictsSubMenu.setEnabled(enabled);
    }

    public void sendChatMessage(String msg) {
        _organizer.sendChatMessage(msg);
    }

    public void setControlPanelDescription(String text) {
        this.controlPanel.setControlPanelDescription(text);
    }


    public void selectCharacter(Characters characters) {
        SoundContainer.playFile("turn", 1.0f);
        JFrame mainFrame = CitadelsApp.getApplication().getMainFrame();
        _scsBox = new SingleCharacterSelectionBox(this, mainFrame, false, SingleCharacterSelectionBox.SCS_MODE.SELECTION, characters);
        _scsBox.setLocationRelativeTo(mainFrame);
        CitadelsApp.getApplication().show(_scsBox);
    }

    public void characterSelected(int c) {
        _organizer.characterSelected(c);
    }

    public void selectNewKing(List<Player> players) {
        SoundContainer.playFile("turn", 1.0f);
        JFrame mainFrame = CitadelsApp.getApplication().getMainFrame();
        _psBox = new PlayerSelectionBox(this, mainFrame, false, PS_MODE.EMPEROR, _organizer.getCurrentGameState().getPlayers(), _playerID);
        _psBox.setLocationRelativeTo(mainFrame);
        CitadelsApp.getApplication().show(_psBox);
    }

    public void selectTributeToEmperor(Player player) {
        SoundContainer.playFile("turn", 1.0f);
        JFrame mainFrame = CitadelsApp.getApplication().getMainFrame();
        EmperorTributeDialog etBox = new EmperorTributeDialog(this, mainFrame, true, player.getHand(), player.getGold() > 0);
        etBox.setLocationRelativeTo(mainFrame);
        CitadelsApp.getApplication().show(etBox);
    }

    public void emperorTributeSelectedCard(int cardNum) {
        _organizer.emperorTributeCard(cardNum);
    }

    public void emperorTributeSelectedGold() {
        _organizer.emperorTributeGold();
    }

    public void selectGraveyard(District district) {
        SoundContainer.playFile("turn", 1.0f);

        String districtAbility = "";
        if (district.getEffect() != EFFECT.NONE) {
            districtAbility = "\n" + _resourceMap.getString("district."+district.getName()+".effect");
        }

        //If there is enough money, check if player wants to build
        int res = JOptionPane.showConfirmDialog(null, _resourceMap.getString("dialog.message.confirmGraveyard") + "\n" +
                        _resourceMap.getString("term.district") + " : " + _resourceMap.getString("district."+district.getName()+".name") + "\n" +
                        _resourceMap.getString("term.cost") + " : " + district.getCost() + districtAbility,
                        _resourceMap.getString("dialog.message.confirmGraveyard.title"), JOptionPane.YES_NO_OPTION, JOptionPane.PLAIN_MESSAGE);

       _organizer.graveyardAbility(res == JOptionPane.YES_OPTION);
    }

    public void updateControl(Progress progress, Character character, Player player, boolean action, boolean build, boolean end, boolean[] SAs, boolean distIncome, boolean[] DAs) {
        //Clear all dialogs
        if (_scsBox != null) _scsBox.dispose();
        if (_sdsBox != null) _sdsBox.dispose();
        if (_mdsBox != null) _mdsBox.dispose();
        if (_psBox != null) _psBox.dispose();
        if (_diplomatDialog != null) {_diplomatDialog.dispose(); _diplomatDialog = null;}
        
        //Play sound if this is the first time
        if (_organizer.getCurrentGameState().isFirstTurn(_playerID)) {
            SoundContainer.playFile("turn", 1.0f);
        }

        controlPanel.updateControl(character, player, action, build, end, SAs, distIncome, DAs);
    }

    public void actionCoinsSelected() {
        _organizer.actionCoins();
    }

    public void actionCardSelected() {
        JFrame mainFrame = CitadelsApp.getApplication().getMainFrame();
        List<District> districts = _organizer.getCurrentGameState().getActionCardsChoice(_playerID);
        int keep = _organizer.getCurrentGameState().getActionCardsKeepNum(_playerID);

        if (keep >= districts.size()) {
            //Keep all, so no selection necessary
            int cards[] = new int[keep];
            for (int i = 0; i < cards.length; i++) {
                cards[i] = i;
            }
            _organizer.actionCards(cards);
        } else if (keep == 1) {
            _sdsBox = new SingleDistrictSelectionBox(this, mainFrame, true, SDS_MODE.ACTION_CARDS, districts);
            _sdsBox.setLocationRelativeTo(mainFrame);
            CitadelsApp.getApplication().show(_sdsBox);
        } else if (keep == 2) {
            _mdsBox = new MultipleDistrictSelectionBox(this, mainFrame, false, MDS_MODE.ACTION_CARDS, districts);
            _mdsBox.setLocationRelativeTo(mainFrame);
            CitadelsApp.getApplication().show(_mdsBox);
        }
    }

    public void actionCardsSelected(int[] cards) {
            _organizer.actionCards(cards);
    }

    public void turnEndSelected() {
        _organizer.turnEnd();
    }

    public void districtIncomeSelected() {
        _organizer.earnDistrictIncome();
    }

    public void specialAbility1Selected() {
        CHARACTER character = _organizer.getCurrentGameState().getCurrentCharacter().character;

        if (character == null) return;

        JFrame mainFrame = CitadelsApp.getApplication().getMainFrame();
        SCS_MODE scsMode = null;

        switch(character) {
            case ASSASSIN:
                scsMode = SCS_MODE.ASSASSINATE;
            case THIEF:
                if (scsMode == null) scsMode = SCS_MODE.STEAL;
            case WITCH:
                if (scsMode == null) scsMode = SCS_MODE.BEWITCH;
                _scsBox = new SingleCharacterSelectionBox(this, mainFrame, false, scsMode, _organizer.getCurrentGameState().getCharacters());
                _scsBox.setLocationRelativeTo(mainFrame);
                CitadelsApp.getApplication().show(_scsBox);
                break;
            case MAGICIAN:
                _psBox = new PlayerSelectionBox(this, mainFrame, false, PS_MODE.MAGICIAN, _organizer.getCurrentGameState().getPlayers(), _playerID);
                _psBox.setLocationRelativeTo(mainFrame);
                CitadelsApp.getApplication().show(_psBox);
                break;
            case WIZARD:
                _psBox = new PlayerSelectionBox(this, mainFrame, false, PS_MODE.WIZARD, _organizer.getCurrentGameState().getPlayers(), _playerID);
                _psBox.setLocationRelativeTo(mainFrame);
                CitadelsApp.getApplication().show(_psBox);
                break;
            case ABBOT:
                _organizer.abbotAbility();
                break;
            case NAVIGATOR:
                _organizer.navigatorAbilityGold();
                break;
            case ARTIST:
                break;

        }
    }

    public void specialAbility2Selected() {

        CHARACTER character = _organizer.getCurrentGameState().getCurrentCharacter().character;

        if (character == null) return;

        JFrame mainFrame = CitadelsApp.getApplication().getMainFrame();
         
        switch(character) {
            case MAGICIAN:
                 _mdsBox = new MultipleDistrictSelectionBox(this, mainFrame, false, MDS_MODE.MAGICIAN, _organizer.getCurrentGameState().getPlayer(_playerID).getHand());
                 _mdsBox.setLocationRelativeTo(mainFrame);
                 CitadelsApp.getApplication().show(_mdsBox);
                break;
            case NAVIGATOR:
                _organizer.navigatorAbilityCards();
                break;
        }
    }

    public void assassinateSelected(int target) {
        _organizer.assassinAbility(target);
    }

    public void stealSelected(int target) {
        _organizer.thiefAbility(target);
    }
    
    public void bewitchSelected(int target) {
        _organizer.witchAbility(target);
    }

    public void magicianSwapSelected(int targetPlayer) {
        _organizer.magicianAbilitySwap(targetPlayer);
    }

    public void magicianExchangeSelected(int[] cards) {
        _organizer.magicianAbilityExchange(cards);
    }

    public void kingSelected(int targetPlayer) {
        _organizer.emperorAbilityKingSelected(targetPlayer);
    }

    public void wizardTargetPlayerSelected(int targetPlayerID) {
        JFrame mainFrame = CitadelsApp.getApplication().getMainFrame();
        _sdsBox = new SingleDistrictSelectionBox(this, mainFrame, true, SDS_MODE.WIZARD, _organizer.getCurrentGameState().getPlayer(targetPlayerID).getHand(), targetPlayerID);
        _sdsBox.setLocationRelativeTo(mainFrame);
        CitadelsApp.getApplication().show(_sdsBox);
    }

    public void wizardTargetCardSelected(int targetPlayerID, int targetDistrict) {
        District district = _organizer.getCurrentGameState().getPlayer(targetPlayerID).getHand().get(targetDistrict);
        boolean enoughMoney = _organizer.getCurrentGameState().getPlayer(_playerID).getGold() >= district.getCost();

        if (enoughMoney) {
                //If there is enough money, check if player wants to build
                int buildRes = JOptionPane.showConfirmDialog(null, _resourceMap.getString("dialog.message.confirmWizardBuild") + "\n" +
                        _resourceMap.getString("term.district") + " : " + _resourceMap.getString("district."+district.getName()+".name") + "\n" +
                        _resourceMap.getString("term.cost") + " : " + district.getCost() + "\n" +
                        _resourceMap.getString("dialog.message.confirmWizarBuild.notice"),
                        _resourceMap.getString("dialog.message.confirmWizardBuild.title"), JOptionPane.YES_NO_OPTION, JOptionPane.PLAIN_MESSAGE);
                
            boolean build = buildRes == JOptionPane.YES_OPTION;
            _organizer.wizardAbility(targetPlayerID, targetDistrict, build);
        } else {
            _organizer.wizardAbility(targetPlayerID, targetDistrict, false);
        }

    }



    public void buildDistrictSelected(int num) {

        BUILD_QUERY_RESPONSE resp = _organizer.getCurrentGameState().canBulildDistrict(_playerID, num);

        switch (resp) {
            case NOT_YOUR_TURN:
                break;
            case ACTION_NOT_TAKEN:
                JOptionPane.showMessageDialog(null, _resourceMap.getString("dialog.message.actionNotTaken"));
                break;
            case INSUFFICIENT_GOLD:
                JOptionPane.showMessageDialog(null, _resourceMap.getString("dialog.message.insufficientGold"));
                break;
            case DUPLICATE_DISTRICT:
                JOptionPane.showMessageDialog(null, _resourceMap.getString("dialog.message.duplicateDistrict"));
                break;
            case BUILDING_LIMIT:
                JOptionPane.showMessageDialog(null, _resourceMap.getString("dialog.message.buildingLimit"));
                break;
            case CAN_BUILD:
                _organizer.buildDistrict(num);
                break;
            case CAN_BUILD_LIGHTHOUSE:
                JFrame mainFrame = CitadelsApp.getApplication().getMainFrame();
                _sdsBox = new SingleDistrictSelectionBox(this, mainFrame, true, SDS_MODE.LIGHTHOUSE, _organizer.getCurrentGameState().getDeck().getAllCards(), num);
                _sdsBox.setLocationRelativeTo(mainFrame);
                 CitadelsApp.getApplication().show(_sdsBox);
                break;
            case CAN_BUILD_BELLTOWER:
                int bellTower = JOptionPane.showConfirmDialog(null, _resourceMap.getString("dialog.message.confirmBellTower"),
                        _resourceMap.getString("dialog.message.confirmBellTower.title"), JOptionPane.YES_NO_OPTION, JOptionPane.PLAIN_MESSAGE);
                if (bellTower == JOptionPane.YES_OPTION) {
                    _organizer.belltowerAbility(num);
                    return;
                } else {
                    _organizer.buildDistrict(num);
                }
                break;

        }
    }

    public void districtSelected(int targetPlayer, int targetDistrict) {
        //Get what can be done to that district
        DISTRICT_INFLUENCE_RESPONSE res = _organizer.getCurrentGameState().canInfluenceDistrict(_playerID, targetPlayer, targetDistrict);
        //Check for museum, double check the playerID
        boolean museumUsable = _organizer.getCurrentGameState().isThisUsableMuseum(targetPlayer, targetDistrict) && _playerID == targetPlayer;
        //Check for armory
        boolean canUseArmory = _organizer.getCurrentGameState().canUseArmory(_playerID, targetPlayer, targetDistrict);

        District district = _organizer.getCurrentGameState().getPlayer(targetPlayer).getCity().get(targetDistrict);

        switch (res) {
            case KEEP:
                if (!canUseArmory) JOptionPane.showMessageDialog(null, _resourceMap.getString("dialog.message.keep"));
                break;
            case PROTECTED:
                if (!canUseArmory) JOptionPane.showMessageDialog(null, _resourceMap.getString("dialog.message.protected"));
                break;
            case CITY_COMPLETED:
                JOptionPane.showMessageDialog(null, _resourceMap.getString("dialog.message.cityCompleted"));
                break;
            case NO_GOLD_TO_DESTROY:
                if (!canUseArmory) JOptionPane.showMessageDialog(null, _resourceMap.getString("dialog.message.noGoldToDestroy"));
                break;
            case DESTROY:
                int destroyRes = JOptionPane.showConfirmDialog(null, _resourceMap.getString("dialog.message.confirmDestroy") + "\n" +
                        _resourceMap.getString("term.district") + " : " + _resourceMap.getString("district."+district.getName()+".name") + "\n" +
                        _resourceMap.getString("term.cost") + " : " + _organizer.getCurrentGameState().computeCostToDestroy(targetPlayer, targetDistrict) + "\n" +
                        _resourceMap.getString("dialog.message.confirmDestroy.warning"),
                        _resourceMap.getString("dialog.message.confirmDestroy.title"), JOptionPane.YES_NO_OPTION, JOptionPane.PLAIN_MESSAGE);
                if (destroyRes == JOptionPane.YES_OPTION) {
                    _organizer.warlordAbility(targetPlayer, targetDistrict);
                    return;
                }
                break;
            case BEAUTIFY_LIMIT:
                if (!museumUsable) JOptionPane.showMessageDialog(null, _resourceMap.getString("dialog.message.beautifyLimit"));
                break;
            case ALREADY_BEAUTIFIED:
                if (!museumUsable) JOptionPane.showMessageDialog(null, _resourceMap.getString("dialog.message.alreadyBeautified"));
                break;
            case NO_GOLD_TO_BEAUTIFY:
                if (!museumUsable) JOptionPane.showMessageDialog(null, _resourceMap.getString("dialog.message.noGoldToBeautify"));
                break;
            case BEAUTIFY:
                int beautifyRes = JOptionPane.showConfirmDialog(null, _resourceMap.getString("dialog.message.confirmBeautify") + "\n" +
                        _resourceMap.getString("term.district") + " : " + _resourceMap.getString("district."+district.getName()+".name") + "\n" +
                        _resourceMap.getString("dialog.message.confirmBeautify.notice"),
                        _resourceMap.getString("dialog.message.confirmBeautify.title"), JOptionPane.YES_NO_OPTION, JOptionPane.PLAIN_MESSAGE);
                if (beautifyRes == JOptionPane.YES_OPTION) {
                    _organizer.artistAbility(targetPlayer, targetDistrict);
                    return;
                }
                break;
            case EXCHANGE_CANDIDATE_SELF:
                if (_diplomatDialog == null) {
                    JFrame mainFrame = CitadelsApp.getApplication().getMainFrame();
                    _diplomatDialog = new DiplomatDialog(this, mainFrame, false);
                    _diplomatDialog.setLocationRelativeTo(mainFrame);
                    CitadelsApp.getApplication().show(_diplomatDialog);
                }
                _diplomatDialog.setSelfDistrict(_organizer.getCurrentGameState(), targetPlayer, targetDistrict);
                break;
            case EXCHANGE_CANDIDATE_OTHER:
                if (_diplomatDialog == null) {
                    JFrame mainFrame = CitadelsApp.getApplication().getMainFrame();
                    _diplomatDialog = new DiplomatDialog(this, mainFrame, false);
                    _diplomatDialog.setLocationRelativeTo(mainFrame);
                    CitadelsApp.getApplication().show(_diplomatDialog);
                }
                _diplomatDialog.setOtherDistrict(_organizer.getCurrentGameState(), targetPlayer, targetDistrict);
                break;
            case EXCHANGE_NO_OWN_DISTRICT:
                JOptionPane.showMessageDialog(null, _resourceMap.getString("dialog.message.exchangeNoOwnDistrict"));
                break;
            case EXCHANGE_DUPLICATE:
                if (!canUseArmory) JOptionPane.showMessageDialog(null, _resourceMap.getString("dialog.message.exchangeDuplicate"));
                break;
        }

        //If museum can be used, ask for that as well
        if (museumUsable) {
            JFrame mainFrame = CitadelsApp.getApplication().getMainFrame();
                _sdsBox = new SingleDistrictSelectionBox(this, mainFrame, true, SDS_MODE.MUSEUM, _organizer.getCurrentGameState().getPlayer(_playerID).getHand(), targetDistrict);
                _sdsBox.setLocationRelativeTo(mainFrame);
                 CitadelsApp.getApplication().show(_sdsBox);
        }

        //Check for Armory
        if (canUseArmory) {
            int armoryRes = JOptionPane.showConfirmDialog(null, _resourceMap.getString("dialog.message.confirmArmory") + "\n" +
                        _resourceMap.getString("term.district") + " : " + _resourceMap.getString("district."+district.getName()+".name") + "\n",
                        _resourceMap.getString("dialog.message.confirmArmory.title"), JOptionPane.YES_NO_OPTION, JOptionPane.PLAIN_MESSAGE);
            if (armoryRes == JOptionPane.YES_OPTION) {
                _organizer.armoryAbility(targetPlayer, targetDistrict);
            }
        }
    }

    public void diplomatAbilitySelected(int districtIndex, int otherPlayerID, int otherDistrictIndex) {
        _organizer.diplomatAbility(districtIndex, otherPlayerID, otherDistrictIndex);
        diplomatDialogClosed();
    }

    public void diplomatDialogClosed() {
        _diplomatDialog.dispose();
        _diplomatDialog = null;
    }

    public void smithySelected() {
        _organizer.smithyAbility();
    }

    public void laboratorySelected() {
        JFrame mainFrame = CitadelsApp.getApplication().getMainFrame();
        _sdsBox = new SingleDistrictSelectionBox(this, mainFrame, true, SDS_MODE.LABORATORY, _organizer.getCurrentGameState().getPlayer(_playerID).getHand());
        _sdsBox.setLocationRelativeTo(mainFrame);
         CitadelsApp.getApplication().show(_sdsBox);
    }

    public void laboratorySelected(int discard) {
        _organizer.laboratoryAbility(discard);
    }


    public void lighthouseSelected(int build, int take) {
        _organizer.lighthouseAbility(build, take);
    }

    public void museumSelected(int museum, int discard) {
        _organizer.museumAbility(museum, discard);
    }

    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        mainPanel = new javax.swing.JPanel();
        handsPanel = new citadels.gui.HandsPanel();
        playerPanel1 = new citadels.gui.PlayerPanel();
        playerPanel2 = new citadels.gui.PlayerPanel();
        playerPanel3 = new citadels.gui.PlayerPanel();
        playerPanel4 = new citadels.gui.PlayerPanel();
        playerPanel5 = new citadels.gui.PlayerPanel();
        playerPanel6 = new citadels.gui.PlayerPanel();
        playerPanel7 = new citadels.gui.PlayerPanel();
        playerPanel8 = new citadels.gui.PlayerPanel();
        controlPanel = new citadels.gui.ControlPanel();
        chatPanel = new citadels.gui.ChatPanel();
        rolePanel = new citadels.gui.RolePanel();
        menuBar = new javax.swing.JMenuBar();
        javax.swing.JMenu gameMenu = new javax.swing.JMenu();
        startServerMenu = new javax.swing.JMenuItem();
        connectAsClientMenu = new javax.swing.JMenuItem();
        jSeparator1 = new javax.swing.JPopupMenu.Separator();
        joinGameMenu = new javax.swing.JMenuItem();
        startGameMenu = new javax.swing.JMenuItem();
        predefinedGamesMenu = new javax.swing.JMenu();
        randomDistrictsSubMenu = new javax.swing.JMenuItem();
        randomOneCharacterAndDistrictsSubMenu = new javax.swing.JMenuItem();
        randomTwoCharactersAndDistrictsSubMenu = new javax.swing.JMenuItem();
        resetGameMenu = new javax.swing.JMenuItem();
        jSeparator2 = new javax.swing.JPopupMenu.Separator();
        addAIMenu = new javax.swing.JMenuItem();
        removeAIMenu = new javax.swing.JMenuItem();
        jSeparator3 = new javax.swing.JPopupMenu.Separator();
        javax.swing.JMenuItem exitMenuItem = new javax.swing.JMenuItem();
        javax.swing.JMenu helpMenu = new javax.swing.JMenu();
        javax.swing.JMenuItem aboutMenuItem = new javax.swing.JMenuItem();
        statusPanel = new javax.swing.JPanel();
        javax.swing.JSeparator statusPanelSeparator = new javax.swing.JSeparator();
        statusMessageLabel = new javax.swing.JLabel();
        statusAnimationLabel = new javax.swing.JLabel();
        progressBar = new javax.swing.JProgressBar();

        mainPanel.setMaximumSize(new java.awt.Dimension(900, 600));
        mainPanel.setMinimumSize(new java.awt.Dimension(600, 600));
        mainPanel.setName("mainPanel"); // NOI18N
        mainPanel.setPreferredSize(new java.awt.Dimension(650, 600));

        handsPanel.setName("handsPanel"); // NOI18N
        handsPanel.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                handsPanelMouseEntered(evt);
            }
        });

        playerPanel1.setName("playerPanel1"); // NOI18N

        playerPanel2.setName("playerPanel2"); // NOI18N

        playerPanel3.setName("playerPanel3"); // NOI18N

        playerPanel4.setName("playerPanel4"); // NOI18N

        playerPanel5.setName("playerPanel5"); // NOI18N

        playerPanel6.setName("playerPanel6"); // NOI18N

        playerPanel7.setName("playerPanel7"); // NOI18N

        playerPanel8.setName("playerPanel8"); // NOI18N

        controlPanel.setName("controlPanel"); // NOI18N
        controlPanel.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                controlPanelMouseEntered(evt);
            }
        });

        chatPanel.setName("chatPanel"); // NOI18N
        chatPanel.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                chatPanelMouseEntered(evt);
            }
        });

        rolePanel.setName("rolePanel"); // NOI18N

        javax.swing.GroupLayout mainPanelLayout = new javax.swing.GroupLayout(mainPanel);
        mainPanel.setLayout(mainPanelLayout);
        mainPanelLayout.setHorizontalGroup(
            mainPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(mainPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(mainPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(mainPanelLayout.createSequentialGroup()
                        .addGroup(mainPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addGroup(mainPanelLayout.createSequentialGroup()
                                .addComponent(playerPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(playerPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(playerPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(playerPanel4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(mainPanelLayout.createSequentialGroup()
                                .addComponent(playerPanel5, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(playerPanel6, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(playerPanel7, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(playerPanel8, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(rolePanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(mainPanelLayout.createSequentialGroup()
                        .addGroup(mainPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(chatPanel, javax.swing.GroupLayout.DEFAULT_SIZE, 525, Short.MAX_VALUE)
                            .addComponent(handsPanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 8, Short.MAX_VALUE)
                        .addComponent(controlPanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        mainPanelLayout.setVerticalGroup(
            mainPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(mainPanelLayout.createSequentialGroup()
                .addGap(14, 14, 14)
                .addGroup(mainPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(mainPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                        .addGroup(javax.swing.GroupLayout.Alignment.LEADING, mainPanelLayout.createSequentialGroup()
                            .addComponent(playerPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(playerPanel5, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGroup(javax.swing.GroupLayout.Alignment.LEADING, mainPanelLayout.createSequentialGroup()
                            .addComponent(playerPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(playerPanel6, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGroup(javax.swing.GroupLayout.Alignment.LEADING, mainPanelLayout.createSequentialGroup()
                            .addComponent(playerPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(playerPanel7, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGroup(mainPanelLayout.createSequentialGroup()
                            .addComponent(playerPanel4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(playerPanel8, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addComponent(rolePanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(8, 8, 8)
                .addGroup(mainPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(mainPanelLayout.createSequentialGroup()
                        .addComponent(handsPanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(chatPanel, javax.swing.GroupLayout.DEFAULT_SIZE, 124, Short.MAX_VALUE))
                    .addComponent(controlPanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(6, 6, 6))
        );

        menuBar.setName("menuBar"); // NOI18N

        org.jdesktop.application.ResourceMap resourceMap = org.jdesktop.application.Application.getInstance(citadels.CitadelsApp.class).getContext().getResourceMap(CitadelsView.class);
        gameMenu.setText(resourceMap.getString("menu.gameMenu")); // NOI18N
        gameMenu.setName("gameMenu"); // NOI18N

        startServerMenu.setText(resourceMap.getString("menu.startServerMenu")); // NOI18N
        startServerMenu.setName("startServerMenu"); // NOI18N
        startServerMenu.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                startServerMenuActionPerformed(evt);
            }
        });
        gameMenu.add(startServerMenu);

        connectAsClientMenu.setText(resourceMap.getString("menu.connectAsClientMenu")); // NOI18N
        connectAsClientMenu.setActionCommand(resourceMap.getString("connectAsClientMenu.actionCommand")); // NOI18N
        connectAsClientMenu.setName("connectAsClientMenu"); // NOI18N
        connectAsClientMenu.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                connectAsClientMenuActionPerformed(evt);
            }
        });
        gameMenu.add(connectAsClientMenu);

        jSeparator1.setName("jSeparator1"); // NOI18N
        gameMenu.add(jSeparator1);

        joinGameMenu.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_J, java.awt.event.InputEvent.CTRL_MASK));
        joinGameMenu.setText(resourceMap.getString("menu.joinGameMenu")); // NOI18N
        joinGameMenu.setEnabled(false);
        joinGameMenu.setName("joinGameMenu"); // NOI18N
        joinGameMenu.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                joinGameMenuActionPerformed(evt);
            }
        });
        gameMenu.add(joinGameMenu);

        startGameMenu.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_S, java.awt.event.InputEvent.CTRL_MASK));
        startGameMenu.setText(resourceMap.getString("menu.startGameMenu")); // NOI18N
        startGameMenu.setEnabled(false);
        startGameMenu.setName("startGameMenu"); // NOI18N
        startGameMenu.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                startGameMenuActionPerformed(evt);
            }
        });
        gameMenu.add(startGameMenu);

        predefinedGamesMenu.setText(resourceMap.getString("menu.predefinedGamesMenu")); // NOI18N
        predefinedGamesMenu.setEnabled(false);
        predefinedGamesMenu.setName("predefinedGamesMenu"); // NOI18N

        randomDistrictsSubMenu.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_S, java.awt.event.InputEvent.SHIFT_MASK | java.awt.event.InputEvent.CTRL_MASK));
        randomDistrictsSubMenu.setText(resourceMap.getString("menu.randomDistrictsSubMenu")); // NOI18N
        randomDistrictsSubMenu.setEnabled(false);
        randomDistrictsSubMenu.setName("randomDistrictsSubMenu"); // NOI18N
        randomDistrictsSubMenu.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                randomDistrictsSubMenuActionPerformed(evt);
            }
        });
        predefinedGamesMenu.add(randomDistrictsSubMenu);

        randomOneCharacterAndDistrictsSubMenu.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_1, java.awt.event.InputEvent.SHIFT_MASK | java.awt.event.InputEvent.CTRL_MASK));
        randomOneCharacterAndDistrictsSubMenu.setText(resourceMap.getString("menu.randomOneCharacterAndDistrictsSubMenu")); // NOI18N
        randomOneCharacterAndDistrictsSubMenu.setEnabled(false);
        randomOneCharacterAndDistrictsSubMenu.setName("randomOneCharacterAndDistrictsSubMenu"); // NOI18N
        randomOneCharacterAndDistrictsSubMenu.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                randomOneCharacterAndDistrictsSubMenuActionPerformed(evt);
            }
        });
        predefinedGamesMenu.add(randomOneCharacterAndDistrictsSubMenu);

        randomTwoCharactersAndDistrictsSubMenu.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_2, java.awt.event.InputEvent.SHIFT_MASK | java.awt.event.InputEvent.CTRL_MASK));
        randomTwoCharactersAndDistrictsSubMenu.setText(resourceMap.getString("menu.randomTwoCharactersAndDistrictsSubMenu")); // NOI18N
        randomTwoCharactersAndDistrictsSubMenu.setEnabled(false);
        randomTwoCharactersAndDistrictsSubMenu.setName("randomTwoCharactersAndDistrictsSubMenu"); // NOI18N
        randomTwoCharactersAndDistrictsSubMenu.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                randomTwoCharactersAndDistrictsSubMenuActionPerformed(evt);
            }
        });
        predefinedGamesMenu.add(randomTwoCharactersAndDistrictsSubMenu);

        gameMenu.add(predefinedGamesMenu);

        resetGameMenu.setText(resourceMap.getString("menu.resetGameMenu")); // NOI18N
        resetGameMenu.setEnabled(false);
        resetGameMenu.setName("resetGameMenu"); // NOI18N
        resetGameMenu.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                resetGameMenuActionPerformed(evt);
            }
        });
        gameMenu.add(resetGameMenu);

        jSeparator2.setName("jSeparator2"); // NOI18N
        gameMenu.add(jSeparator2);

        addAIMenu.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_Z, java.awt.event.InputEvent.CTRL_MASK));
        addAIMenu.setText(resourceMap.getString("menu.addAIMenu")); // NOI18N
        addAIMenu.setEnabled(false);
        addAIMenu.setName("addAIMenu"); // NOI18N
        addAIMenu.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                addAIMenuActionPerformed(evt);
            }
        });
        gameMenu.add(addAIMenu);

        removeAIMenu.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_X, java.awt.event.InputEvent.CTRL_MASK));
        removeAIMenu.setText(resourceMap.getString("menu.removeAIMenu")); // NOI18N
        removeAIMenu.setEnabled(false);
        removeAIMenu.setName("removeAIMenu"); // NOI18N
        removeAIMenu.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                removeAIMenuActionPerformed(evt);
            }
        });
        gameMenu.add(removeAIMenu);

        jSeparator3.setName("jSeparator3"); // NOI18N
        gameMenu.add(jSeparator3);

        javax.swing.ActionMap actionMap = org.jdesktop.application.Application.getInstance(citadels.CitadelsApp.class).getContext().getActionMap(CitadelsView.class, this);
        exitMenuItem.setAction(actionMap.get("quit")); // NOI18N
        exitMenuItem.setText(resourceMap.getString("menu.exitMenuItem")); // NOI18N
        exitMenuItem.setName("exitMenuItem"); // NOI18N
        gameMenu.add(exitMenuItem);

        menuBar.add(gameMenu);

        helpMenu.setText(resourceMap.getString("menu.helpMenu")); // NOI18N
        helpMenu.setName("helpMenu"); // NOI18N

        aboutMenuItem.setAction(actionMap.get("showAboutBox")); // NOI18N
        aboutMenuItem.setText(resourceMap.getString("menu.aboutMenu")); // NOI18N
        aboutMenuItem.setName("aboutMenuItem"); // NOI18N
        helpMenu.add(aboutMenuItem);

        menuBar.add(helpMenu);

        statusPanel.setName("statusPanel"); // NOI18N

        statusPanelSeparator.setName("statusPanelSeparator"); // NOI18N

        statusMessageLabel.setName("statusMessageLabel"); // NOI18N

        statusAnimationLabel.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        statusAnimationLabel.setName("statusAnimationLabel"); // NOI18N

        progressBar.setName("progressBar"); // NOI18N

        javax.swing.GroupLayout statusPanelLayout = new javax.swing.GroupLayout(statusPanel);
        statusPanel.setLayout(statusPanelLayout);
        statusPanelLayout.setHorizontalGroup(
            statusPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(statusPanelSeparator, javax.swing.GroupLayout.DEFAULT_SIZE, 900, Short.MAX_VALUE)
            .addGroup(statusPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(statusMessageLabel)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 725, Short.MAX_VALUE)
                .addComponent(progressBar, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(statusAnimationLabel)
                .addContainerGap())
        );
        statusPanelLayout.setVerticalGroup(
            statusPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(statusPanelLayout.createSequentialGroup()
                .addComponent(statusPanelSeparator, javax.swing.GroupLayout.PREFERRED_SIZE, 2, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(statusPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(statusMessageLabel)
                    .addComponent(statusAnimationLabel)
                    .addComponent(progressBar, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(3, 3, 3))
        );

        setComponent(mainPanel);
        setMenuBar(menuBar);
        setStatusBar(statusPanel);
    }// </editor-fold>//GEN-END:initComponents

    private void startServerMenuActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_startServerMenuActionPerformed
        JFrame mainFrame = CitadelsApp.getApplication().getMainFrame();
        _startServerBox = new StartServerBox(this, mainFrame, true);
        _startServerBox.setLocationRelativeTo(mainFrame);
        CitadelsApp.getApplication().show(_startServerBox);
    }//GEN-LAST:event_startServerMenuActionPerformed

    private void connectAsClientMenuActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_connectAsClientMenuActionPerformed
        JFrame mainFrame = CitadelsApp.getApplication().getMainFrame();
        _connectServerBox= new ConnectServerBox(this, mainFrame, true);
        _connectServerBox.setLocationRelativeTo(mainFrame);
        CitadelsApp.getApplication().show(_connectServerBox);
    }//GEN-LAST:event_connectAsClientMenuActionPerformed

    private void startGameMenuActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_startGameMenuActionPerformed
        JFrame mainFrame = CitadelsApp.getApplication().getMainFrame();
        _startGameBox= new StartGameBox(this, mainFrame, true, _organizer.getCurrentGameState().getPlayers().size());
        _startGameBox.setLocationRelativeTo(mainFrame);
        CitadelsApp.getApplication().show(_startGameBox);
    }//GEN-LAST:event_startGameMenuActionPerformed

    private void addAIMenuActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_addAIMenuActionPerformed
       _organizer.addAIPlayer();
    }//GEN-LAST:event_addAIMenuActionPerformed

    private void removeAIMenuActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_removeAIMenuActionPerformed
        _organizer.removeAIPlayer();
    }//GEN-LAST:event_removeAIMenuActionPerformed

    private void resetGameMenuActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_resetGameMenuActionPerformed
        _organizer.resetGame();
    }//GEN-LAST:event_resetGameMenuActionPerformed

    private void joinGameMenuActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_joinGameMenuActionPerformed
        _organizer.joinGame(false);

    }//GEN-LAST:event_joinGameMenuActionPerformed

    private void handsPanelMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_handsPanelMouseEntered
       controlPanel.setControlPanelDescription(playerListString);
    }//GEN-LAST:event_handsPanelMouseEntered

    private void chatPanelMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_chatPanelMouseEntered
       controlPanel.setControlPanelDescription(playerListString);
    }//GEN-LAST:event_chatPanelMouseEntered

    private void controlPanelMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_controlPanelMouseEntered
       controlPanel.setControlPanelDescription(playerListString);
    }//GEN-LAST:event_controlPanelMouseEntered

    private void randomDistrictsSubMenuActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_randomDistrictsSubMenuActionPerformed
        startPredefinedGame(PREDEFINED_GAMES.RANDOM_DISTRICTS);
    }//GEN-LAST:event_randomDistrictsSubMenuActionPerformed

    private void randomOneCharacterAndDistrictsSubMenuActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_randomOneCharacterAndDistrictsSubMenuActionPerformed
        startPredefinedGame(PREDEFINED_GAMES.RANDOM_DISTRICTS_WITH_ONE_EXPANSION_CHARACTER);
    }//GEN-LAST:event_randomOneCharacterAndDistrictsSubMenuActionPerformed

    private void randomTwoCharactersAndDistrictsSubMenuActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_randomTwoCharactersAndDistrictsSubMenuActionPerformed
        startPredefinedGame(PREDEFINED_GAMES.RANDOM_DISTRICTS_WITH_TWO_EXPANSION_CHARACTERS);
    }//GEN-LAST:event_randomTwoCharactersAndDistrictsSubMenuActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JMenuItem addAIMenu;
    private citadels.gui.ChatPanel chatPanel;
    private javax.swing.JMenuItem connectAsClientMenu;
    private citadels.gui.ControlPanel controlPanel;
    private citadels.gui.HandsPanel handsPanel;
    private javax.swing.JPopupMenu.Separator jSeparator1;
    private javax.swing.JPopupMenu.Separator jSeparator2;
    private javax.swing.JPopupMenu.Separator jSeparator3;
    private javax.swing.JMenuItem joinGameMenu;
    private javax.swing.JPanel mainPanel;
    private javax.swing.JMenuBar menuBar;
    private citadels.gui.PlayerPanel playerPanel1;
    private citadels.gui.PlayerPanel playerPanel2;
    private citadels.gui.PlayerPanel playerPanel3;
    private citadels.gui.PlayerPanel playerPanel4;
    private citadels.gui.PlayerPanel playerPanel5;
    private citadels.gui.PlayerPanel playerPanel6;
    private citadels.gui.PlayerPanel playerPanel7;
    private citadels.gui.PlayerPanel playerPanel8;
    private javax.swing.JMenu predefinedGamesMenu;
    private javax.swing.JProgressBar progressBar;
    private javax.swing.JMenuItem randomDistrictsSubMenu;
    private javax.swing.JMenuItem randomOneCharacterAndDistrictsSubMenu;
    private javax.swing.JMenuItem randomTwoCharactersAndDistrictsSubMenu;
    private javax.swing.JMenuItem removeAIMenu;
    private javax.swing.JMenuItem resetGameMenu;
    private citadels.gui.RolePanel rolePanel;
    private javax.swing.JMenuItem startGameMenu;
    private javax.swing.JMenuItem startServerMenu;
    private javax.swing.JLabel statusAnimationLabel;
    private javax.swing.JLabel statusMessageLabel;
    private javax.swing.JPanel statusPanel;
    // End of variables declaration//GEN-END:variables

    private final Timer messageTimer;
    private final Timer busyIconTimer;
    private final Icon idleIcon;
    private final Icon[] busyIcons = new Icon[15];
    private int busyIconIndex = 0;

    private JDialog aboutBox;

}
