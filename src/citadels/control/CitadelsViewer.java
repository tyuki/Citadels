/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package citadels.control;

import citadels.game.GameState;
import java.util.List;
import javax.swing.text.SimpleAttributeSet;

/**
 *
 * @author Personal
 */
public interface CitadelsViewer {

    public void setGameState(GameState game);
    public void addChatMessage(String text, SimpleAttributeSet attr);
    public void setConnectedNames(List<String> names);
    public void reset();
}
