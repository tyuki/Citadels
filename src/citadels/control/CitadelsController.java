/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package citadels.control;

import citadels.game.Characters;
import citadels.game.Character;
import citadels.game.District;
import citadels.game.Player;
import citadels.game.Progress;
import java.util.List;

/**
 *
 * @author Personal
 */
public interface CitadelsController {

    public static final int DISTICT_SMITHY = 0;
    public static final int DISTICT_LABORATORY = 1;
    public static final int DISTICT_MUSEUM = 2;
    
    public void setID(int id);
    public void selectCharacter(Characters characters);
    public void selectNewKing(List<Player> players);
    public void selectTributeToEmperor(Player player);
    public void selectGraveyard(District district);
    /**
     *
     * @param progress
     * @param character
     * @param player
     * @param action
     * @param build
     * @param end
     * @param SAs
     * @param distIncome
     * @param DAs smithy, laboratory, museum
     */
    public void updateControl(Progress progress, Character character, Player player, boolean action, boolean build, boolean end, boolean[] SAs, boolean distIncome, boolean[] DAs);
}
