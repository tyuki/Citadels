/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package citadels.control;

import citadels.CitadelsApp;
import citadels.game.District.EFFECT;
import citadels.game.GameOptions;
import citadels.game.GameState;
import citadels.game.GameState.BUILD_QUERY_RESPONSE;
import citadels.game.Server;
import java.io.IOException;
import java.util.List;
import java.util.Observable;
import java.util.Observer;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import javax.swing.text.SimpleAttributeSet;
import network.Client;
import network.Message;
import network.MessageConnection;

/**
 *
 * @author Personal
 */
public class Organizer implements Observer {

    private org.jdesktop.application.ResourceMap _resourceMap;

    private CitadelsViewer _viewer;
    private CitadelsController _controller = null;

    private int _playerID = -1;
    private String _playerName;

    private Server _server;
    private Client _client;
    private boolean _ackResponse;
    private boolean _ackArrived;

    private GameState _currentState;

    public Organizer() {
        _resourceMap = org.jdesktop.application.Application.getInstance(citadels.CitadelsApp.class).getContext().getResourceMap(CitadelsApp.class);
        
        //Initialize with default viewer that does nothing
        _viewer = new CitadelsViewer() {

            public void setGameState(GameState game) {
            }

            public void addChatMessage(String text, SimpleAttributeSet attr) {
            }

            public void reset() {
            }
            
            public void setConnectedNames(List<String> names) {}

        };
    }

    public void setViewer(CitadelsViewer viewer) {
        _viewer = viewer;
    }

    public void setController(CitadelsController controller) {
        _controller = controller;
    }

     /**
     * Starts a server
     *
     * @param port
     * @return
     */
    public boolean startServer(int port) {
        _server = new Server(port);
        return _server.start();
    }

//    /**
//     * Starts a server
//     *
//     * @param port
//     * @param use9th
//     * @param shortGame
//     * @param expansionCards
//     * @param expansionDistricts
//     * @return
//     */
//    public boolean startServer(int port, boolean use9th, boolean shortGame, int[] expansionCards, EFFECT[] expansionDistricts) {
//        _server = new Server(port, use9th, shortGame, expansionCards, expansionDistricts);
//        return _server.start();
//    }
//
//    /**
//     * Starts a server
//     *
//     * @param port
//     * @param use9th
//     * @param shortGame
//     * @param expansionCards
//     * @param randomCardNum
//     * @return
//     */
//    public boolean startServer(int port, boolean use9th, boolean shortGame, int[] expansionCards, int randomCardNum) {
//        _server = new Server(port, use9th, shortGame, expansionCards, randomCardNum);
//        return _server.start();
//    }

    public void stopServer() {
        if (_server != null) _server.stop();
    }

    /**
     * Connect to a server
     *
     * @param playerName
     * @param host
     * @param port
     * @param join
     * @return
     */
    public boolean connectServer(String playerName, String host, int port) {
        _playerName = playerName;
        _client = new Client(this, host, port);
        if (_client.connect()) {
            try {
                _ackArrived = false;
                _ackResponse = false;
                _client.write(new Message(Message.COMMAND_SETUP, Message.CONNECT, playerName));
                //Wait for response
                while (!_ackArrived) {
                    try {
                        Thread.sleep(10);
                    } catch (InterruptedException ex) {
                    }
                }
                //Check if acked
                if (!_ackResponse) {
                    JOptionPane.showMessageDialog(null, _resourceMap.getString("error.NameUsed"));
                    return false;
                }
                return _ackResponse;
            } catch (IOException ex) {
                JOptionPane.showMessageDialog(null, _resourceMap.getString("error.OtherException") + ":" + ex.getMessage());
                ex.printStackTrace();
                return false;
            }
        }

        return false;
    }

    public void joinGame(boolean AI) {
        if (_client != null && _client.isConnected()) {
            try {
                _client.write(new Message(Message.COMMAND_SETUP, Message.JOIN, AI));
            } catch (IOException ex) {
                JOptionPane.showMessageDialog(null, _resourceMap.getString("error.OtherException") + ":" + ex.getMessage());
                ex.printStackTrace();
            }
        }
    }

    public void resetGame() {
        if (_client.isConnected()) {
            try {
                _client.write(new Message(Message.COMMAND_SETUP, Message.RESET, _playerName));
            } catch (IOException ex) {
                JOptionPane.showMessageDialog(null, _resourceMap.getString("error.OtherException") + ":" + ex.getMessage());
                ex.printStackTrace();
            }
        }
    }

    public void disconnectFromServer(){
        if (_client != null) {
            if (_playerID >= 0) {
                try {
                    _client.write(new Message(Message.COMMAND_SETUP, Message.LEAVE, _playerID));
                } catch (IOException ex) {
                } finally {
                    _playerID = -1;
                    _controller.setID(_playerID);
                }
            }
            _client.disconnect();
            _viewer.addChatMessage("Disconnected from server", null);
        }
    }

    public void addAIPlayer() {
        if (_client != null) {
            try {
                _client.write(new Message(Message.COMMAND_SETUP, Message.ADD_AI, _playerID));
            } catch (IOException ex) {
                JOptionPane.showMessageDialog(null, _resourceMap.getString("error.OtherException") + ":" + ex.getMessage());
                ex.printStackTrace();
            }
        }
    }

    public void removeAIPlayer() {
        if (_client != null) {
            try {
                _client.write(new Message(Message.COMMAND_SETUP, Message.REMOVE_AI, _playerID));
            } catch (IOException ex) {
                JOptionPane.showMessageDialog(null, _resourceMap.getString("error.OtherException") + ":" + ex.getMessage());
                ex.printStackTrace();
            }
        }
    }

    public boolean isServerRunning() {
        if (_server != null) return _server.isRunning();

        return false;
    }

    public int getPort() {
        if (_server != null && _server.isRunning()) return _server.getPort();

        return -1;
    }

    public String getPlayerName() {
        return _playerName;
    }

    /**
     *
     * @param text
     */
    public void sendChatMessage(String text) {
        try {
            if (_client != null) {
                _client.write(new Message(Message.COMMAND_CHAT, Message.USER_MESSAGE, text));
            }
        } catch (IOException ioe) {
            disconnectedFromServer();
        }
    }

    /**
     * 
     */
    public boolean startGame(GameOptions options) {
        if (options != null) {
            try {
                _client.write(new Message(Message.COMMAND_SETUP, Message.START, options));
            } catch (Exception e) {
                e.printStackTrace();
                JOptionPane.showMessageDialog(null, _resourceMap.getString("error.OtherException" + " : " + e.getMessage()));
                return false;
            }
            return true;
        } else {
            return false;
        }
    }

    private void disconnectedFromServer() {
         JOptionPane.showMessageDialog(null, _resourceMap.getString("error.ConnectionClosed"));
    }

    public GameState getCurrentGameState() {
        return _currentState;
    }

    public void update(Observable o, Object arg) {
        if (o instanceof MessageConnection && arg instanceof Message) {
           try {
                messageHandling((MessageConnection)o, (Message)arg);
            } catch (Exception e) {
                JOptionPane.showMessageDialog(null, _resourceMap.getString("error.OtherException") + "\n" + e.getMessage());
                e.printStackTrace();
            }
        }
    }


    private void messageHandling(MessageConnection connection, Message msg) {
        try {
            switch (msg.getCommandType()) {
                case Message.COMMAND_SETUP:
                    setupMessageHandling(connection, msg);
                    break;
                case Message.COMMAND_CHAT:
                    chatMessageHandling(connection, msg);
                    break;
                case Message.COMMAND_GAME:
                    gameMessageHandling(connection, msg);
                    break;
                case Message.COMMAND_ERROR:
                    break;
                default:
                    JOptionPane.showMessageDialog(null, _resourceMap.getString("error.UnhandledCommandType") + ":" + msg.getCommandType());
                    break;
            }
        } catch (IOException ioe) {
            disconnectedFromServer();
        }
    }

    @SuppressWarnings("unchecked")
    private void setupMessageHandling(MessageConnection connection, Message msg) throws IOException {
        switch (msg.getMessageType()) {
            //Check alive
            case Message.CHECKALIVE:
                break;
            //Connection ack
            case Message.CONNECT:
                _ackArrived = true;
                _ackResponse = (Boolean)msg.getItems()[0];
                break;
            //Join ack, player ID returned
            case Message.JOIN:
                _playerID = (Integer)msg.getItems()[0];
                _controller.setID(_playerID);
                break;
            case Message.RESET:
                _playerID = -1;
                _controller.setID(-1);
                _viewer.reset();
                break;
            case Message.CONNECTED_NAMES:
                List<String> connectedList = (List<String>) msg.getItems()[0];
                _viewer.setConnectedNames(connectedList);
                break;
            default:
                JOptionPane.showMessageDialog(null, _resourceMap.getString("error.UnhandledClientMessageType") + ":" + msg.getMessageType());
                break;
        }
    }

    private void chatMessageHandling(MessageConnection connection, Message msg) throws IOException {
        switch (msg.getMessageType()) {
            case Message.USER_MESSAGE:
            case Message.SERVER_MESSAGE:
            case Message.GAME_MESSAGE:
                _viewer.addChatMessage((String)msg.getItems()[0], (SimpleAttributeSet)msg.getItems()[1]);
                break;
            default:
                JOptionPane.showMessageDialog(null, _resourceMap.getString("error.UnhandledClientMessageType") + ":" + msg.getMessageType());
                break;
        }

    }

    private void gameMessageHandling(MessageConnection connection, Message msg) throws IOException {
        switch (msg.getMessageType()) {
            case Message.ENTIRE_GAME_STATE:
                _currentState = (GameState)msg.getItems()[0];
                _viewer.setGameState(_currentState);
                if (_controller != null && _playerID >= 0) {
                    askController(_currentState);
                }
                break;
            default:
                JOptionPane.showMessageDialog(null, _resourceMap.getString("error.UnhandledClientMessageType") + ":" + msg.getMessageType());
                break;
        }

    }

    private void askController(GameState game) {

        //Reset the control panel
        _controller.updateControl(game.getProgress(), null, null, false, false, false, null, false, null);
        
        //nothing to do if the game is not started
        if (!game.getProgress().started) return;
        
        //nothing to do if the game has ended
        if (game.getProgress().ended) return;

        //Roll selection
        if (game.getProgress().roleSelection) {
            //Check if its this players turn
            if (game.getProgress().currentSelectionID != _playerID) return;

            //ask the controller to select a character
            _controller.selectCharacter(game.getCharacters());
        }

        //Check for Emperor actions
        //If its emperor
        if (game.getProgress().useEmperorPower && game.isMyTurn(_playerID)) {
            _controller.selectNewKing(game.getPlayers());
            return;
        }

        //Tribute to Emperor
        if (game.getProgress().crownedByEmperorID == _playerID) {
            _controller.selectTributeToEmperor(game.getPlayer(_playerID));
            return;
        //Others do nothing
        } else if (game.getProgress().crownedByEmperorID >= 0) {
            return;
        }

        //Graveyard can be used
        if (game.getProgress().graveYardID == _playerID) {
            _controller.selectGraveyard(game.getDeck().getGraveyardStop());
            return;
        //Others do nothing
        } else if (game.getProgress().graveYardID >= 0) {
            return;
        }

        //update control for the character
        if (game.isMyTurn(_playerID)) {
            //_controller.updateControl(game.getPlayer(_playerID), game.getProgress(), game.getCharacters());
            boolean action = !game.getProgress().actionTaken;
            boolean build = game.canBuild(_playerID) == BUILD_QUERY_RESPONSE.CAN_BUILD;
            boolean end = game.getProgress().actionTaken;
            boolean[] SAs = game.canUseSpecialAbilities(_playerID);
            boolean[] DAs = game.canUseDistrictAbilities(_playerID);
            _controller.updateControl(game.getProgress(), game.getCurrentCharacter(), game.getPlayer(_playerID), action, build, end, SAs, SAs[3], DAs);
        }
    }

    public void characterSelected(int c) {
        try {
            _client.write(new Message(Message.COMMAND_GAME, Message.SELECT_CHARACTER, new Object[]{_playerID, c}));
        } catch (IOException ex) {
            disconnectedFromServer();
        }
    }

    public void actionCoins() {
        try {
            _client.write(new Message(Message.COMMAND_GAME, Message.ACTION_GOLD, _playerID));
        } catch (IOException ex) {
            disconnectedFromServer();
        }
    }

    public void actionCards(int[] cards) {
        try {
            _client.write(new Message(Message.COMMAND_GAME, Message.ACTION_CARDS, new Object[]{_playerID, cards}));
        } catch (IOException ex) {
            disconnectedFromServer();
        }
    }

    public void buildDistrict(int num) {
        try {
            _client.write(new Message(Message.COMMAND_GAME, Message.BUILD_DISTRICT, new Object[]{_playerID, num}));
        } catch (IOException ex) {
            disconnectedFromServer();
        }

    }

    public void turnEnd() {
        try {
            _client.write(new Message(Message.COMMAND_GAME, Message.END_TURN, _playerID));
        } catch (IOException ex) {
            disconnectedFromServer();
        }
    }

    public void earnDistrictIncome() {
        try {
            _client.write(new Message(Message.COMMAND_GAME, Message.DISTRICT_INCOME, _playerID));
        } catch (IOException ex) {
            disconnectedFromServer();
        }
    }

    public void assassinAbility(int target) {
        try {
            _client.write(new Message(Message.COMMAND_GAME, Message.SA_ASSASSIN, new Object[]{_playerID, target}));
        } catch (IOException ex) {
            disconnectedFromServer();
        }
    }

    public void thiefAbility(int target) {
        try {
            _client.write(new Message(Message.COMMAND_GAME, Message.SA_THIEF, new Object[]{_playerID, target}));
        } catch (IOException ex) {
            disconnectedFromServer();
        }
    }

    public void magicianAbilitySwap(int target) {
        try {
            _client.write(new Message(Message.COMMAND_GAME, Message.SA_MAGICIAN_SWAP, new Object[]{_playerID, target}));
        } catch (IOException ex) {
            disconnectedFromServer();
        }
    }

    public void magicianAbilityExchange(int[] cards) {
        try {
            _client.write(new Message(Message.COMMAND_GAME, Message.SA_MAGICIAN_EXCHANGE, new Object[]{_playerID, cards}));
        } catch (IOException ex) {
            disconnectedFromServer();
        }
    }

    public void warlordAbility(int targetPlayerID, int targetDistrict) {
        try {
            _client.write(new Message(Message.COMMAND_GAME, Message.SA_WARLORD, new Object[]{_playerID, targetPlayerID, targetDistrict}));
        } catch (IOException ex) {
            disconnectedFromServer();
        }
    }

    public void witchAbility(int target) {
        try {
            _client.write(new Message(Message.COMMAND_GAME, Message.SA_WITCH, new Object[]{_playerID, target}));
        } catch (IOException ex) {
            disconnectedFromServer();
        }
    }


   public void wizardAbility(int targetPlayerID, int targetDistrict, boolean build) {
        try {
            _client.write(new Message(Message.COMMAND_GAME, Message.SA_WIZARD, new Object[]{_playerID, targetPlayerID, targetDistrict, build}));
        } catch (IOException ex) {
            disconnectedFromServer();
        }
   }

    public void emperorAbilityKingSelected(int target) {
        try {
            _client.write(new Message(Message.COMMAND_GAME, Message.SA_EMPEROR_KING, new Object[]{_playerID, target}));
        } catch (IOException ex) {
            disconnectedFromServer();
        }
    }

    public void emperorTributeCard(int cardNum) {
        try {
            _client.write(new Message(Message.COMMAND_GAME, Message.SA_EMPEROR_TRIBUTE_CARD, new Object[]{_playerID, cardNum}));
        } catch (IOException ex) {
            disconnectedFromServer();
        }
    }

    public void emperorTributeGold() {
        try {
            _client.write(new Message(Message.COMMAND_GAME, Message.SA_EMPEROR_TRIBUTE_GOLD, _playerID));
        } catch (IOException ex) {
            disconnectedFromServer();
        }
        
    }

    public void abbotAbility() {
        try {
            _client.write(new Message(Message.COMMAND_GAME, Message.SA_ABBOT, _playerID));
        } catch (IOException ex) {
            disconnectedFromServer();
        }
        
    }

    public void navigatorAbilityGold() {
        try {
            _client.write(new Message(Message.COMMAND_GAME, Message.SA_NAVIGATOR_GOLD, _playerID));
        } catch (IOException ex) {
            disconnectedFromServer();
        }
    }

    public void navigatorAbilityCards() {
        try {
            _client.write(new Message(Message.COMMAND_GAME, Message.SA_NAVIGATOR_CARDS, _playerID));
        } catch (IOException ex) {
            disconnectedFromServer();
        }
    }

    public void diplomatAbility(int districtIndex, int otherPlayerID, int otherDistrictIndex) {
        try {
            _client.write(new Message(Message.COMMAND_GAME, Message.SA_DIPLOMAT, new Object[]{_playerID, districtIndex, otherPlayerID, otherDistrictIndex}));
        } catch (IOException ex) {
            disconnectedFromServer();
        }
    }

    public void artistAbility(int targetPlayer, int targetDistrict) {
        try {
            _client.write(new Message(Message.COMMAND_GAME, Message.SA_ARTIST, new Object[]{_playerID, targetDistrict}));
        } catch (IOException ex) {
            disconnectedFromServer();
        }
    }

    public void smithyAbility() {
        try {
            _client.write(new Message(Message.COMMAND_GAME, Message.DA_SMITHY, _playerID));
        } catch (IOException ex) {
            disconnectedFromServer();
        }
    }

    public void laboratoryAbility(int discard) {
        try {
            _client.write(new Message(Message.COMMAND_GAME, Message.DA_LABORATORY, new Object[] {_playerID, discard}));
        } catch (IOException ex) {
            disconnectedFromServer();
        }
    }

    public void graveyardAbility(boolean use) {
        try {
            _client.write(new Message(Message.COMMAND_GAME, Message.DA_GRAVEYARD, new Object[] {_playerID, use}));
        } catch (IOException ex) {
            disconnectedFromServer();
        }
    }

    public void lighthouseAbility(int build, int take) {
        try {
            _client.write(new Message(Message.COMMAND_GAME, Message.DA_LIGHTHOUSE, new Object[] {_playerID, build, take}));
        } catch (IOException ex) {
            disconnectedFromServer();
        }
    }

    public void museumAbility(int museum, int discard) {
        try {
            _client.write(new Message(Message.COMMAND_GAME, Message.DA_MUSEUM, new Object[] {_playerID, museum, discard}));
        } catch (IOException ex) {
            disconnectedFromServer();
        }
    }

    public void armoryAbility(int targetID, int targetDistrict) {
        try {
            _client.write(new Message(Message.COMMAND_GAME, Message.DA_ARMORY, new Object[] {_playerID, targetID, targetDistrict}));
        } catch (IOException ex) {
            disconnectedFromServer();
        }
    }

    public void belltowerAbility(int district) {
        try {
            _client.write(new Message(Message.COMMAND_GAME, Message.DA_BELLTOWER, new Object[] {_playerID, district}));
        } catch (IOException ex) {
            disconnectedFromServer();
        }
    }

}
