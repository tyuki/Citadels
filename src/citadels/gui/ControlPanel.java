/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/*
 * ControlPanel.java
 *
 * Created on 2010/01/04, 9:28:23
 */

package citadels.gui;

import citadels.CitadelsApp;
import citadels.CitadelsView;
import citadels.control.CitadelsController;
import citadels.game.Character;
import citadels.game.Character.CHARACTER;
import citadels.game.District;
import citadels.game.Player;
import java.util.List;
import org.jdesktop.application.ResourceMap;

/**
 *
 * @author Personal
 */
public class ControlPanel extends javax.swing.JPanel {

    private ResourceMap _resourceMap;
    private CitadelsView _view;
    private Character _character;;

    /** Creates new form ControlPanel */
    public ControlPanel() {
        initComponents();
        _resourceMap = org.jdesktop.application.Application.getInstance(citadels.CitadelsApp.class).getContext().getResourceMap(CitadelsApp.class);
    }

    public void setView(CitadelsView view) {
        _view = view;
    }

    public void updateControl(Character character, Player player, boolean action, boolean build, boolean end, boolean[] SAs, boolean distIncome, boolean[] DAs) {
        _character = character;

        //Disable all in the beginning
        this.disableAll();
        //Do nothing if its not my turn
        if (_character == null) {
            return;
        }

        //Basics
        setBasicActionEnabled(action, build, end);
        //Specials
        setSpecialAbilitiesEnabled(character.character, SAs, distIncome);
        //Districts
        setDistrictAbilitiesEnabled(player.getCity(), DAs);
    }

    private void disableAll() {
            //disable all buttons
            Coins.setIcon(null); Coins.setEnabled(false);
            Cards.setIcon(null); Cards.setEnabled(false);
            Build.setIcon(null); Build.setEnabled(false);
            End.setIcon(null); End.setEnabled(false);

            SpecialAbility1.setIcon(null); SpecialAbility1.setEnabled(false);
            SpecialAbility2.setIcon(null); SpecialAbility2.setEnabled(false);
            SpecialAbility3.setIcon(null); SpecialAbility3.setEnabled(false);
            DistrictIncome.setIcon(null); DistrictIncome.setEnabled(false);

            Smithy.setIcon(null); Smithy.setEnabled(false);
            Laboratory.setIcon(null); Laboratory.setEnabled(false);
    }

    private void setBasicActionEnabled(boolean action, boolean build, boolean end) {
        Coins.setIcon(IconContainer.getIcon("coins"));
        Cards.setIcon(IconContainer.getIcon("cards"));
        Build.setIcon(IconContainer.getIcon("build"));
        End.setIcon(IconContainer.getIcon("end"));

        Coins.setEnabled(action);
        Cards.setEnabled(action);
        Build.setEnabled(build);
        End.setEnabled(end);
    }

    private void setSpecialAbilitiesEnabled(CHARACTER character, boolean[] SAs, boolean districtIncome) {
            //Set Special ability Icons
            switch (character) {
                case ASSASSIN:
                    SpecialAbility1.setIcon(IconContainer.getIcon("assassinSA"));
                    break;
                case THIEF:
                    SpecialAbility1.setIcon(IconContainer.getIcon("thiefSA"));
                    break;
                case MAGICIAN:
                    SpecialAbility1.setIcon(IconContainer.getIcon("magicianSA1"));
                    SpecialAbility2.setIcon(IconContainer.getIcon("magicianSA2"));
                    break;
                case KING:
                    DistrictIncome.setIcon(IconContainer.getIcon("yellow"));
                    break;
                case BISHOP:
                    DistrictIncome.setIcon(IconContainer.getIcon("blue"));
                    break;
                case MERCHANT:
                    DistrictIncome.setIcon(IconContainer.getIcon("green"));
                    break;
                case WARLORD:
                    SpecialAbility1.setIcon(IconContainer.getIcon("warlordSA"));
                    DistrictIncome.setIcon(IconContainer.getIcon("red"));
                    break;
                case WITCH:
                    SpecialAbility1.setIcon(IconContainer.getIcon("witchSA"));
                    break;
                case WIZARD:
                    SpecialAbility1.setIcon(IconContainer.getIcon("magicianSA1"));
                    break;
                case EMPEROR:
                    DistrictIncome.setIcon(IconContainer.getIcon("yellow"));
                    break;
                case ABBOT:
                    SpecialAbility1.setIcon(IconContainer.getIcon("coins"));
                    DistrictIncome.setIcon(IconContainer.getIcon("blue"));
                    break;
                case NAVIGATOR:
                    SpecialAbility1.setIcon(IconContainer.getIcon("navigatorSA_coin"));
                    SpecialAbility2.setIcon(IconContainer.getIcon("navigatorSA_card"));
                    break;
                case DIPLOMAT:
                    SpecialAbility1.setIcon(IconContainer.getIcon("warlordSA"));
                    DistrictIncome.setIcon(IconContainer.getIcon("red"));
                    break;
                case ARTIST:
                    SpecialAbility1.setIcon(IconContainer.getIcon("artistSA"));
                    break;
            }

            //Set enabled accoring to the inputs
            if (SAs[0] && SpecialAbility1.getIcon() != null) {
                SpecialAbility1.setEnabled(true);
            }
            if (SAs[1] && SpecialAbility2.getIcon() != null) {
                SpecialAbility2.setEnabled(true);
            }
            if (SAs[2] && SpecialAbility3.getIcon() != null) {
                SpecialAbility3.setEnabled(true);
            }
            if (districtIncome && DistrictIncome.getIcon() != null) {
                DistrictIncome.setEnabled(true);
            }
        
    }

    private void setDistrictAbilitiesEnabled(List<District> city, boolean[] DAs) {
        for (District dist : city) {

            switch (dist.getEffect()) {
                case SMITHY:
                    Smithy.setIcon(IconContainer.getIcon("smithySA"));
                    Smithy.setEnabled(DAs[CitadelsController.DISTICT_SMITHY]);
                    break;
                case LABORATORY:
                    Laboratory.setIcon(IconContainer.getIcon("laboratorySA"));
                    Laboratory.setEnabled(DAs[CitadelsController.DISTICT_LABORATORY]);
                    break;
            }
        }
    }
    
    synchronized public void setControlPanelDescription(String text) {
        ControlDescriptionPane.setText(text);
        ControlDescriptionPane.setCaretPosition(0);
    }

    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        Coins = new javax.swing.JLabel();
        Cards = new javax.swing.JLabel();
        Build = new javax.swing.JLabel();
        End = new javax.swing.JLabel();
        SpecialAbility1 = new javax.swing.JLabel();
        SpecialAbility2 = new javax.swing.JLabel();
        SpecialAbility3 = new javax.swing.JLabel();
        DistrictIncome = new javax.swing.JLabel();
        Smithy = new javax.swing.JLabel();
        Laboratory = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        ControlDescriptionPane = new javax.swing.JTextPane();
        ControlActionLabel = new javax.swing.JLabel();
        ControlSALabel = new javax.swing.JLabel();
        ControlDALabel = new javax.swing.JLabel();

        org.jdesktop.application.ResourceMap resourceMap = org.jdesktop.application.Application.getInstance(citadels.CitadelsApp.class).getContext().getResourceMap(ControlPanel.class);
        setBackground(resourceMap.getColor("Form.background")); // NOI18N
        setEnabled(false);
        setMaximumSize(new java.awt.Dimension(350, 300));
        setName("Form"); // NOI18N
        setPreferredSize(new java.awt.Dimension(345, 230));

        Coins.setText(resourceMap.getString("Coins.text")); // NOI18N
        Coins.setEnabled(false);
        Coins.setMaximumSize(new java.awt.Dimension(36, 36));
        Coins.setMinimumSize(new java.awt.Dimension(36, 36));
        Coins.setName("Coins"); // NOI18N
        Coins.setPreferredSize(new java.awt.Dimension(36, 36));
        Coins.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                CoinsMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                ControlsMouseExited(evt);
            }
            public void mousePressed(java.awt.event.MouseEvent evt) {
                CoinsMousePressed(evt);
            }
        });

        Cards.setEnabled(false);
        Cards.setMaximumSize(new java.awt.Dimension(36, 36));
        Cards.setMinimumSize(new java.awt.Dimension(36, 36));
        Cards.setName("Cards"); // NOI18N
        Cards.setPreferredSize(new java.awt.Dimension(36, 36));
        Cards.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                CardsMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                ControlsMouseExited(evt);
            }
            public void mousePressed(java.awt.event.MouseEvent evt) {
                CardsMousePressed(evt);
            }
        });

        Build.setEnabled(false);
        Build.setMaximumSize(new java.awt.Dimension(36, 36));
        Build.setMinimumSize(new java.awt.Dimension(36, 36));
        Build.setName("Build"); // NOI18N
        Build.setPreferredSize(new java.awt.Dimension(36, 36));
        Build.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                BuildMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                ControlsMouseExited(evt);
            }
        });

        End.setEnabled(false);
        End.setMaximumSize(new java.awt.Dimension(36, 36));
        End.setMinimumSize(new java.awt.Dimension(36, 36));
        End.setName("End"); // NOI18N
        End.setPreferredSize(new java.awt.Dimension(36, 36));
        End.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                EndMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                ControlsMouseExited(evt);
            }
            public void mousePressed(java.awt.event.MouseEvent evt) {
                EndMousePressed(evt);
            }
        });

        SpecialAbility1.setEnabled(false);
        SpecialAbility1.setMaximumSize(new java.awt.Dimension(36, 36));
        SpecialAbility1.setMinimumSize(new java.awt.Dimension(36, 36));
        SpecialAbility1.setName("SpecialAbility1"); // NOI18N
        SpecialAbility1.setPreferredSize(new java.awt.Dimension(36, 36));
        SpecialAbility1.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                SpecialAbility1MouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                ControlsMouseExited(evt);
            }
            public void mousePressed(java.awt.event.MouseEvent evt) {
                SpecialAbility1MousePressed(evt);
            }
        });

        SpecialAbility2.setEnabled(false);
        SpecialAbility2.setMaximumSize(new java.awt.Dimension(36, 36));
        SpecialAbility2.setMinimumSize(new java.awt.Dimension(36, 36));
        SpecialAbility2.setName("SpecialAbility2"); // NOI18N
        SpecialAbility2.setPreferredSize(new java.awt.Dimension(36, 36));
        SpecialAbility2.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                SpecialAbility2MouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                ControlsMouseExited(evt);
            }
            public void mousePressed(java.awt.event.MouseEvent evt) {
                SpecialAbility2MousePressed(evt);
            }
        });

        SpecialAbility3.setEnabled(false);
        SpecialAbility3.setMaximumSize(new java.awt.Dimension(36, 36));
        SpecialAbility3.setMinimumSize(new java.awt.Dimension(36, 36));
        SpecialAbility3.setName("SpecialAbility3"); // NOI18N
        SpecialAbility3.setPreferredSize(new java.awt.Dimension(36, 36));
        SpecialAbility3.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                SpecialAbility3MouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                ControlsMouseExited(evt);
            }
            public void mousePressed(java.awt.event.MouseEvent evt) {
                SpecialAbility3MousePressed(evt);
            }
        });

        DistrictIncome.setEnabled(false);
        DistrictIncome.setMaximumSize(new java.awt.Dimension(36, 36));
        DistrictIncome.setMinimumSize(new java.awt.Dimension(36, 36));
        DistrictIncome.setName("DistrictIncome"); // NOI18N
        DistrictIncome.setPreferredSize(new java.awt.Dimension(36, 36));
        DistrictIncome.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                DistrictIncomeMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                ControlsMouseExited(evt);
            }
            public void mousePressed(java.awt.event.MouseEvent evt) {
                DistrictIncomeMousePressed(evt);
            }
        });

        Smithy.setEnabled(false);
        Smithy.setMaximumSize(new java.awt.Dimension(36, 36));
        Smithy.setMinimumSize(new java.awt.Dimension(36, 36));
        Smithy.setName("Smithy"); // NOI18N
        Smithy.setPreferredSize(new java.awt.Dimension(36, 36));
        Smithy.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                SmithyMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                ControlsMouseExited(evt);
            }
            public void mousePressed(java.awt.event.MouseEvent evt) {
                SmithyMousePressed(evt);
            }
        });

        Laboratory.setEnabled(false);
        Laboratory.setMaximumSize(new java.awt.Dimension(36, 36));
        Laboratory.setMinimumSize(new java.awt.Dimension(36, 36));
        Laboratory.setName("Laboratory"); // NOI18N
        Laboratory.setPreferredSize(new java.awt.Dimension(36, 36));
        Laboratory.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                LaboratoryMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                ControlsMouseExited(evt);
            }
            public void mousePressed(java.awt.event.MouseEvent evt) {
                LaboratoryMousePressed(evt);
            }
        });

        jScrollPane1.setName("jScrollPane1"); // NOI18N

        ControlDescriptionPane.setBackground(resourceMap.getColor("ControlDescriptionPane.background")); // NOI18N
        ControlDescriptionPane.setBorder(null);
        ControlDescriptionPane.setContentType(resourceMap.getString("ControlDescriptionPane.contentType")); // NOI18N
        ControlDescriptionPane.setEditable(false);
        ControlDescriptionPane.setFont(resourceMap.getFont("ControlDescriptionPane.font")); // NOI18N
        ControlDescriptionPane.setName("ControlDescriptionPane"); // NOI18N
        jScrollPane1.setViewportView(ControlDescriptionPane);

        ControlActionLabel.setFont(resourceMap.getFont("ControlActionLabel.font")); // NOI18N
        ControlActionLabel.setText(resourceMap.getString("ControlActionLabel.text")); // NOI18N
        ControlActionLabel.setName("ControlActionLabel"); // NOI18N

        ControlSALabel.setFont(resourceMap.getFont("ControlSALabel.font")); // NOI18N
        ControlSALabel.setText(resourceMap.getString("ControlSALabel.text")); // NOI18N
        ControlSALabel.setName("ControlSALabel"); // NOI18N

        ControlDALabel.setFont(resourceMap.getFont("ControlDALabel.font")); // NOI18N
        ControlDALabel.setText(resourceMap.getString("ControlDALabel.text")); // NOI18N
        ControlDALabel.setName("ControlDALabel"); // NOI18N

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(Coins, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(Cards, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(Build, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(End, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(12, 12, 12))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(ControlActionLabel, javax.swing.GroupLayout.DEFAULT_SIZE, 99, Short.MAX_VALUE)
                        .addGap(78, 78, 78))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(Smithy, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(Laboratory, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(ControlDALabel, javax.swing.GroupLayout.DEFAULT_SIZE, 177, Short.MAX_VALUE)
                    .addComponent(ControlSALabel, javax.swing.GroupLayout.DEFAULT_SIZE, 177, Short.MAX_VALUE)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(SpecialAbility1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(SpecialAbility2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(SpecialAbility3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(DistrictIncome, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 139, Short.MAX_VALUE)
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 210, Short.MAX_VALUE)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(ControlActionLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 13, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(End, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(Build, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(Cards, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(Coins, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(18, 18, 18)
                        .addComponent(ControlSALabel, javax.swing.GroupLayout.PREFERRED_SIZE, 13, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(DistrictIncome, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(SpecialAbility3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(SpecialAbility2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(SpecialAbility1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 23, Short.MAX_VALUE)
                        .addComponent(ControlDALabel, javax.swing.GroupLayout.PREFERRED_SIZE, 13, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(Smithy, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(Laboratory, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addContainerGap())
        );
    }// </editor-fold>//GEN-END:initComponents

    private void CoinsMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_CoinsMouseEntered
        if (evt.getComponent().isEnabled()) {
            this.setControlPanelDescription(_resourceMap.getString("control.description.coins"));
        }
    }//GEN-LAST:event_CoinsMouseEntered

    private void CardsMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_CardsMouseEntered
        if (evt.getComponent().isEnabled()) {
            this.setControlPanelDescription(_resourceMap.getString("control.description.cards"));
        }
    }//GEN-LAST:event_CardsMouseEntered

    private void EndMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_EndMouseEntered
        if (evt.getComponent().isEnabled()) {
            this.setControlPanelDescription(_resourceMap.getString("control.description.end"));
        }
    }//GEN-LAST:event_EndMouseEntered

    private void SpecialAbility1MouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_SpecialAbility1MouseEntered
        if (evt.getComponent().isEnabled()) {
            if (_character != null) {
                if (_resourceMap.containsKey("control.description."+_character.name+"SA1")) {
                    this.setControlPanelDescription(_resourceMap.getString("control.description."+_character.name+"SA1"));
                }
            }
        }
    }//GEN-LAST:event_SpecialAbility1MouseEntered

    private void SpecialAbility2MouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_SpecialAbility2MouseEntered
        if (evt.getComponent().isEnabled()) {
            if (_character != null) {
                if (_resourceMap.containsKey("control.description."+_character.name+"SA2")) {
                    this.setControlPanelDescription(_resourceMap.getString("control.description."+_character.name+"SA2"));
                }
            }
        }
    }//GEN-LAST:event_SpecialAbility2MouseEntered

    private void SpecialAbility3MouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_SpecialAbility3MouseEntered
        if (evt.getComponent().isEnabled()) {
            if (_character != null) {
                if (_resourceMap.containsKey("control.description."+_character.name+"SA3")) {
                    this.setControlPanelDescription(_resourceMap.getString("control.description."+_character.name+"SA3"));
                }
            }
        }
    }//GEN-LAST:event_SpecialAbility3MouseEntered

    private void DistrictIncomeMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_DistrictIncomeMouseEntered
         if (evt.getComponent().isEnabled()) {
            this.setControlPanelDescription(_resourceMap.getString("control.description.districtIncome"));
        }
    }//GEN-LAST:event_DistrictIncomeMouseEntered

    private void SmithyMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_SmithyMouseEntered
        if (evt.getComponent().isEnabled()) {
            this.setControlPanelDescription(_resourceMap.getString("control.description.smithy"));
        }
    }//GEN-LAST:event_SmithyMouseEntered

    private void LaboratoryMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_LaboratoryMouseEntered
        if (evt.getComponent().isEnabled()) {
            this.setControlPanelDescription(_resourceMap.getString("control.description.laboratory"));
        }
    }//GEN-LAST:event_LaboratoryMouseEntered

    private void ControlsMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_ControlsMouseExited
        //this.setControlPanelDescription("");
    }//GEN-LAST:event_ControlsMouseExited

    private void CoinsMousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_CoinsMousePressed
         if (evt.getComponent().isEnabled()) {
             _view.actionCoinsSelected();
         }
    }//GEN-LAST:event_CoinsMousePressed

    private void CardsMousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_CardsMousePressed
        if (evt.getComponent().isEnabled()) {
             _view.actionCardSelected();
         }
    }//GEN-LAST:event_CardsMousePressed

    private void EndMousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_EndMousePressed
        if (evt.getComponent().isEnabled()) {
             _view.turnEndSelected();
         }
    }//GEN-LAST:event_EndMousePressed

    private void SpecialAbility1MousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_SpecialAbility1MousePressed
        if (evt.getComponent().isEnabled()) {
             _view.specialAbility1Selected();
         }
    }//GEN-LAST:event_SpecialAbility1MousePressed

    private void SpecialAbility2MousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_SpecialAbility2MousePressed
         if (evt.getComponent().isEnabled()) {
             _view.specialAbility2Selected();
         }
    }//GEN-LAST:event_SpecialAbility2MousePressed

    private void SpecialAbility3MousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_SpecialAbility3MousePressed
        // TODO add your handling code here:
    }//GEN-LAST:event_SpecialAbility3MousePressed

    private void DistrictIncomeMousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_DistrictIncomeMousePressed
        if (evt.getComponent().isEnabled()) {
             _view.districtIncomeSelected();
         }
    }//GEN-LAST:event_DistrictIncomeMousePressed

    private void SmithyMousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_SmithyMousePressed
        if (evt.getComponent().isEnabled()) {
            _view.smithySelected();
        }
    }//GEN-LAST:event_SmithyMousePressed

    private void LaboratoryMousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_LaboratoryMousePressed
        if (evt.getComponent().isEnabled()) {
            _view.laboratorySelected();
        }
    }//GEN-LAST:event_LaboratoryMousePressed

    private void BuildMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_BuildMouseEntered
         if (evt.getComponent().isEnabled()) {
            this.setControlPanelDescription(_resourceMap.getString("control.description.build"));
        }
    }//GEN-LAST:event_BuildMouseEntered


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel Build;
    private javax.swing.JLabel Cards;
    private javax.swing.JLabel Coins;
    private javax.swing.JLabel ControlActionLabel;
    private javax.swing.JLabel ControlDALabel;
    private javax.swing.JTextPane ControlDescriptionPane;
    private javax.swing.JLabel ControlSALabel;
    private javax.swing.JLabel DistrictIncome;
    private javax.swing.JLabel End;
    private javax.swing.JLabel Laboratory;
    private javax.swing.JLabel Smithy;
    private javax.swing.JLabel SpecialAbility1;
    private javax.swing.JLabel SpecialAbility2;
    private javax.swing.JLabel SpecialAbility3;
    private javax.swing.JScrollPane jScrollPane1;
    // End of variables declaration//GEN-END:variables

}
