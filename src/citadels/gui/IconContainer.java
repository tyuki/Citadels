/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package citadels.gui;

import java.awt.Image;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.imageio.ImageIO;
import javax.swing.ImageIcon;

/**
 *
 * @author Personal
 */
public class IconContainer {

    public static enum IMAGE_TYPE {
        DISTRICT, ROLE_LABEL, ROLE_CARD, CONTROL, OTHER
    }

    private static Map<String, ImageIcon> _images = new HashMap<String, ImageIcon>();
    private static Map<String, ImageIcon> _miniImages = new HashMap<String, ImageIcon>();

    private static int MINI_WIDTH_DISTRICT = 30;
    private static int MINI_HEIGHT_DISTRICT = 40;
    private static int MINI_WIDTH_ROLE_LABEL = 60;
    private static int MINI_HEIGHT_ROLE_LABEL = 12;
    private static int MINI_WIDTH_ROLE_CARD = 30;
    private static int MINI_HEIGHT_ROLE_CARD = 40;
    private static int MINI_WIDTH_CONTROL = 24;
    private static int MINI_HEIGHT_CONTROL = 24;

    public static void loadDirectory(String directory, IMAGE_TYPE type) {
        File dir = new File(directory);
        if (!dir.isDirectory()) return;

        for (File file : dir.listFiles()) {
            try {
                if (!file.isFile()) continue;
                loadIcon(file.getAbsolutePath(), type);
            } catch (FileNotFoundException ex) {
                Logger.getLogger(IconContainer.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    public static void loadIcon(String filename, IMAGE_TYPE type) throws FileNotFoundException {
        try {
            InputStream is = new FileInputStream(filename);
            BufferedImage img = ImageIO.read(is);
            String converted = filename.replaceAll("\\\\", "/");
            Pattern regex = Pattern.compile("(.+\\/)*(.+)\\.png");
            Matcher matcher = regex.matcher(converted);
            if (matcher.matches()) {
                String name = matcher.group(2);
                switch (type) {
                    case DISTRICT:
                       _images.put(name, new ImageIcon(img));
                        _miniImages.put(name, new ImageIcon(img.getScaledInstance(MINI_WIDTH_DISTRICT, MINI_HEIGHT_DISTRICT, Image.SCALE_SMOOTH)));
                        break;
                    case ROLE_LABEL:
                       _images.put(name, new ImageIcon(img));
                       _miniImages.put(name, new ImageIcon(img.getScaledInstance(MINI_WIDTH_ROLE_LABEL, MINI_HEIGHT_ROLE_LABEL, Image.SCALE_SMOOTH)));
                        break;
                    case ROLE_CARD:
                       _images.put(name, new ImageIcon(img.getScaledInstance(MINI_WIDTH_ROLE_CARD*2, MINI_HEIGHT_ROLE_CARD*2, Image.SCALE_SMOOTH)));
                       _miniImages.put(name, new ImageIcon(img.getScaledInstance(MINI_WIDTH_ROLE_CARD, MINI_HEIGHT_ROLE_CARD, Image.SCALE_SMOOTH)));
                        break;
                    case CONTROL:
                       _images.put(name, new ImageIcon(img));
                       _miniImages.put(name, new ImageIcon(img.getScaledInstance(MINI_WIDTH_CONTROL, MINI_HEIGHT_CONTROL, Image.SCALE_SMOOTH)));
                        break;
                    case OTHER:
                       _images.put(name, new ImageIcon(img));
                        break;
                }
            }
        } catch (FileNotFoundException fne) {
            throw fne;
        } catch (IOException ex) {
            Logger.getLogger(IconContainer.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public static ImageIcon getIcon(String name) {
        return _images.get(name);
    }

    public static ImageIcon getMiniIcon(String name) {
        return _miniImages.get(name);
    }

}
