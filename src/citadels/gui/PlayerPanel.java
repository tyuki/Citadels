/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/*
 * PlayerPanel.java
 *
 * Created on 2010/01/03, 16:36:25
 */

package citadels.gui;

import citadels.CitadelsApp;
import citadels.CitadelsView;
import citadels.game.Characters;
import citadels.game.District;
import citadels.game.District.EFFECT;
import citadels.game.Player;
import citadels.game.Progress;
import java.awt.Color;
import java.util.LinkedList;
import java.util.List;
import javax.swing.JLabel;

/**
 *
 * @author Personal
 */
public class PlayerPanel extends javax.swing.JPanel {

    private org.jdesktop.application.ResourceMap _resourceMap;

    private Color _defaultColor;

    private CitadelsView _view;
    private List<JLabel> _districtLabels;
    private Player _player;
    private List<District> _city;
    

    /** Creates new form PlayerPanel */
    public PlayerPanel() {
        initComponents();
        _defaultColor = this.getBackground();
        
        _resourceMap = org.jdesktop.application.Application.getInstance(citadels.CitadelsApp.class).getContext().getResourceMap(CitadelsApp.class);

        _districtLabels = new LinkedList<JLabel>();
        _districtLabels.add(DistrictLabel1);
        _districtLabels.add(DistrictLabel2);
        _districtLabels.add(DistrictLabel3);
        _districtLabels.add(DistrictLabel4);
        _districtLabels.add(DistrictLabel5);
        _districtLabels.add(DistrictLabel6);
        _districtLabels.add(DistrictLabel7);
        _districtLabels.add(DistrictLabel8);
        
    }

    public void setView(CitadelsView view) {
        _view = view;
    }

    private void clearAll() {
        PlayerNameLabel.setText("");
        GoldLabel.setText("");
        CardsLabel.setText("");
        KingLabel.setIcon(null);
        CharacterLabel.setIcon(null);
        for (int i = 0; i < _districtLabels.size(); i++) {
            _districtLabels.get(i).setIcon(null);
            _districtLabels.get(i).setText("");
            _districtLabels.get(i).setEnabled(false);
        }

        this.setBackground(_defaultColor);

    }

    public void setPlayer(Player player, Progress progress, Characters characters, int playerID) {
        //
        if (player == null || progress == null) {
            clearAll();
            return;
        }

        //border
        if (_player != null && _player.getID() == playerID && progress.started) {
            if ((_player.getCharacter() != null && progress.currentCharacter == _player.getCharacter().order) ||
                    progress.currentSelectionID == playerID) {
                setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(255, 0, 0)));
            } else {
                setBorder(null);
            }
        } else {
            setBorder(null);
        }

        //Set name, gold ,cards
        PlayerNameLabel.setText(player.getName());
        GoldLabel.setText(_resourceMap.getString("term.gold") + " : "+ player.getGold());
        CardsLabel.setText(_resourceMap.getString("term.cards") + " : "+ player.getHand().size());

        //King
        if (player.getIsKing()) {
            if (IconContainer.getIcon("crown") != null) {
                KingLabel.setIcon(IconContainer.getIcon("crown"));
            }
        } else {
                KingLabel.setIcon(null);
        }

        //Districts
        _player = player;
        _city = player.getCity();
        for (int i = 0; i < Math.min(_city.size(), _districtLabels.size()); i++) {
            //beautified
            if (_city.get(i).getBeautified()) {
                _districtLabels.get(i).setIcon(IconContainer.getMiniIcon(_city.get(i).getName()+"_beautified"));
            //not
            } else {
                _districtLabels.get(i).setIcon(IconContainer.getMiniIcon(_city.get(i).getName()));
            }
            _districtLabels.get(i).setText(Integer.toString(i));
            _districtLabels.get(i).setEnabled(true);
        }
        for (int i = _city.size(); i < _districtLabels.size(); i++) {
            _districtLabels.get(i).setIcon(null);
            _districtLabels.get(i).setText("");
            _districtLabels.get(i).setEnabled(false);
        }
        
        //Character
        if (player.getCharacter() != null) {
            if (player.getID() == playerID) {
                CharacterLabel.setIcon(IconContainer.getMiniIcon(player.getCharacter().name));
            } else {
                if (!progress.roleSelection && player.getCharacter().order <= progress.currentCharacter && characters.isPlayed(player.getCharacter().order)) {
                    CharacterLabel.setIcon(IconContainer.getMiniIcon(player.getCharacter().name));
                } else {
                    CharacterLabel.setIcon(IconContainer.getMiniIcon("unknown"));
                }
            }
        } else {
            CharacterLabel.setIcon(null);
        }

        //Background
        this.setBackground(player.getColor());
    }


    


    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        PlayerNameLabel = new javax.swing.JLabel();
        DistrictLabel1 = new javax.swing.JLabel();
        DistrictLabel2 = new javax.swing.JLabel();
        DistrictLabel3 = new javax.swing.JLabel();
        DistrictLabel4 = new javax.swing.JLabel();
        DistrictLabel5 = new javax.swing.JLabel();
        DistrictLabel6 = new javax.swing.JLabel();
        DistrictLabel7 = new javax.swing.JLabel();
        DistrictLabel8 = new javax.swing.JLabel();
        GoldLabel = new javax.swing.JLabel();
        CardsLabel = new javax.swing.JLabel();
        CharacterLabel = new javax.swing.JLabel();
        KingLabel = new javax.swing.JLabel();

        org.jdesktop.application.ResourceMap resourceMap = org.jdesktop.application.Application.getInstance(citadels.CitadelsApp.class).getContext().getResourceMap(PlayerPanel.class);
        setBackground(resourceMap.getColor("Form.background")); // NOI18N
        setMaximumSize(new java.awt.Dimension(170, 170));
        setMinimumSize(new java.awt.Dimension(170, 150));
        setName("Form"); // NOI18N
        setPreferredSize(new java.awt.Dimension(170, 170));

        PlayerNameLabel.setFont(resourceMap.getFont("PlayerNameLabel.font")); // NOI18N
        PlayerNameLabel.setText(resourceMap.getString("PlayerNameLabel.text")); // NOI18N
        PlayerNameLabel.setMaximumSize(new java.awt.Dimension(140, 19));
        PlayerNameLabel.setMinimumSize(new java.awt.Dimension(140, 19));
        PlayerNameLabel.setName("PlayerNameLabel"); // NOI18N
        PlayerNameLabel.setPreferredSize(new java.awt.Dimension(140, 19));

        DistrictLabel1.setText(resourceMap.getString("District1.text")); // NOI18N
        DistrictLabel1.setMaximumSize(new java.awt.Dimension(30, 40));
        DistrictLabel1.setMinimumSize(new java.awt.Dimension(30, 40));
        DistrictLabel1.setName("District1"); // NOI18N
        DistrictLabel1.setPreferredSize(new java.awt.Dimension(30, 40));
        DistrictLabel1.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                DistrictLabelMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                DistrictLabelMouseExited(evt);
            }
            public void mousePressed(java.awt.event.MouseEvent evt) {
                DistrictLabelMousePressed(evt);
            }
        });

        DistrictLabel2.setText(resourceMap.getString("DistrictLabel2.text")); // NOI18N
        DistrictLabel2.setMaximumSize(new java.awt.Dimension(30, 40));
        DistrictLabel2.setMinimumSize(new java.awt.Dimension(30, 40));
        DistrictLabel2.setName("DistrictLabel2"); // NOI18N
        DistrictLabel2.setPreferredSize(new java.awt.Dimension(30, 40));
        DistrictLabel2.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                DistrictLabelMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                DistrictLabelMouseExited(evt);
            }
            public void mousePressed(java.awt.event.MouseEvent evt) {
                DistrictLabelMousePressed(evt);
            }
        });

        DistrictLabel3.setText(resourceMap.getString("DistrictLabel3.text")); // NOI18N
        DistrictLabel3.setMaximumSize(new java.awt.Dimension(30, 40));
        DistrictLabel3.setMinimumSize(new java.awt.Dimension(30, 40));
        DistrictLabel3.setName("DistrictLabel3"); // NOI18N
        DistrictLabel3.setPreferredSize(new java.awt.Dimension(30, 40));
        DistrictLabel3.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                DistrictLabelMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                DistrictLabelMouseExited(evt);
            }
            public void mousePressed(java.awt.event.MouseEvent evt) {
                DistrictLabelMousePressed(evt);
            }
        });

        DistrictLabel4.setText(resourceMap.getString("DistrictLabel4.text")); // NOI18N
        DistrictLabel4.setMaximumSize(new java.awt.Dimension(30, 40));
        DistrictLabel4.setMinimumSize(new java.awt.Dimension(30, 40));
        DistrictLabel4.setName("DistrictLabel4"); // NOI18N
        DistrictLabel4.setPreferredSize(new java.awt.Dimension(30, 40));
        DistrictLabel4.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                DistrictLabelMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                DistrictLabelMouseExited(evt);
            }
            public void mousePressed(java.awt.event.MouseEvent evt) {
                DistrictLabelMousePressed(evt);
            }
        });

        DistrictLabel5.setText(resourceMap.getString("DistrictLabel5.text")); // NOI18N
        DistrictLabel5.setMaximumSize(new java.awt.Dimension(30, 40));
        DistrictLabel5.setMinimumSize(new java.awt.Dimension(30, 40));
        DistrictLabel5.setName("DistrictLabel5"); // NOI18N
        DistrictLabel5.setPreferredSize(new java.awt.Dimension(30, 40));
        DistrictLabel5.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                DistrictLabelMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                DistrictLabelMouseExited(evt);
            }
            public void mousePressed(java.awt.event.MouseEvent evt) {
                DistrictLabelMousePressed(evt);
            }
        });

        DistrictLabel6.setText(resourceMap.getString("DistrictLabel6.text")); // NOI18N
        DistrictLabel6.setMaximumSize(new java.awt.Dimension(30, 40));
        DistrictLabel6.setMinimumSize(new java.awt.Dimension(30, 40));
        DistrictLabel6.setName("DistrictLabel6"); // NOI18N
        DistrictLabel6.setPreferredSize(new java.awt.Dimension(30, 40));
        DistrictLabel6.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                DistrictLabelMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                DistrictLabelMouseExited(evt);
            }
            public void mousePressed(java.awt.event.MouseEvent evt) {
                DistrictLabelMousePressed(evt);
            }
        });

        DistrictLabel7.setText(resourceMap.getString("DistrictLabel7.text")); // NOI18N
        DistrictLabel7.setMaximumSize(new java.awt.Dimension(30, 40));
        DistrictLabel7.setMinimumSize(new java.awt.Dimension(30, 40));
        DistrictLabel7.setName("DistrictLabel7"); // NOI18N
        DistrictLabel7.setPreferredSize(new java.awt.Dimension(30, 40));
        DistrictLabel7.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                DistrictLabelMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                DistrictLabelMouseExited(evt);
            }
            public void mousePressed(java.awt.event.MouseEvent evt) {
                DistrictLabelMousePressed(evt);
            }
        });

        DistrictLabel8.setText(resourceMap.getString("DistrictLabel8.text")); // NOI18N
        DistrictLabel8.setMaximumSize(new java.awt.Dimension(30, 40));
        DistrictLabel8.setMinimumSize(new java.awt.Dimension(30, 40));
        DistrictLabel8.setName("DistrictLabel8"); // NOI18N
        DistrictLabel8.setPreferredSize(new java.awt.Dimension(30, 40));
        DistrictLabel8.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                DistrictLabelMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                DistrictLabelMouseExited(evt);
            }
            public void mousePressed(java.awt.event.MouseEvent evt) {
                DistrictLabelMousePressed(evt);
            }
        });

        GoldLabel.setText(resourceMap.getString("GoldLabel.text")); // NOI18N
        GoldLabel.setMaximumSize(new java.awt.Dimension(70, 13));
        GoldLabel.setMinimumSize(new java.awt.Dimension(65, 13));
        GoldLabel.setName("GoldLabel"); // NOI18N
        GoldLabel.setPreferredSize(new java.awt.Dimension(70, 13));

        CardsLabel.setText(resourceMap.getString("CardsLabel.text")); // NOI18N
        CardsLabel.setMaximumSize(new java.awt.Dimension(70, 13));
        CardsLabel.setMinimumSize(new java.awt.Dimension(70, 13));
        CardsLabel.setName("CardsLabel"); // NOI18N
        CardsLabel.setPreferredSize(new java.awt.Dimension(70, 13));

        CharacterLabel.setText(resourceMap.getString("CharacterLabel.text")); // NOI18N
        CharacterLabel.setMaximumSize(new java.awt.Dimension(110, 16));
        CharacterLabel.setMinimumSize(new java.awt.Dimension(110, 16));
        CharacterLabel.setName("CharacterLabel"); // NOI18N
        CharacterLabel.setPreferredSize(new java.awt.Dimension(110, 16));

        KingLabel.setText(resourceMap.getString("KingLabel.text")); // NOI18N
        KingLabel.setMaximumSize(new java.awt.Dimension(30, 16));
        KingLabel.setMinimumSize(new java.awt.Dimension(30, 15));
        KingLabel.setName("KingLabel"); // NOI18N
        KingLabel.setPreferredSize(new java.awt.Dimension(30, 15));

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(12, 12, 12)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(PlayerNameLabel, javax.swing.GroupLayout.DEFAULT_SIZE, 165, Short.MAX_VALUE)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(DistrictLabel5, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(DistrictLabel6, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(DistrictLabel7, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(DistrictLabel8, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(DistrictLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(12, 12, 12)
                                .addComponent(DistrictLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(DistrictLabel3, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(DistrictLabel4, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addGap(9, 9, 9))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(GoldLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 70, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(CardsLabel, javax.swing.GroupLayout.DEFAULT_SIZE, 88, Short.MAX_VALUE))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(CharacterLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 113, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(KingLabel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 10, Short.MAX_VALUE)))
                .addGap(11, 11, 11))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(8, 8, 8)
                .addComponent(PlayerNameLabel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(GoldLabel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(CardsLabel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(CharacterLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 17, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(KingLabel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(DistrictLabel3, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(DistrictLabel4, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(DistrictLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(DistrictLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(DistrictLabel5, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(DistrictLabel7, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(DistrictLabel8, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addComponent(DistrictLabel6, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(9, 9, 9))
        );
    }// </editor-fold>//GEN-END:initComponents

    private void DistrictLabelMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_DistrictLabelMouseEntered
        if (_city == null) return;
        
        try {
            int num = Integer.parseInt(((JLabel)evt.getComponent()).getText());
            //get district ability
            String districtAbility = "";
            if (_city.get(num).getEffect() != EFFECT.NONE) {
                //Museum
                if (_city.get(num).getEffect() == EFFECT.MUSEUM) {
                    districtAbility = "<br>" + _resourceMap.getString("term.museumCards") + ":" + _city.get(num).getMuseumCards().size();
                }
                districtAbility += "<br><br>" + _resourceMap.getString("district."+_city.get(num).getName()+".effect");
            }


            _view.setControlPanelDescription("<b>" + _resourceMap.getString("district."+_city.get(num).getName()+".name") +"</b>" + "<br>" +
                         _resourceMap.getString("term.cost") + " : " + _city.get(num).getCost() + districtAbility);
        } catch (Exception e) {}
    }//GEN-LAST:event_DistrictLabelMouseEntered

    private void DistrictLabelMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_DistrictLabelMouseExited
        //_view.setControlPanelDescription("");
    }//GEN-LAST:event_DistrictLabelMouseExited

    private void DistrictLabelMousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_DistrictLabelMousePressed
        if (evt.getComponent().isEnabled()) {
            try {
                 int num = Integer.parseInt(((JLabel)evt.getComponent()).getText());
                _view.districtSelected(_player.getID(), num);
            } catch (Exception e) {}
        }
    }//GEN-LAST:event_DistrictLabelMousePressed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel CardsLabel;
    private javax.swing.JLabel CharacterLabel;
    private javax.swing.JLabel DistrictLabel1;
    private javax.swing.JLabel DistrictLabel2;
    private javax.swing.JLabel DistrictLabel3;
    private javax.swing.JLabel DistrictLabel4;
    private javax.swing.JLabel DistrictLabel5;
    private javax.swing.JLabel DistrictLabel6;
    private javax.swing.JLabel DistrictLabel7;
    private javax.swing.JLabel DistrictLabel8;
    private javax.swing.JLabel GoldLabel;
    private javax.swing.JLabel KingLabel;
    private javax.swing.JLabel PlayerNameLabel;
    // End of variables declaration//GEN-END:variables

}
