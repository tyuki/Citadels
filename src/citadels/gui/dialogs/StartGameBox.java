/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/*
 * StartServerBox.java
 *
 * Created on 2010/01/05, 11:33:49
 */

package citadels.gui.dialogs;

import citadels.CitadelsApp;
import citadels.CitadelsView;
import citadels.game.District;
import citadels.game.District.EFFECT;


/**
 *
 * @author Personal
 */
public class StartGameBox extends javax.swing.JDialog {

    private org.jdesktop.application.ResourceMap _resourceMap;
    
    private CitadelsView _view;

    /** Creates new form StartServerBox */
    public StartGameBox(CitadelsView view, java.awt.Frame parent, boolean modal, int numPlayers) {
        super(parent, modal);
        initComponents();
        _view = view;
        ninethNoButton.setSelected(true);
        _resourceMap = org.jdesktop.application.Application.getInstance(citadels.CitadelsApp.class).getContext().getResourceMap(CitadelsApp.class);
        this.setTitle(_resourceMap.getString("dialog.startGame.title"));

        //Add 8 characters
        expansionCard1SBox.addItem(_resourceMap.getString("character.witch.name"));
        expansionCard1SBox.addItem(_resourceMap.getString("character.taxcollector.name"));
        expansionCard1SBox.addItem(_resourceMap.getString("character.wizard.name"));
        expansionCard1SBox.addItem(_resourceMap.getString("character.emperor.name"));
        expansionCard1SBox.addItem(_resourceMap.getString("character.abbot.name"));
        expansionCard1SBox.addItem(_resourceMap.getString("character.alchemist.name"));
        expansionCard1SBox.addItem(_resourceMap.getString("character.navigator.name"));
        expansionCard1SBox.addItem(_resourceMap.getString("character.diplomat.name"));

        expansionCard2SBox.addItem(_resourceMap.getString("character.witch.name"));
        expansionCard2SBox.addItem(_resourceMap.getString("character.taxcollector.name"));
        expansionCard2SBox.addItem(_resourceMap.getString("character.wizard.name"));
        expansionCard2SBox.addItem(_resourceMap.getString("character.emperor.name"));
        expansionCard2SBox.addItem(_resourceMap.getString("character.abbot.name"));
        expansionCard2SBox.addItem(_resourceMap.getString("character.alchemist.name"));
        expansionCard2SBox.addItem(_resourceMap.getString("character.navigator.name"));
        expansionCard2SBox.addItem(_resourceMap.getString("character.diplomat.name"));

        String[] exDistricts = new String[] {
            "poorhouse",
            "imperialtreasury",
            "park",
            "factory",
            "wishingwell",
            "maproom",
            "throneroom",
            "lighthouse",
            "belltower",
            "museum",
            "armory",
            "hospital",
            "quarry"
        };

        EFFECT[] exDistEffects = new EFFECT[] {
            EFFECT.POORHOUSE,
            EFFECT.IMPERIALTREASURY,
            EFFECT.PARK,
            EFFECT.FACTORY,
            EFFECT.WISHINGWELL,
            EFFECT.MAPROOM,
            EFFECT.THRONEROOM,
            EFFECT.LIGHTHOUSE,
            EFFECT.BELLTOWER,
            EFFECT.MUSEUM,
            EFFECT.ARMORY,
            EFFECT.HOSPITAL,
            EFFECT.QUARRY
        };

        //add extra districts
        for (int i = 0; i < exDistricts.length; i++) {
            extraDistrictCardCBox1.addItem(new DistrictNameEffectPair(_resourceMap.getString("district." + exDistricts[i] + ".name"), exDistEffects[i]));
            extraDistrictCardCBox2.addItem(new DistrictNameEffectPair(_resourceMap.getString("district." + exDistricts[i] + ".name"), exDistEffects[i]));
            extraDistrictCardCBox3.addItem(new DistrictNameEffectPair(_resourceMap.getString("district." + exDistricts[i] + ".name"), exDistEffects[i]));
        }

        if (numPlayers >= 8) {
            ninethYesButton.setSelected(true);
            ninethNoButton.setEnabled(false);
        }
        
    }
    
    public boolean getUse9th() {
        return  ninethYesButton.isSelected();

    }

    public boolean getIsShortGame() {
        return endingConditionSBox.getSelectedIndex() == 1;
    }

    public int[] getExpansionCards() {
        int ec1 = expansionCard1SBox.getSelectedIndex();
        int ec2 = expansionCard2SBox.getSelectedIndex();

        return new int[] {ec1, ec2};
    }

    public boolean isRandomExpansionCardsSelected() {
        return randomExpansionCardsButton.isSelected();
    }

    public boolean isRandomDistrictCardsSelected() {
        return randomExtraDistrictCardsButton.isSelected();
    }

    public EFFECT[] getSelectedExtraDistrictCards() {

        //Count number of cards
        int count = 0;
        if (extraDistrictCardCBox1.getSelectedIndex() > 0) count++;
        if (extraDistrictCardCBox2.getSelectedIndex() > 0) count++;
        if (extraDistrictCardCBox3.getSelectedIndex() > 0) count++;

        EFFECT[] districts = new EFFECT[count];

        //Add selected district to the return value
        int i = 0;
        if (extraDistrictCardCBox1.getSelectedIndex() > 0) {
            districts[i] = ((DistrictNameEffectPair)(extraDistrictCardCBox1.getSelectedItem())).effect;
            i++;
        }
        if (extraDistrictCardCBox2.getSelectedIndex() > 0) {
            districts[i] = ((DistrictNameEffectPair)(extraDistrictCardCBox2.getSelectedItem())).effect;
            i++;
        }
        if (extraDistrictCardCBox3.getSelectedIndex() > 0) {
            districts[i] = ((DistrictNameEffectPair)(extraDistrictCardCBox3.getSelectedItem())).effect;
            i++;
        }

        return districts;
    }

    public int getNumberOfRandomCharacterCards() {
        return  Integer.parseInt((String)(randomExpansionCardsCBox.getSelectedItem()));
    }

    public int getNumberOfRandomExtraCards() {
        return Integer.parseInt((String)(randomExtraDistrictCardsCBox.getSelectedItem()));
    }

    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        ninethCardGroup = new javax.swing.ButtonGroup();
        extraDistrictGroup = new javax.swing.ButtonGroup();
        expansionCardGroup = new javax.swing.ButtonGroup();
        gameSettingLabel = new javax.swing.JLabel();
        endingConditionLabel = new javax.swing.JLabel();
        characterSettingLabel = new javax.swing.JLabel();
        ninethCardLabel = new javax.swing.JLabel();
        expansionCard1Label = new javax.swing.JLabel();
        expansionCard2Label = new javax.swing.JLabel();
        selectExpansionCardsButton = new javax.swing.JRadioButton();
        randomExpansionCardsButton = new javax.swing.JRadioButton();
        randomExpansionCardsCBox = new javax.swing.JComboBox();
        randomExpansionCardsNumLabel = new javax.swing.JLabel();
        endingConditionSBox = new javax.swing.JComboBox();
        ninethNoButton = new javax.swing.JRadioButton();
        ninethYesButton = new javax.swing.JRadioButton();
        expansionCard1SBox = new javax.swing.JComboBox();
        expansionCard2SBox = new javax.swing.JComboBox();
        districtSettingLabel = new javax.swing.JLabel();
        selectExtraDistrictCardsButton = new javax.swing.JRadioButton();
        randomExtraDistrictCardsButton = new javax.swing.JRadioButton();
        randomExtraDistrictCardsCBox = new javax.swing.JComboBox();
        randomExtraDistrictCardsNumLabel = new javax.swing.JLabel();
        selectExtraDistrictCardLabel1 = new javax.swing.JLabel();
        selectExtraDistrictCardLabel2 = new javax.swing.JLabel();
        selectExtraDistrictCardLabel3 = new javax.swing.JLabel();
        extraDistrictCardCBox1 = new javax.swing.JComboBox();
        extraDistrictCardCBox2 = new javax.swing.JComboBox();
        extraDistrictCardCBox3 = new javax.swing.JComboBox();
        startButton = new javax.swing.JButton();
        cancelButton = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setName("Form"); // NOI18N
        setResizable(false);

        org.jdesktop.application.ResourceMap resourceMap = org.jdesktop.application.Application.getInstance(citadels.CitadelsApp.class).getContext().getResourceMap(StartGameBox.class);
        gameSettingLabel.setText(resourceMap.getString("dialog.startGame.gameSetting")); // NOI18N
        gameSettingLabel.setName("gameSettingLabel"); // NOI18N

        endingConditionLabel.setText(resourceMap.getString("dialog.startGame.endingCondition")); // NOI18N
        endingConditionLabel.setName("endingConditionLabel"); // NOI18N

        characterSettingLabel.setText(resourceMap.getString("dialog.startGame.characterSetting")); // NOI18N
        characterSettingLabel.setName("characterSettingLabel"); // NOI18N

        ninethCardLabel.setText(resourceMap.getString("dialog.startGame.ninethCard")); // NOI18N
        ninethCardLabel.setName("ninethCardLabel"); // NOI18N

        expansionCard1Label.setText(resourceMap.getString("dialog.startGame.expansionCard1")); // NOI18N
        expansionCard1Label.setName("expansionCard1Label"); // NOI18N

        expansionCard2Label.setText(resourceMap.getString("dialog.startGame.expansionCard2")); // NOI18N
        expansionCard2Label.setName("expansionCard2Label"); // NOI18N

        expansionCardGroup.add(selectExpansionCardsButton);
        selectExpansionCardsButton.setSelected(true);
        selectExpansionCardsButton.setText(resourceMap.getString("dialog.startGame.selectExpansionCards")); // NOI18N
        selectExpansionCardsButton.setName("selectExpansionCardsButton"); // NOI18N

        expansionCardGroup.add(randomExpansionCardsButton);
        randomExpansionCardsButton.setText(resourceMap.getString("dialog.startGame.randomExpansionCards")); // NOI18N
        randomExpansionCardsButton.setName("randomExpansionCardsButton"); // NOI18N

        randomExpansionCardsCBox.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "1", "2", "3" }));
        randomExpansionCardsCBox.setName("randomExpansionCardsCBox"); // NOI18N
        randomExpansionCardsCBox.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                randomExpansionCardsCBoxActionPerformed(evt);
            }
        });

        randomExpansionCardsNumLabel.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        randomExpansionCardsNumLabel.setText(resourceMap.getString("dialog.startGame.randomExpansionCardsNum")); // NOI18N
        randomExpansionCardsNumLabel.setName("randomExpansionCardsNumLabel"); // NOI18N

        endingConditionSBox.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "8", "7" }));
        endingConditionSBox.setName("endingConditionSBox"); // NOI18N

        ninethCardGroup.add(ninethNoButton);
        ninethNoButton.setText(resourceMap.getString("dialog.startGame.ninethNo")); // NOI18N
        ninethNoButton.setName("ninethNoButton"); // NOI18N
        ninethNoButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ninethNoButtonActionPerformed(evt);
            }
        });

        ninethCardGroup.add(ninethYesButton);
        ninethYesButton.setText(resourceMap.getString("dialog.startGame.ninethYes")); // NOI18N
        ninethYesButton.setName("ninethYesButton"); // NOI18N
        ninethYesButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ninethYesButtonActionPerformed(evt);
            }
        });

        expansionCard1SBox.setMaximumRowCount(10);
        expansionCard1SBox.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "-" }));
        expansionCard1SBox.setName("expansionCard1SBox"); // NOI18N
        expansionCard1SBox.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                expansionCard1SBoxActionPerformed(evt);
            }
        });

        expansionCard2SBox.setMaximumRowCount(10);
        expansionCard2SBox.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "-" }));
        expansionCard2SBox.setName("expansionCard2SBox"); // NOI18N
        expansionCard2SBox.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                expansionCard2SBoxActionPerformed(evt);
            }
        });

        districtSettingLabel.setText(resourceMap.getString("dialog.startGame.districtSetting")); // NOI18N
        districtSettingLabel.setName("districtSettingLabel"); // NOI18N

        extraDistrictGroup.add(selectExtraDistrictCardsButton);
        selectExtraDistrictCardsButton.setSelected(true);
        selectExtraDistrictCardsButton.setText(resourceMap.getString("dialog.startGame.selectExtraDistrictCards")); // NOI18N
        selectExtraDistrictCardsButton.setName("selectExtraDistrictCardsButton"); // NOI18N

        extraDistrictGroup.add(randomExtraDistrictCardsButton);
        randomExtraDistrictCardsButton.setText(resourceMap.getString("dialog.startGame.randomExtraDistrictCards")); // NOI18N
        randomExtraDistrictCardsButton.setName("randomExtraDistrictCardsButton"); // NOI18N

        randomExtraDistrictCardsCBox.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "1", "2", "3" }));
        randomExtraDistrictCardsCBox.setName("randomExtraDistrictCardsCBox"); // NOI18N
        randomExtraDistrictCardsCBox.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                randomExtraDistrictCardsCBoxActionPerformed(evt);
            }
        });

        randomExtraDistrictCardsNumLabel.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        randomExtraDistrictCardsNumLabel.setText(resourceMap.getString("dialog.startGame.randomExtraDistrictCardsNum")); // NOI18N
        randomExtraDistrictCardsNumLabel.setName("randomExtraDistrictCardsNumLabel"); // NOI18N

        selectExtraDistrictCardLabel1.setText(resourceMap.getString("dialog.startGame.selectExtraDistrictCard1")); // NOI18N
        selectExtraDistrictCardLabel1.setName("selectExtraDistrictCardLabel1"); // NOI18N

        selectExtraDistrictCardLabel2.setText(resourceMap.getString("dialog.startGame.selectExtraDistrictCard2")); // NOI18N
        selectExtraDistrictCardLabel2.setName("selectExtraDistrictCardLabel2"); // NOI18N

        selectExtraDistrictCardLabel3.setText(resourceMap.getString("dialog.startGame.selectExtraDistrictCard3")); // NOI18N
        selectExtraDistrictCardLabel3.setName("selectExtraDistrictCardLabel3"); // NOI18N

        extraDistrictCardCBox1.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "-" }));
        extraDistrictCardCBox1.setName("extraDistrictCardCBox1"); // NOI18N
        extraDistrictCardCBox1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                extraDistrictCardCBox1ActionPerformed(evt);
            }
        });

        extraDistrictCardCBox2.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "-" }));
        extraDistrictCardCBox2.setName("extraDistrictCardCBox2"); // NOI18N
        extraDistrictCardCBox2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                extraDistrictCardCBox2ActionPerformed(evt);
            }
        });

        extraDistrictCardCBox3.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "-" }));
        extraDistrictCardCBox3.setName("extraDistrictCardCBox3"); // NOI18N
        extraDistrictCardCBox3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                extraDistrictCardCBox3ActionPerformed(evt);
            }
        });

        startButton.setText(resourceMap.getString("startButton.text")); // NOI18N
        startButton.setActionCommand(resourceMap.getString("startButton.text")); // NOI18N
        startButton.setName("startButton"); // NOI18N
        startButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                startButtonActionPerformed(evt);
            }
        });

        cancelButton.setText(resourceMap.getString("cancelButton.text")); // NOI18N
        cancelButton.setName("cancelButton"); // NOI18N
        cancelButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cancelButtonActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(gameSettingLabel)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(endingConditionLabel)
                                .addGap(18, 18, 18)
                                .addComponent(endingConditionSBox, javax.swing.GroupLayout.PREFERRED_SIZE, 38, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addContainerGap())
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addGroup(javax.swing.GroupLayout.Alignment.LEADING, layout.createSequentialGroup()
                                .addComponent(selectExtraDistrictCardLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 90, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(extraDistrictCardCBox1, 0, 156, Short.MAX_VALUE))
                            .addComponent(selectExtraDistrictCardsButton, javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(javax.swing.GroupLayout.Alignment.LEADING, layout.createSequentialGroup()
                                .addComponent(selectExtraDistrictCardLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 90, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(extraDistrictCardCBox2, 0, 156, Short.MAX_VALUE))
                            .addGroup(javax.swing.GroupLayout.Alignment.LEADING, layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                .addComponent(randomExtraDistrictCardsButton)
                                .addGroup(layout.createSequentialGroup()
                                    .addComponent(startButton)
                                    .addGap(18, 18, 18)
                                    .addComponent(cancelButton)))
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(randomExtraDistrictCardsNumLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 150, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, 18)
                                .addComponent(randomExtraDistrictCardsCBox, javax.swing.GroupLayout.PREFERRED_SIZE, 38, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(javax.swing.GroupLayout.Alignment.LEADING, layout.createSequentialGroup()
                                .addComponent(selectExtraDistrictCardLabel3, javax.swing.GroupLayout.PREFERRED_SIZE, 90, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(12, 12, 12)
                                .addComponent(extraDistrictCardCBox3, 0, 156, Short.MAX_VALUE))
                            .addGroup(javax.swing.GroupLayout.Alignment.LEADING, layout.createSequentialGroup()
                                .addComponent(districtSettingLabel)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 180, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(javax.swing.GroupLayout.Alignment.LEADING, layout.createSequentialGroup()
                                .addComponent(characterSettingLabel)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 168, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addGap(16, 16, 16))
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(selectExpansionCardsButton, javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(javax.swing.GroupLayout.Alignment.LEADING, layout.createSequentialGroup()
                                .addComponent(ninethCardLabel)
                                .addGap(35, 35, 35)
                                .addComponent(ninethNoButton)
                                .addGap(18, 18, 18)
                                .addComponent(ninethYesButton))
                            .addComponent(randomExpansionCardsButton, javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(randomExpansionCardsNumLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 150, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, 18)
                                .addComponent(randomExpansionCardsCBox, javax.swing.GroupLayout.PREFERRED_SIZE, 38, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(javax.swing.GroupLayout.Alignment.LEADING, layout.createSequentialGroup()
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addComponent(expansionCard1Label, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(expansionCard2Label))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                    .addComponent(expansionCard2SBox, 0, 152, Short.MAX_VALUE)
                                    .addComponent(expansionCard1SBox, 0, 152, Short.MAX_VALUE))))
                        .addGap(20, 20, 20))))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(gameSettingLabel)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(endingConditionLabel)
                    .addComponent(endingConditionSBox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(characterSettingLabel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(ninethCardLabel)
                    .addComponent(ninethNoButton)
                    .addComponent(ninethYesButton))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(selectExpansionCardsButton)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(expansionCard1Label)
                    .addComponent(expansionCard1SBox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(expansionCard2Label)
                    .addComponent(expansionCard2SBox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(randomExpansionCardsButton)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(randomExpansionCardsCBox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(randomExpansionCardsNumLabel))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(districtSettingLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 17, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(selectExtraDistrictCardsButton)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(extraDistrictCardCBox1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(selectExtraDistrictCardLabel1))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(selectExtraDistrictCardLabel2)
                    .addComponent(extraDistrictCardCBox2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(extraDistrictCardCBox3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(selectExtraDistrictCardLabel3))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(randomExtraDistrictCardsButton)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(randomExtraDistrictCardsCBox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(randomExtraDistrictCardsNumLabel))
                .addGap(10, 10, 10)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(startButton)
                    .addComponent(cancelButton))
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void cancelButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cancelButtonActionPerformed
        this.dispose();
    }//GEN-LAST:event_cancelButtonActionPerformed

    private void ninethNoButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ninethNoButtonActionPerformed
        if (expansionCard1SBox.getSelectedIndex() == expansionCard1SBox.getItemCount()-1) expansionCard1SBox.setSelectedIndex(0);
        if (expansionCard2SBox.getSelectedIndex() == expansionCard1SBox.getItemCount()-1) expansionCard2SBox.setSelectedIndex(0);

        expansionCard1SBox.removeItemAt(9);
        expansionCard2SBox.removeItemAt(9);

    }//GEN-LAST:event_ninethNoButtonActionPerformed

    private void ninethYesButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ninethYesButtonActionPerformed
        expansionCard1SBox.addItem(_resourceMap.getString("character.artist.name"));
        expansionCard2SBox.addItem(_resourceMap.getString("character.artist.name"));
    }//GEN-LAST:event_ninethYesButtonActionPerformed

    private void expansionCard1SBoxActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_expansionCard1SBoxActionPerformed
        if (expansionCard1SBox.getSelectedIndex() == expansionCard2SBox.getSelectedIndex()) {
            expansionCard1SBox.setSelectedIndex(0);
        }
    }//GEN-LAST:event_expansionCard1SBoxActionPerformed

    private void expansionCard2SBoxActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_expansionCard2SBoxActionPerformed
        if (expansionCard2SBox.getSelectedIndex() == expansionCard1SBox.getSelectedIndex()) {
            expansionCard2SBox.setSelectedIndex(0);
        }
    }//GEN-LAST:event_expansionCard2SBoxActionPerformed

    private void startButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_startButtonActionPerformed
        if (_view.startGame()) {
            this.dispose();
        }
    }//GEN-LAST:event_startButtonActionPerformed

    private void extraDistrictCardCBox1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_extraDistrictCardCBox1ActionPerformed
        if (extraDistrictCardCBox1.getSelectedIndex() == extraDistrictCardCBox2.getSelectedIndex() ||
            extraDistrictCardCBox1.getSelectedIndex() == extraDistrictCardCBox3.getSelectedIndex()) {
            extraDistrictCardCBox1.setSelectedIndex(0);
        }
    }//GEN-LAST:event_extraDistrictCardCBox1ActionPerformed

    private void extraDistrictCardCBox2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_extraDistrictCardCBox2ActionPerformed
        if (extraDistrictCardCBox2.getSelectedIndex() == extraDistrictCardCBox1.getSelectedIndex() ||
            extraDistrictCardCBox2.getSelectedIndex() == extraDistrictCardCBox3.getSelectedIndex()) {
            extraDistrictCardCBox2.setSelectedIndex(0);
        }
    }//GEN-LAST:event_extraDistrictCardCBox2ActionPerformed

    private void extraDistrictCardCBox3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_extraDistrictCardCBox3ActionPerformed
        if (extraDistrictCardCBox3.getSelectedIndex() == extraDistrictCardCBox1.getSelectedIndex() ||
            extraDistrictCardCBox3.getSelectedIndex() == extraDistrictCardCBox2.getSelectedIndex()) {
            extraDistrictCardCBox3.setSelectedIndex(0);
        }
    }//GEN-LAST:event_extraDistrictCardCBox3ActionPerformed

    private void randomExtraDistrictCardsCBoxActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_randomExtraDistrictCardsCBoxActionPerformed
        randomExtraDistrictCardsButton.setSelected(true);
    }//GEN-LAST:event_randomExtraDistrictCardsCBoxActionPerformed

    private void randomExpansionCardsCBoxActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_randomExpansionCardsCBoxActionPerformed
        randomExpansionCardsButton.setSelected(true);
    }//GEN-LAST:event_randomExpansionCardsCBoxActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton cancelButton;
    private javax.swing.JLabel characterSettingLabel;
    private javax.swing.JLabel districtSettingLabel;
    private javax.swing.JLabel endingConditionLabel;
    private javax.swing.JComboBox endingConditionSBox;
    private javax.swing.JLabel expansionCard1Label;
    private javax.swing.JComboBox expansionCard1SBox;
    private javax.swing.JLabel expansionCard2Label;
    private javax.swing.JComboBox expansionCard2SBox;
    private javax.swing.ButtonGroup expansionCardGroup;
    private javax.swing.JComboBox extraDistrictCardCBox1;
    private javax.swing.JComboBox extraDistrictCardCBox2;
    private javax.swing.JComboBox extraDistrictCardCBox3;
    private javax.swing.ButtonGroup extraDistrictGroup;
    private javax.swing.JLabel gameSettingLabel;
    private javax.swing.ButtonGroup ninethCardGroup;
    private javax.swing.JLabel ninethCardLabel;
    private javax.swing.JRadioButton ninethNoButton;
    private javax.swing.JRadioButton ninethYesButton;
    private javax.swing.JRadioButton randomExpansionCardsButton;
    private javax.swing.JComboBox randomExpansionCardsCBox;
    private javax.swing.JLabel randomExpansionCardsNumLabel;
    private javax.swing.JRadioButton randomExtraDistrictCardsButton;
    private javax.swing.JComboBox randomExtraDistrictCardsCBox;
    private javax.swing.JLabel randomExtraDistrictCardsNumLabel;
    private javax.swing.JRadioButton selectExpansionCardsButton;
    private javax.swing.JLabel selectExtraDistrictCardLabel1;
    private javax.swing.JLabel selectExtraDistrictCardLabel2;
    private javax.swing.JLabel selectExtraDistrictCardLabel3;
    private javax.swing.JRadioButton selectExtraDistrictCardsButton;
    private javax.swing.JButton startButton;
    // End of variables declaration//GEN-END:variables

    private class DistrictNameEffectPair {
        public final String name;
        public final District.EFFECT effect;

        public DistrictNameEffectPair(String name, EFFECT effect) {
            this.name = name;
            this.effect = effect;
        }

        @Override
        public String toString() {
            return name;
        }
    }
}
