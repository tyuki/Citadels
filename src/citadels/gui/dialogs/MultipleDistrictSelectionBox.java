/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/*
 * SingleDistrictSelectionBox.java
 *
 * Created on 2010/01/07, 9:10:51
 */

package citadels.gui.dialogs;

import citadels.CitadelsApp;
import citadels.CitadelsView;
import citadels.game.District;
import citadels.game.District.EFFECT;
import citadels.gui.IconContainer;
import java.util.LinkedList;
import java.util.List;
import javax.swing.JLabel;
import javax.swing.SwingConstants;

/**
 *
 * @author Personal
 */
public class MultipleDistrictSelectionBox extends javax.swing.JDialog {

    private CitadelsView _view;

    private org.jdesktop.application.ResourceMap _resourceMap;
   private List<JLabel> _districtCardLabels;

    private MDS_MODE _mode;
    private int _shift = 0;
    private final List<District> _districts;
    private boolean[] _selected;

    public static enum MDS_MODE {
        ACTION_CARDS,
        MAGICIAN
    }

    /** Creates new form SingleDistrictSelectionBox */
    public MultipleDistrictSelectionBox(CitadelsView view, java.awt.Frame parent, boolean modal, MDS_MODE mode, List<District> districts) {
        super(parent, modal);
        initComponents();

        _view = view;
        _resourceMap = org.jdesktop.application.Application.getInstance(citadels.CitadelsApp.class).getContext().getResourceMap(CitadelsApp.class);
        _mode = mode;
        _districts = districts;

        //initialize selected
        _selected = new boolean[_districts.size()];
        for (int i = 0; i < _selected.length; i++) {
            _selected[i] = false;
        }

        //Initialize list of labels
        _districtCardLabels = new LinkedList<JLabel>();
        _districtCardLabels.add(cardLabel1);
        _districtCardLabels.add(cardLabel2);
        _districtCardLabels.add(cardLabel3);
        _districtCardLabels.add(cardLabel4);

        //Place districts
        _shift = 0;
        setDistricts();

        //Shift buttons
        if (_districts.size() > _districtCardLabels.size()) {
            rightShiftButton.setEnabled(true);
        }

        //Mode specific setup
        switch (mode) {
            case ACTION_CARDS:
                this.setTitle(_resourceMap.getString("dialog.mdsActionCards.title"));
                selectMultipleDistrictLabel.setText(_resourceMap.getString("dialog.mdsActionCards.title"));
                break;
            case MAGICIAN:
                this.setTitle(_resourceMap.getString("dialog.mdsMagician.title"));
                selectMultipleDistrictLabel.setText(_resourceMap.getString("dialog.mdsMagician.title"));
                cancelButton.setEnabled(true);
                break;
        }
    }

    private void setDistricts() {

        //When number of labels is enough to show all
        if (_districts.size() <= _districtCardLabels.size()) {
            for (int i = 0; i < _districts.size(); i++) {
                _districtCardLabels.get(i).setIcon(IconContainer.getIcon(_districts.get(i).getName()));
                _districtCardLabels.get(i).setText(Integer.toString(i));
                _districtCardLabels.get(i).setEnabled(true);
                if (_selected[i]) {
                    _districtCardLabels.get(i).setVerticalAlignment(SwingConstants.TOP);
                } else {
                    _districtCardLabels.get(i).setVerticalAlignment(SwingConstants.BOTTOM);
                }
            }
            for (int i = _districts.size(); i < _districtCardLabels.size(); i++) {
                _districtCardLabels.get(i).setIcon(null);
                _districtCardLabels.get(i).setText("");
                _districtCardLabels.get(i).setEnabled(false);
            }
        } else {
            for (int i = 0; i < _districtCardLabels.size(); i++) {
                try {
                    _districtCardLabels.get(i).setIcon(IconContainer.getIcon(_districts.get(i+_shift).getName()));
                    _districtCardLabels.get(i).setText(Integer.toString(i+_shift));
                   _districtCardLabels.get(i).setEnabled(true);
                     if (_selected[i+_shift]) {
                        _districtCardLabels.get(i).setVerticalAlignment(SwingConstants.TOP);
                    } else {
                        _districtCardLabels.get(i).setVerticalAlignment(SwingConstants.BOTTOM);
                    }
                } catch (NullPointerException npe) {
                    npe.printStackTrace();
                }
            }
        }

        //Count selected cards
        int count = 0;
        for (boolean b : _selected) {
            if (b) count++;
        }
        if (count > 0) {
            okButton.setEnabled(true);
        } else {
            okButton.setEnabled(false);
        }

        if (_mode == MDS_MODE.ACTION_CARDS) {
            if (count > 2) okButton.setEnabled(false);
        }

        //Show the number of the label
        selectedStateLabel.setText(_resourceMap.getString("dialog.mdsSelectedNumber") + " : " + Integer.toString(count));
    }

    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        selectMultipleDistrictLabel = new javax.swing.JLabel();
        cardLabel1 = new javax.swing.JLabel();
        cardLabel2 = new javax.swing.JLabel();
        cardLabel3 = new javax.swing.JLabel();
        cardLabel4 = new javax.swing.JLabel();
        leftShiftButton = new javax.swing.JButton();
        rightShiftButton = new javax.swing.JButton();
        jScrollPane1 = new javax.swing.JScrollPane();
        districtDescriptionPane = new javax.swing.JTextPane();
        okButton = new javax.swing.JButton();
        cancelButton = new javax.swing.JButton();
        selectedStateLabel = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.DO_NOTHING_ON_CLOSE);
        setName("Form"); // NOI18N
        setResizable(false);

        org.jdesktop.application.ResourceMap resourceMap = org.jdesktop.application.Application.getInstance(citadels.CitadelsApp.class).getContext().getResourceMap(MultipleDistrictSelectionBox.class);
        selectMultipleDistrictLabel.setFont(resourceMap.getFont("dialog.title.font")); // NOI18N
        selectMultipleDistrictLabel.setText(resourceMap.getString("selectMultipleDistrictLabel.text")); // NOI18N
        selectMultipleDistrictLabel.setName("selectMultipleDistrictLabel"); // NOI18N

        cardLabel1.setText(resourceMap.getString("cardLabel1.text")); // NOI18N
        cardLabel1.setVerticalAlignment(javax.swing.SwingConstants.BOTTOM);
        cardLabel1.setMaximumSize(new java.awt.Dimension(60, 120));
        cardLabel1.setMinimumSize(new java.awt.Dimension(60, 120));
        cardLabel1.setName("cardLabel1"); // NOI18N
        cardLabel1.setPreferredSize(new java.awt.Dimension(60, 120));
        cardLabel1.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                cardLabelMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                cardLabelMouseExited(evt);
            }
            public void mousePressed(java.awt.event.MouseEvent evt) {
                cardLabelMousePressed(evt);
            }
        });

        cardLabel2.setVerticalAlignment(javax.swing.SwingConstants.BOTTOM);
        cardLabel2.setMaximumSize(new java.awt.Dimension(60, 120));
        cardLabel2.setMinimumSize(new java.awt.Dimension(60, 120));
        cardLabel2.setName("cardLabel2"); // NOI18N
        cardLabel2.setPreferredSize(new java.awt.Dimension(60, 120));
        cardLabel2.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                cardLabelMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                cardLabelMouseExited(evt);
            }
            public void mousePressed(java.awt.event.MouseEvent evt) {
                cardLabelMousePressed(evt);
            }
        });

        cardLabel3.setVerticalAlignment(javax.swing.SwingConstants.BOTTOM);
        cardLabel3.setMaximumSize(new java.awt.Dimension(60, 120));
        cardLabel3.setMinimumSize(new java.awt.Dimension(60, 120));
        cardLabel3.setName("cardLabel3"); // NOI18N
        cardLabel3.setPreferredSize(new java.awt.Dimension(60, 120));
        cardLabel3.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                cardLabelMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                cardLabelMouseExited(evt);
            }
            public void mousePressed(java.awt.event.MouseEvent evt) {
                cardLabelMousePressed(evt);
            }
        });

        cardLabel4.setVerticalAlignment(javax.swing.SwingConstants.BOTTOM);
        cardLabel4.setMaximumSize(new java.awt.Dimension(60, 120));
        cardLabel4.setMinimumSize(new java.awt.Dimension(60, 120));
        cardLabel4.setName("cardLabel4"); // NOI18N
        cardLabel4.setPreferredSize(new java.awt.Dimension(60, 120));
        cardLabel4.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                cardLabelMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                cardLabelMouseExited(evt);
            }
            public void mousePressed(java.awt.event.MouseEvent evt) {
                cardLabelMousePressed(evt);
            }
        });

        leftShiftButton.setText(resourceMap.getString("leftShiftButton.text")); // NOI18N
        leftShiftButton.setEnabled(false);
        leftShiftButton.setName("leftShiftButton"); // NOI18N
        leftShiftButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                leftShiftButtonActionPerformed(evt);
            }
        });

        rightShiftButton.setText(resourceMap.getString("rightShiftButton.text")); // NOI18N
        rightShiftButton.setEnabled(false);
        rightShiftButton.setName("rightShiftButton"); // NOI18N
        rightShiftButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                rightShiftButtonActionPerformed(evt);
            }
        });

        jScrollPane1.setName("jScrollPane1"); // NOI18N

        districtDescriptionPane.setBackground(new java.awt.Color(240, 240, 240));
        districtDescriptionPane.setContentType("text/html"); // NOI18N
        districtDescriptionPane.setEditable(false);
        districtDescriptionPane.setFont(resourceMap.getFont("descriptionPane.font")); // NOI18N
        districtDescriptionPane.setFocusable(false);
        districtDescriptionPane.setName("districtDescriptionPane"); // NOI18N
        jScrollPane1.setViewportView(districtDescriptionPane);

        okButton.setText(resourceMap.getString("okButton.text")); // NOI18N
        okButton.setEnabled(false);
        okButton.setName("okButton"); // NOI18N
        okButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                okButtonActionPerformed(evt);
            }
        });

        cancelButton.setText(resourceMap.getString("cancelButton.text")); // NOI18N
        cancelButton.setEnabled(false);
        cancelButton.setName("cancelButton"); // NOI18N
        cancelButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cancelButtonActionPerformed(evt);
            }
        });

        selectedStateLabel.setFont(resourceMap.getFont("selectedStateLabel.font")); // NOI18N
        selectedStateLabel.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        selectedStateLabel.setText(resourceMap.getString("selectedStateLabel.text")); // NOI18N
        selectedStateLabel.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        selectedStateLabel.setName("selectedStateLabel"); // NOI18N

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(selectMultipleDistrictLabel, javax.swing.GroupLayout.DEFAULT_SIZE, 261, Short.MAX_VALUE)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 261, Short.MAX_VALUE)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(leftShiftButton)
                            .addComponent(cardLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(selectedStateLabel, javax.swing.GroupLayout.DEFAULT_SIZE, 127, Short.MAX_VALUE)
                                .addGap(28, 28, 28)
                                .addComponent(rightShiftButton))
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(cardLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(cardLabel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(cardLabel4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addComponent(okButton)
                        .addGap(44, 44, 44)
                        .addComponent(cancelButton)
                        .addGap(50, 50, 50)))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(selectMultipleDistrictLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(cardLabel1, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cardLabel2, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 124, Short.MAX_VALUE)
                    .addComponent(cardLabel3, javax.swing.GroupLayout.DEFAULT_SIZE, 124, Short.MAX_VALUE)
                    .addComponent(cardLabel4, javax.swing.GroupLayout.DEFAULT_SIZE, 124, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(leftShiftButton)
                        .addComponent(selectedStateLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 21, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(rightShiftButton))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 105, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(cancelButton)
                    .addComponent(okButton))
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void cardLabelMousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_cardLabelMousePressed
        if (evt.getComponent().isEnabled()) {
            try {
                int i = Integer.parseInt(((JLabel)evt.getComponent()).getText());
                _selected[i]  = !_selected[i];
                setDistricts();
            } catch (Exception e) {}
        }
    }//GEN-LAST:event_cardLabelMousePressed

    private void leftShiftButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_leftShiftButtonActionPerformed
        //Shift the displayed cards to the left
        if (_shift > 0) _shift--;
        //Disable the button if the beginning of the hand reached
        if (_shift == 0) leftShiftButton.setEnabled(false);
        //Enable the right shift when there is still room to move right
        if (_shift < _districts.size()-_districtCardLabels.size()) rightShiftButton.setEnabled(true);
        setDistricts();
    }//GEN-LAST:event_leftShiftButtonActionPerformed

    private void rightShiftButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_rightShiftButtonActionPerformed
                if (_shift < _districts.size()-_districtCardLabels.size()) _shift++;
        //Disable the button if the end of the hand reached
        if (_shift == _districts.size()-_districtCardLabels.size()) rightShiftButton.setEnabled(false);
        //Enable the left shift when moved to the right at least by one
        if (_shift > 0) leftShiftButton.setEnabled(true);
        setDistricts();
    }//GEN-LAST:event_rightShiftButtonActionPerformed

    private void okButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_okButtonActionPerformed
        this.setVisible(false);

        List<Integer> sCards = new LinkedList<Integer>();
        for (int i = 0; i < _selected.length; i++) {
            if (_selected[i]) sCards.add(i);
        }

        int[] cards = new int[sCards.size()];
        for (int i = 0; i < cards.length; i++) {
            cards[i] = sCards.get(i);
        }

        switch (_mode) {
            case ACTION_CARDS:
                _view.actionCardsSelected(cards);
                break;
            case MAGICIAN:
                _view.magicianExchangeSelected(cards);
                break;
        }
    }//GEN-LAST:event_okButtonActionPerformed

    private void cardLabelMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_cardLabelMouseEntered

        if (evt.getComponent().isEnabled()) {
            int entered = Integer.parseInt(((JLabel)evt.getComponent()).getText());
            //get district ability
            String districtAbility;
            if (_districts.get(entered).getEffect() == EFFECT.NONE) {
                districtAbility = _resourceMap.getString("dialog.sds.description.none");
            } else {
                districtAbility = _resourceMap.getString("district."+_districts.get(entered).getName()+".effect");
            }

            //set name and description
            districtDescriptionPane.setText(_resourceMap.getString("district."+_districts.get(entered).getName()+".name") + "<br>"  +
                                        _resourceMap.getString("term.cost")+" : "+_districts.get(entered).getCost() + " " +
                                        _resourceMap.getString("term.score")+" : "+_districts.get(entered).getScore() + "<br>" +
                                        _resourceMap.getString("dialog.sds.description.districtAbility")+" : " + districtAbility);
        }
    }//GEN-LAST:event_cardLabelMouseEntered

    private void cardLabelMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_cardLabelMouseExited
        if (evt.getComponent().isEnabled()) {
            districtDescriptionPane.setText("");
        }
    }//GEN-LAST:event_cardLabelMouseExited

    private void cancelButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cancelButtonActionPerformed
        this.dispose();
    }//GEN-LAST:event_cancelButtonActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton cancelButton;
    private javax.swing.JLabel cardLabel1;
    private javax.swing.JLabel cardLabel2;
    private javax.swing.JLabel cardLabel3;
    private javax.swing.JLabel cardLabel4;
    private javax.swing.JTextPane districtDescriptionPane;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JButton leftShiftButton;
    private javax.swing.JButton okButton;
    private javax.swing.JButton rightShiftButton;
    private javax.swing.JLabel selectMultipleDistrictLabel;
    private javax.swing.JLabel selectedStateLabel;
    // End of variables declaration//GEN-END:variables

}
