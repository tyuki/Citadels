/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/*
 * SingleDistrictSelectionBox.java
 *
 * Created on 2010/01/07, 9:10:51
 */

package citadels.gui.dialogs;

import citadels.CitadelsApp;
import citadels.CitadelsView;
import citadels.game.District;
import citadels.game.District.EFFECT;
import citadels.gui.IconContainer;
import java.util.LinkedList;
import java.util.List;
import javax.swing.JLabel;

/**
 *
 * @author Personal
 */
public class SingleDistrictSelectionBox extends javax.swing.JDialog {

    private CitadelsView _view;

    private org.jdesktop.application.ResourceMap _resourceMap;
    private List<JLabel> _districtCardLabels;

    private SDS_MODE _mode;
    public int _selected = 0;
    private int _shift = 0;
    private final List<District> _districts;
    private final int _optionalNum;

    public static enum SDS_MODE {
        ACTION_CARDS, LABORATORY, WIZARD, LIGHTHOUSE, MUSEUM
    }

    /** Creates new form SingleDistrictSelectionBox */
    public SingleDistrictSelectionBox(CitadelsView view, java.awt.Frame parent, boolean modal, SDS_MODE mode, List<District> districts, int opt) {
        super(parent, modal);
        initComponents();
        _districts = districts;
        _optionalNum = opt;
        initialize(view, mode);
    }

    public SingleDistrictSelectionBox(CitadelsView view, java.awt.Frame parent, boolean modal, SDS_MODE mode, List<District> districts) {
        super(parent, modal);
        initComponents();
        _districts = districts;
        _optionalNum = -1;
        initialize(view, mode);
    }

    public void initialize(CitadelsView view, SDS_MODE mode) {
        _view = view;
        _resourceMap = org.jdesktop.application.Application.getInstance(citadels.CitadelsApp.class).getContext().getResourceMap(CitadelsApp.class);
        _mode = mode;

        //Initialize list of labels
        _districtCardLabels = new LinkedList<JLabel>();
        _districtCardLabels.add(cardLabel1);
        _districtCardLabels.add(cardLabel2);
        _districtCardLabels.add(cardLabel3);
        _districtCardLabels.add(cardLabel4);

        //Place districts
        _shift = 0;
        setDistricts();

        //Shift buttons
        if (_districts.size() > _districtCardLabels.size()) {
            rightShiftButton.setEnabled(true);
        }

        //Mode specific setup
        switch (mode) {
            case ACTION_CARDS:
                this.setTitle(_resourceMap.getString("dialog.sdsActionCards.title"));
                selectSingleDistrictLabel.setText(_resourceMap.getString("dialog.sdsActionCards.label"));
                break;
            case LABORATORY:
                this.setTitle(_resourceMap.getString("dialog.sdsLaboratory.title"));
                selectSingleDistrictLabel.setText(_resourceMap.getString("dialog.sdsLaboratory.label"));
                cancelButton.setEnabled(true);
                break;
            case WIZARD:
                this.setTitle(_resourceMap.getString("dialog.sdsWizard.title"));
                selectSingleDistrictLabel.setText(_resourceMap.getString("dialog.sdsWizard.label"));
                break;
            case LIGHTHOUSE:
                this.setTitle(_resourceMap.getString("dialog.sdsLighthouse.title"));
                selectSingleDistrictLabel.setText(_resourceMap.getString("dialog.sdsLighthouse.label"));
                break;
            case MUSEUM:
                this.setTitle(_resourceMap.getString("dialog.sdsMuseum.title"));
                selectSingleDistrictLabel.setText(_resourceMap.getString("dialog.sdsMuseum.label"));
                break;
        }
    }

    private void setDistricts() {

        //When number of labels is enough to show all
        if (_districts.size() <= _districtCardLabels.size()) {
            for (int i = 0; i < _districts.size(); i++) {
                _districtCardLabels.get(i).setIcon(IconContainer.getIcon(_districts.get(i).getName()));
                _districtCardLabels.get(i).setText(Integer.toString(i));
                _districtCardLabels.get(i).setEnabled(true);
            }
            for (int i = _districts.size(); i < _districtCardLabels.size(); i++) {
                _districtCardLabels.get(i).setIcon(null);
                _districtCardLabels.get(i).setText("");
                _districtCardLabels.get(i).setEnabled(false);
            }
        } else {
            for (int i = 0; i < _districtCardLabels.size(); i++) {
                try {
                    _districtCardLabels.get(i).setIcon(IconContainer.getIcon(_districts.get(i+_shift).getName()));
                    _districtCardLabels.get(i).setText(Integer.toString(i+_shift));
                   _districtCardLabels.get(i).setEnabled(true);
                } catch (NullPointerException npe) {
                    npe.printStackTrace();
                }
            }
        }
    }

    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        selectSingleDistrictLabel = new javax.swing.JLabel();
        cardLabel1 = new javax.swing.JLabel();
        cardLabel2 = new javax.swing.JLabel();
        cardLabel3 = new javax.swing.JLabel();
        cardLabel4 = new javax.swing.JLabel();
        leftShiftButton = new javax.swing.JButton();
        rightShiftButton = new javax.swing.JButton();
        jScrollPane1 = new javax.swing.JScrollPane();
        districtDescriptionPane = new javax.swing.JTextPane();
        okButton = new javax.swing.JButton();
        cancelButton = new javax.swing.JButton();
        districtNameLabel = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.DO_NOTHING_ON_CLOSE);
        setName("Form"); // NOI18N
        setResizable(false);

        org.jdesktop.application.ResourceMap resourceMap = org.jdesktop.application.Application.getInstance(citadels.CitadelsApp.class).getContext().getResourceMap(SingleDistrictSelectionBox.class);
        selectSingleDistrictLabel.setFont(resourceMap.getFont("dialog.title.font")); // NOI18N
        selectSingleDistrictLabel.setText(resourceMap.getString("selectSingleDistrictLabel.text")); // NOI18N
        selectSingleDistrictLabel.setName("selectSingleDistrictLabel"); // NOI18N

        cardLabel1.setText(resourceMap.getString("cardLabel1.text")); // NOI18N
        cardLabel1.setMaximumSize(new java.awt.Dimension(60, 80));
        cardLabel1.setMinimumSize(new java.awt.Dimension(60, 80));
        cardLabel1.setName("cardLabel1"); // NOI18N
        cardLabel1.setPreferredSize(new java.awt.Dimension(60, 80));
        cardLabel1.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                cardLabelMousePressed(evt);
            }
        });

        cardLabel2.setMaximumSize(new java.awt.Dimension(60, 80));
        cardLabel2.setMinimumSize(new java.awt.Dimension(60, 80));
        cardLabel2.setName("cardLabel2"); // NOI18N
        cardLabel2.setPreferredSize(new java.awt.Dimension(60, 80));
        cardLabel2.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                cardLabelMousePressed(evt);
            }
        });

        cardLabel3.setMaximumSize(new java.awt.Dimension(60, 80));
        cardLabel3.setMinimumSize(new java.awt.Dimension(60, 80));
        cardLabel3.setName("cardLabel3"); // NOI18N
        cardLabel3.setPreferredSize(new java.awt.Dimension(60, 80));
        cardLabel3.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                cardLabelMousePressed(evt);
            }
        });

        cardLabel4.setMaximumSize(new java.awt.Dimension(60, 80));
        cardLabel4.setMinimumSize(new java.awt.Dimension(60, 80));
        cardLabel4.setName("cardLabel4"); // NOI18N
        cardLabel4.setPreferredSize(new java.awt.Dimension(60, 80));
        cardLabel4.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                cardLabelMousePressed(evt);
            }
        });

        leftShiftButton.setText(resourceMap.getString("leftShiftButton.text")); // NOI18N
        leftShiftButton.setEnabled(false);
        leftShiftButton.setName("leftShiftButton"); // NOI18N
        leftShiftButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                leftShiftButtonActionPerformed(evt);
            }
        });

        rightShiftButton.setText(resourceMap.getString("rightShiftButton.text")); // NOI18N
        rightShiftButton.setEnabled(false);
        rightShiftButton.setName("rightShiftButton"); // NOI18N
        rightShiftButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                rightShiftButtonActionPerformed(evt);
            }
        });

        jScrollPane1.setName("jScrollPane1"); // NOI18N

        districtDescriptionPane.setBackground(new java.awt.Color(240, 240, 240));
        districtDescriptionPane.setContentType("text/html"); // NOI18N
        districtDescriptionPane.setEditable(false);
        districtDescriptionPane.setFont(resourceMap.getFont("descriptionPane.font")); // NOI18N
        districtDescriptionPane.setFocusable(false);
        districtDescriptionPane.setName("districtDescriptionPane"); // NOI18N
        jScrollPane1.setViewportView(districtDescriptionPane);

        okButton.setText(resourceMap.getString("okButton.text")); // NOI18N
        okButton.setEnabled(false);
        okButton.setName("okButton"); // NOI18N
        okButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                okButtonActionPerformed(evt);
            }
        });

        cancelButton.setText(resourceMap.getString("cancelButton.text")); // NOI18N
        cancelButton.setEnabled(false);
        cancelButton.setName("cancelButton"); // NOI18N
        cancelButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cancelButtonActionPerformed(evt);
            }
        });

        districtNameLabel.setFont(resourceMap.getFont("districtNameLabel.font")); // NOI18N
        districtNameLabel.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        districtNameLabel.setText(resourceMap.getString("districtNameLabel.text")); // NOI18N
        districtNameLabel.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        districtNameLabel.setName("districtNameLabel"); // NOI18N

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 261, Short.MAX_VALUE)
                        .addContainerGap())
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(selectSingleDistrictLabel, javax.swing.GroupLayout.DEFAULT_SIZE, 261, Short.MAX_VALUE)
                            .addGroup(layout.createSequentialGroup()
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(cardLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(leftShiftButton))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                    .addGroup(layout.createSequentialGroup()
                                        .addComponent(cardLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(cardLabel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                    .addComponent(districtNameLabel, javax.swing.GroupLayout.DEFAULT_SIZE, 127, Short.MAX_VALUE))
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(layout.createSequentialGroup()
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(cardLabel4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                                        .addGap(28, 28, 28)
                                        .addComponent(rightShiftButton)))))
                        .addContainerGap())
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addComponent(okButton)
                        .addGap(44, 44, 44)
                        .addComponent(cancelButton)
                        .addGap(62, 62, 62))))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(selectSingleDistrictLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(cardLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cardLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cardLabel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cardLabel4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(leftShiftButton)
                        .addComponent(districtNameLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 21, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(rightShiftButton))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 105, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(cancelButton)
                    .addComponent(okButton))
                .addContainerGap(11, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void cardLabelMousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_cardLabelMousePressed
        if (evt.getComponent().isEnabled()) {
            try {
                _selected = Integer.parseInt(((JLabel)evt.getComponent()).getText());
                //get district ability
                String districtAbility;
                if (_districts.get(_selected).getEffect() == EFFECT.NONE) {
                    districtAbility = _resourceMap.getString("dialog.sds.description.none");
                } else {
                    districtAbility = _resourceMap.getString("district."+_districts.get(_selected).getName()+".effect");
                }

                //set name and description
                districtNameLabel.setText(_resourceMap.getString("district."+_districts.get(_selected).getName()+".name"));
                districtDescriptionPane.setText(_resourceMap.getString("term.cost")+" : "+_districts.get(_selected).getCost() + " " +
                                                _resourceMap.getString("term.score")+" : "+_districts.get(_selected).getScore() + "<br>" +
                                                _resourceMap.getString("dialog.sds.description.districtAbility")+" : " + districtAbility) ;
                okButton.setEnabled(true);
            } catch (Exception e) {}
        }
    }//GEN-LAST:event_cardLabelMousePressed

    private void leftShiftButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_leftShiftButtonActionPerformed
        //Shift the displayed cards to the left
        if (_shift > 0) _shift--;
        //Disable the button if the beginning of the hand reached
        if (_shift == 0) leftShiftButton.setEnabled(false);
        //Enable the right shift when there is still room to move right
        if (_shift < _districts.size()-_districtCardLabels.size()) rightShiftButton.setEnabled(true);
        setDistricts();
    }//GEN-LAST:event_leftShiftButtonActionPerformed

    private void rightShiftButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_rightShiftButtonActionPerformed
                if (_shift < _districts.size()-_districtCardLabels.size()) _shift++;
        //Disable the button if the end of the hand reached
        if (_shift == _districts.size()-_districtCardLabels.size()) rightShiftButton.setEnabled(false);
        //Enable the left shift when moved to the right at least by one
        if (_shift > 0) leftShiftButton.setEnabled(true);
        setDistricts();
    }//GEN-LAST:event_rightShiftButtonActionPerformed

    private void okButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_okButtonActionPerformed
        this.setVisible(false);
        switch (_mode) {
            case ACTION_CARDS:
                //_view.characterSelected(_selected);
                _view.actionCardsSelected(new int[] {_selected});
                break;
            case LABORATORY:
                _view.laboratorySelected(_selected);
                break;
            case WIZARD:
                _view.wizardTargetCardSelected(_optionalNum, _selected);
                break;
            case LIGHTHOUSE:
                _view.lighthouseSelected(_optionalNum, _selected);
                break;
            case MUSEUM:
                _view.museumSelected(_optionalNum, _selected);
                break;
        }
        this.dispose();
    }//GEN-LAST:event_okButtonActionPerformed

    private void cancelButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cancelButtonActionPerformed
        this.dispose();
    }//GEN-LAST:event_cancelButtonActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton cancelButton;
    private javax.swing.JLabel cardLabel1;
    private javax.swing.JLabel cardLabel2;
    private javax.swing.JLabel cardLabel3;
    private javax.swing.JLabel cardLabel4;
    private javax.swing.JTextPane districtDescriptionPane;
    private javax.swing.JLabel districtNameLabel;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JButton leftShiftButton;
    private javax.swing.JButton okButton;
    private javax.swing.JButton rightShiftButton;
    private javax.swing.JLabel selectSingleDistrictLabel;
    // End of variables declaration//GEN-END:variables

}
