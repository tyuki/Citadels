/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package citadels.ai;

import citadels.control.CitadelsViewer;
import java.util.LinkedList;
import java.util.List;

/**
 *
 * @author Personal
 */
public class AIContainer {

    private static List<AIPlayer> _ais = new LinkedList<AIPlayer>();
    private static List<String> NAMES = new LinkedList<String>() {
        {
            add("AI_1");
            add("AI_2");
            add("AI_3");
            add("AI_4");
            add("AI_5");
            add("AI_6");
            add("AI_7");
            add("AI_8");
        }
    };

    public static void addAI(int port) {
        AIPlayer ai = new AIPlayer(NAMES.get(_ais.size()), port);
        _ais.add(ai);
        ai.start();
    }

    public static void removeAI() {
        if (_ais.size() > 0) {
            _ais.get(_ais.size()-1).disconnect();
            _ais.remove(_ais.size()-1);
        }
    }

    public static int getCount() {
        return _ais.size();
    }

    public static void reset() {
        int n = _ais.size();
        for (int i = 0; i < n; i++) {
            removeAI();
        }
    }

}
