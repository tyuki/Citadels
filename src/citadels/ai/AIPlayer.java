/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package citadels.ai;

import citadels.control.CitadelsController;
import citadels.control.CitadelsViewer;
import citadels.control.Organizer;
import citadels.game.Character;
import citadels.game.Character.CHARACTER;
import citadels.game.Characters;
import citadels.game.District;
import citadels.game.GameState;
import citadels.game.GameState.BUILD_QUERY_RESPONSE;
import citadels.game.Player;
import citadels.game.Progress;
import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

/**
 *
 * @author Personal
 */
public class AIPlayer implements CitadelsController, Runnable {

    protected Organizer _organizer;
//    protected CitadelsViewer _playerView;
    protected final String _name;
    protected int _playerID;
    private int _port;
    private Thread _thread;

    protected List<Character> _othersCandidate;

    public AIPlayer(String name, int port) {
        _othersCandidate = new LinkedList<Character>();
//        _playerView = view;
        _name = name;
        _port = port;

    }

    public void start() {
        _thread = new Thread(this);
        _thread.start();
    }



    public void run() {
        _organizer = new Organizer();
        _organizer.setController(this);
        _organizer.connectServer(_name, "localhost", _port);
        _organizer.joinGame(true);
    }
    
    public void disconnect() {
        _organizer.disconnectFromServer();
    }

    public void setID(int id) {
        _playerID = id;
    }

    public void selectCharacter(Characters characters) {
        //Create others candiate
        _othersCandidate.clear();

        //Make list of characters that may be used by others
        for (int i = 1; i <= characters.getSize(); i++) {
            //If not available, and is not revealed, it may be used by some one preceeding
            if (!characters.isAvailable(i) && !characters.isRevealed(i)) {
                _othersCandidate.add(characters.getCharacter(i));
            }
        }

        double[] rating = rateCharacterChoices(_organizer.getCurrentGameState().getPlayer(_playerID), characters);

        //find the highest for now
        double max = -10;
        int maxID = -1;
        for (int i = 0; i < rating.length; i++) {
            if (rating[i] > max) {
                maxID = i+1;
                max = rating[i];
            }
        }

        _organizer.characterSelected(maxID);
    }

    public void selectNewKing(List<Player> players) {
        for (int i = 0; i < players.size(); i++) {
            if (players.get(i).getID() == _playerID) continue;
            if (players.get(i).getIsKing()) continue;
            _organizer.emperorAbilityKingSelected(players.get(i).getID());
        }
    }

    public void selectTributeToEmperor(Player player) {
        //If there is a duplicate, use that first
        boolean[] dup = this.getDuplicateDistricts();
        for (int i = 0; i < dup.length; i++) {
            if (dup[i]) {
                _organizer.emperorAbilityKingSelected(i);
                return;
            }
        }

        //Else gold
        if (player.getGold() > 0) _organizer.emperorTributeGold();

        //Then card
        _organizer.emperorTributeCard(0);
    }

    public void selectGraveyard(District district) {
        _organizer.graveyardAbility(false);
    }

    public void updateControl(Progress progress, Character character, Player player, boolean action, boolean build, boolean end, boolean[] SAs, boolean distIncome, boolean[] DAs) {
        if (character == null) return;

        List<Player> players = _organizer.getCurrentGameState().getPlayers();
        GameState game = _organizer.getCurrentGameState();

        //choose action
        if (action) {
            //case when there is not cards in hand
            if (player.getHand().size() == 0) {
                //If the character is magician
                 if (character.character == CHARACTER.MAGICIAN &&  SAs[0]) {
                     int max = 0;
                     int maxID = -1;
                     for (Player target : players) {
                         if (target.getHand().size() > max) {
                             maxID = target.getID();
                             max = target.getHand().size();
                         }
                     }
                     //Swap the card with the player with most card
                     if (max > 0) {
                         _organizer.magicianAbilitySwap(maxID);
                         return;
                     }
                 }

                 //
                 _organizer.actionCards(drawCardFromDeck(game.getActionCardsChoice(_playerID), game.getActionCardsKeepNum(_playerID)));
                 return;
            }
            _organizer.actionCoins();
            return;
        //Others need character specific decision
        } else {

            switch (character.character) {
                case WITCH:
                case WIZARD:
                    break;
                case ABBOT:
                    if (SAs[0]) {
                       _organizer.abbotAbility();
                        return;
                    }
                    break;
                case ALCHEMIST:
                case ARCHITECT:
                case NAVIGATOR:
                case WARLORD:
                case DIPLOMAT:
                case QUEEN:
                case ARTIST:
            }

            //Build first
            if (build) {
                for (int i = 0; i < player.getHand().size(); i++) {
                    if (game.canBulildDistrict(_playerID, i) == BUILD_QUERY_RESPONSE.CAN_BUILD) {
                        _organizer.buildDistrict(i);
                        return;
                    }
                }
            }

            //Use district income
            if (distIncome) {
                switch (character.character) {
                    case KING:
                    case EMPEROR:
                    case BISHOP:
                    case ABBOT:
                    case MERCHANT:
                    case WARLORD:
                    case DIPLOMAT:
                        _organizer.earnDistrictIncome();
                        return;
                }
            }

            //characters ability to be used
            switch (character.character) {
                case ASSASSIN:
                    if (SAs[0]) {
                        _organizer.assassinAbility(selectAssassinationTarget(game.getCharacters()));
                        return;
                    }
                    break;
                case WITCH:
                    break;
                case THIEF:
                    if (SAs[0]) {
                       _organizer.thiefAbility(selectThiefTarget(game.getCharacters()));
                        return;
                    }
                    break;
                case MAGICIAN:
                case WIZARD:
                case ABBOT:
                case NAVIGATOR:
                case WARLORD:
                case DIPLOMAT:
                case ARTIST:
            }
        }
            _organizer.turnEnd();
    }

    protected double[] rateCharacterChoices(Player player, Characters characters) {

       double[] res = new double[characters.getSize()];

        for (int i = 0; i < res.length; i++) {
            Character character = characters.getCharacter(i+1);
            double rate = -1;

            //Skip character that is not available
            if (!characters.isAvailable(character.order)) {
                res[i] = rate;
                continue;
            }

            switch (character.character) {
                case ASSASSIN:
                    rate = 3;
                    break;
                case WITCH:
                    rate = 3;
                    break;
                case THIEF:
                    rate = 3;
                    break;
                case TAX_COLLECTOR:
                    rate = 2;
                    break;
                case MAGICIAN:
                    rate = 3;
                    break;
                case WIZARD:
                    rate = 2;
                    break;
                case KING:
                    rate = 2;
                    break;
                case EMPEROR:
                    rate = 2;
                    break;
                case BISHOP:
                    rate = 3;
                    break;
                case ABBOT:
                    rate = 2;
                    break;
                case MERCHANT:
                    rate = 5;
                    break;
                case ALCHEMIST:
                    rate = 5;
                    break;
                case ARCHITECT:
                    rate = 5;
                    break;
                case NAVIGATOR:
                    rate = 5;
                    break;
                case WARLORD:
                    rate = 2;
                    break;
                case DIPLOMAT:
                    rate = 2;
                    break;
                case QUEEN:
                    rate = 1;
                    break;
                case ARTIST:
                    rate = 1;
                    break;
            }
            res[i] = rate;
        }


        return res;
    }

    protected int[] drawCardFromDeck(List<District> choice, int keepNum) {
        int[] res = new int[keepNum];

        //When you can pick all
        if (choice.size() == keepNum) {
            for (int i = 0; i < res.length; i++) {
                res[i] = i;
            }
        //Else randomly pick
        } else {
            for (int i = 0; i < res.length; i++) {
                res[i] = i;
            }
        }

        return res;
    }

    protected int selectAssassinationTarget(Characters characters) {

        double[] rate = new double[_othersCandidate.size()];
        double maxRate = -1;
        
        for (int i = 0; i < _othersCandidate.size(); i++) {
            rate[i] = -1;

            //rate based on character
            switch (_othersCandidate.get(i).character) {
                case THIEF:
                    rate[i] = 0.1;
                    break;
                case TAX_COLLECTOR:
                    rate[i] = 0.1;
                    break;
                case MAGICIAN:
                    rate[i] = 3;
                    break;
                case WIZARD:
                    rate[i] = 2;
                    break;
                case KING:
                    rate[i] = 2;
                    break;
                case EMPEROR:
                    rate[i] = 3;
                    break;
                case BISHOP:
                    rate[i] = 3;
                    break;
                case ABBOT:
                    rate[i] = 2;
                    break;
                case MERCHANT:
                    rate[i] = 5;
                    break;
                case ALCHEMIST:
                    rate[i] = 5;
                    break;
                case ARCHITECT:
                    rate[i] = 5;
                    break;
                case NAVIGATOR:
                    rate[i] = 5;
                    break;
                case WARLORD:
                    rate[i] = 2;
                    break;
                case DIPLOMAT:
                    rate[i] = 2;
                    break;
                case QUEEN:
                    rate[i] = 1;
                    break;
                case ARTIST:
                    rate[i] = 1;
                    break;

            }

            //take max
            if (rate[i] > maxRate) {
                maxRate = rate[i];
            }
        }

        List<Integer> ties = new ArrayList<Integer>();
        for (int i = 0; i < rate.length; i++) {
            if (rate[i] == maxRate) ties.add(i);
        }

        Collections.shuffle(ties);

        return _othersCandidate.get(ties.get(0)).order;
    }

    protected int selectThiefTarget(Characters characters) {

        double[] rate = new double[_othersCandidate.size()];
        double maxRate = -1;
        for (int i = 0; i < _othersCandidate.size(); i++) {
            rate[i] = -1;

            if (characters.getAssassinated() == _othersCandidate.get(i).order ||
                    characters.getBeWitched() == _othersCandidate.get(i).order) {
                continue;
            }

            //rate based on character
            switch (_othersCandidate.get(i).character) {
                case THIEF:
                    rate[i] = 0.1;
                    break;
                case TAX_COLLECTOR:
                    rate[i] = 0.1;
                    break;
                case MAGICIAN:
                    rate[i] = 3;
                    break;
                case WIZARD:
                    rate[i] = 2;
                    break;
                case KING:
                    rate[i] = 2;
                    break;
                case EMPEROR:
                    rate[i] = 3;
                    break;
                case BISHOP:
                    rate[i] = 3;
                    break;
                case ABBOT:
                    rate[i] = 2;
                    break;
                case MERCHANT:
                    rate[i] = 5;
                    break;
                case ALCHEMIST:
                    rate[i] = 5;
                    break;
                case ARCHITECT:
                    rate[i] = 5;
                    break;
                case NAVIGATOR:
                    rate[i] = 5;
                    break;
                case WARLORD:
                    rate[i] = 2;
                    break;
                case DIPLOMAT:
                    rate[i] = 2;
                    break;
                case QUEEN:
                    rate[i] = 1;
                    break;
                case ARTIST:
                    rate[i] = 1;
                    break;

            }

            //take max
            if (rate[i] > maxRate) {
                maxRate = rate[i];
            }
        }

        List<Integer> ties = new ArrayList<Integer>();
        for (int i = 0; i < rate.length; i++) {
            if (rate[i] == maxRate) ties.add(i);
        }

        Collections.shuffle(ties);

        return _othersCandidate.get(ties.get(0)).order;
    }

    protected boolean[] getDuplicateDistricts() {
        Player player = _organizer.getCurrentGameState().getPlayer(_playerID);
        boolean[] res = new boolean[player.getHand().size()];

        //Look at each hands
        for (int i = 0; i < player.getHand().size(); i++) {
            District hand = player.getHand().get(i);
            //Look at each district
            for (District city : player.getCity()) {
                //If it is the same district
                if (hand.getName().compareTo(city.getName()) == 0) {
                    res[i] = true;
                    break;
                }
            }
            //Look at other cards in the hand
            for (int j = i+1; j < player.getHand().size(); j++) {
                //Look at other hands
                if (hand.getName().compareTo(player.getHand().get(j).getName()) == 0) {
                    res[i] = res[j] = true;
                    break;
                }
            }
        }

        return res;
    }
}
