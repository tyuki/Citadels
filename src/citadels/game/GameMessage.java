/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package citadels.game;

import citadels.CitadelsApp;
import citadels.game.GameMessage.ARGC1_MESSAGE;
import java.util.Locale;

/**
 *
 * @author Personal
 */
public class GameMessage {

    public static enum COLOR_TYPE {
        DEFAULT,
        PLAYER,
        ERROR,
        EMPHASIZE
    }
    
    public static enum ARGC1_MESSAGE {
        ROUND_START,
        CHARACTER_SELECTED,
        ACTION_GOLD,
        ACTION_CARDS,
        END_TURN,
        CITY_COMPLETED,
        GAME_END,

        SA_KING,
        SA_MERCHANT,
        SA_ARCHITECT,
        SA_QUEEN,

        SA_BEWITCH_BEWITCHED,
        SA_BEWITCH_RESUME_TURN,
        SA_ABBOT_FAILED,
        SA_NAVIGATOR_GOLD,
        SA_NAVIGATOR_CARDS,

        DA_SMITHY,
        DA_LABORATORY,
        DA_POORHOUSE,
        DA_PARK,
        DA_THRONEROOM,
        DA_LIGHTHOUSE,
        DA_HOSPITAL,
        DA_MUSEUM
    }
    
    public static enum ARGC2_MESSAGE {
        DISTRICT_INCOME,
        BUILD_DISTRICT,
        SCORE_REPORT,

        SA_ASSASSIN,
        SA_THIEF_ANNOUNCE,
        SA_MAGICIAN_SWAP,
        SA_MAGICIAN_EXCHANGE,
        SA_WITCH,
        SA_TAX_COLLECTOR,
        SA_WIZARD_TAKE,
        SA_EMPEROR,
        SA_EMPEROR_TRIBUTE_GOLD,
        SA_EMPEROR_TRIBUTE_CARD,
        SA_ABBOT,
        SA_ALCHEMIST,
        SA_ARTIST,

        DA_GRAVEYARD,
        DA_BELLTOWER
    }

    public static enum ARGC3_MESSAGE {
        SA_THIEF_STEAL,
        SA_WARLORD,
        SA_WIZARD_TAKE_AND_BUILD,
        DA_ARMORY
    }

    public static enum ARGC4_MESSAGE {
        SA_DIPLOMAT
    }

    private static org.jdesktop.application.ResourceMap _resourceMap;
    
    static {
        _resourceMap = org.jdesktop.application.Application.getInstance(citadels.CitadelsApp.class).getContext().getResourceMap(CitadelsApp.class);
    }

    public final String string;
    public final COLOR_TYPE colorType;
    public final int colorID;

    public GameMessage(String string, COLOR_TYPE colorType, int colorID) {
        this.string = string;
        this.colorType = colorType;
        this.colorID = colorID;
    }
    
    public static GameMessage singleArgMessage(ARGC1_MESSAGE msgType, String arg1, COLOR_TYPE type, int id) {

        Locale local = Locale.getDefault();

        if (local == Locale.JAPAN || local == Locale.JAPANESE) {
            return new GameMessage(singleArgMessageJP(msgType, arg1), type, id);
        } else {
            return new GameMessage(singleArgMessageEN(msgType, arg1), type, id);
        }
    }
    
    private static String singleArgMessageEN(ARGC1_MESSAGE msgType, String arg1) {
            switch (msgType) {
                case ROUND_START:
                    return "Starting ROUND " + arg1;
                case CHARACTER_SELECTED:
                    return arg1 + " has selected a character";
                case ACTION_GOLD:
                    return arg1 + " has received 2 gold";
                case ACTION_CARDS:
                    return arg1 + " has drawn cards";
                case END_TURN:
                    return arg1 + " has ended his turn";
                case CITY_COMPLETED:
                    return arg1 + " has completed a city (this will be the last round of the game)";
                case GAME_END:
                    return "GAME END";
                case SA_KING:
                    return "The Crown has moved to " + arg1;
                case SA_MERCHANT:
                    return arg1 + " has received 1 gold with Merchant's ability";
                case SA_ARCHITECT:
                    return arg1 + " got 2 cards with Architect's ability";
                case SA_QUEEN:
                    return arg1 + " received 3 gold with Queen's ability";
                case SA_BEWITCH_BEWITCHED:
                    return "Turn of "+arg1+" ends";
                case SA_BEWITCH_RESUME_TURN:
                    return "Resuming " + arg1 + "'s turn with Witch's ability";
                case SA_ABBOT_FAILED:
                    return "Abbot's ability did not give " + arg1 + " any gold";
                case SA_NAVIGATOR_GOLD:
                    return arg1 + " has received 4 gold with Navigator's ability";
                case SA_NAVIGATOR_CARDS:
                    return arg1 + " has drawn 4 cards with Navigator's ability";
                case DA_SMITHY:
                    return arg1 + " has paid 2 gold to draw 3 cards using Smithy";
                case DA_LABORATORY:
                    return arg1 + " has discarded 1 card to earn 1 gold using Laboratory";
                case DA_POORHOUSE:
                    return arg1 + " has received 1 gold from Poor House";
                case DA_PARK:
                    return arg1 + " has received 2 cards from Park";
                case DA_THRONEROOM:
                    return arg1 + " has received 1 gold from Throne Room";
                case DA_LIGHTHOUSE:
                    return arg1 + " has taken 1 card from the deck using Lighthouse";
                case DA_HOSPITAL:
                    return arg1 + " has been attacked by the Assassin, but survived using Hospital";
                case DA_MUSEUM:
                    return arg1 + " has put a card under the Museum";

            }
        return "";
    }

    private static String singleArgMessageJP(ARGC1_MESSAGE msgType, String arg1) {
        switch (msgType) {
            case ROUND_START:
                return "第" + arg1 + "ラウンド開始";
            case CHARACTER_SELECTED:
                return arg1 + "がキャラクターを選択しました";
            case ACTION_GOLD:
                return arg1 + "が金2を得ました";
            case ACTION_CARDS:
                return arg1 + "がカードを引きました";
            case END_TURN:
                return arg1 + "がターンを終了しました";
            case CITY_COMPLETED:
                return arg1 + "が街を完成させました (これが最終ラウンドとなります)";
            case GAME_END:
                return "ゲーム終了";
            case SA_KING:
                return "王冠が" + arg1 + "に移動しました";
            case SA_MERCHANT:
                return arg1 + "は商人の能力により金1を得ました";
            case SA_ARCHITECT:
                return arg1 + "は建築家の能力によりカードを2枚引きました";
            case SA_QUEEN:
                return arg1 + "は女王の能力により金3を得ました";
            case SA_BEWITCH_BEWITCHED:
                return arg1+"のターンは終了しました";
            case SA_BEWITCH_RESUME_TURN:
                return "魔女の魅了により" + arg1 + "のターンが再開しました";
            case SA_ABBOT_FAILED:
                return arg1+"は僧院長の能力で金を得ることが出来ませんでした";
            case SA_NAVIGATOR_GOLD:
                return arg1 + "は航海士の能力で金4を得ました";
            case SA_NAVIGATOR_CARDS:
                return arg1 + "は航海士の能力でカードを4枚引きました";
            case DA_SMITHY:
                return arg1 + "は鍛冶屋の能力で金2と引き換えにカードを3枚引きました";
            case DA_LABORATORY:
                return arg1 + "は研究所の能力でカード1枚と引き換えに金1を得ました";
            case DA_POORHOUSE:
                return arg1 + "は救賓館の効果により金1を得ました";
            case DA_PARK:
                return arg1 + "は公園の効果によりカードを2枚引きました";
            case DA_THRONEROOM:
                return arg1 + "は玉座の効果により金1を得ました";
            case DA_LIGHTHOUSE:
                return arg1 + "は灯台の効果で山札からカードを1枚取りました";
            case DA_HOSPITAL:
                return arg1 + "は暗殺者に襲われましたが、病院の効果で一命をとりとめました";
            case DA_MUSEUM:
                return arg1 + "は博物館にカードを収めました";


        }
        return singleArgMessageEN(msgType, arg1);
    }



    public static GameMessage coupleArgMessage(ARGC2_MESSAGE msgType, String arg1, String arg2, COLOR_TYPE type, int id) {

        Locale local = Locale.getDefault();

        if (local == Locale.JAPAN || local == Locale.JAPANESE) {
            return new GameMessage(coupleArgMessageJP(msgType, arg1, arg2), type, id);
        } else {
            return new GameMessage(coupleArgMessageEN(msgType, arg1, arg2), type, id);
        }
    }


    private static String coupleArgMessageEN(ARGC2_MESSAGE msgType, String arg1, String arg2) {
            switch (msgType) {
                case DISTRICT_INCOME:
                    return arg1 + " has received " + arg2 + " gold from districts";
                case BUILD_DISTRICT:
                    return arg1 + " has built " + _resourceMap.getString("district."+arg2+".name");
                case SCORE_REPORT:
                    return arg2 + " : " + arg1;
                case SA_ASSASSIN:
                    return arg1 + " has declared to assassinate " + _resourceMap.getString("character."+arg2+".name");
                case SA_THIEF_ANNOUNCE:
                    return arg1 + " has declared to steal from " + _resourceMap.getString("character."+arg2+".name");
                case SA_MAGICIAN_SWAP:
                    return arg1 + " has swapped hands with " + arg2;
                case SA_MAGICIAN_EXCHANGE:
                    return arg1 + " has exchanged " + arg2 + " cards with the deck";
                case SA_WITCH:
                    return arg1 + " has declared to bewitch " + _resourceMap.getString("character."+arg2+".name");
                case SA_TAX_COLLECTOR:
                    return arg1 + " has collected 1 gold from " + arg2 + " with Tax Collector's ability";
                case SA_WIZARD_TAKE:
                    return arg1 + " has taken a district card from " + arg2 + " with Wizard's ability";
                case SA_EMPEROR:
                    return arg1 + " has moved the Crown to " + arg2;
                case SA_EMPEROR_TRIBUTE_GOLD:
                    return arg1 + " has given " + arg2 + " 1 gold for receiving the Crown";
                case SA_EMPEROR_TRIBUTE_CARD:
                    return arg1 + " has given " + arg2 + " 1 card for receiving the Crown";
                case SA_ABBOT:
                    return arg1 + " has received 1 gold from " + arg2 + " with Abbot's ability";
                case SA_ALCHEMIST:
                    return arg1 + " has received " + arg2 + " gold back with Alchemist's ability";
                case SA_ARTIST:
                    return arg1 + " has beautified " + arg2;
                case DA_GRAVEYARD:
                    return arg1 + " has paid 1 gold to take " + _resourceMap.getString("district."+arg2+".name") + " with Graveyard's ability";
                case DA_BELLTOWER:
                    return arg1 + " has declared to use Bell Tower's ability. Cities will now complete with " + arg2 + " districts";
            }
        return "";
    }

    private static String coupleArgMessageJP(ARGC2_MESSAGE msgType, String arg1, String arg2) {
        switch (msgType) {
            case DISTRICT_INCOME:
                return arg1 + "は自分の街から金" + arg2 + "を得ました";
            case BUILD_DISTRICT:
                return arg1 + "は" + _resourceMap.getString("district."+arg2+".name") + "を建築しました";
            case SCORE_REPORT:
                return arg2 + " : " + arg1;
            case SA_ASSASSIN:
                return arg1 + "は " + _resourceMap.getString("character."+arg2+".name") + "の暗殺を宣言しました";
            case SA_THIEF_ANNOUNCE:
                return arg1 + "は" + _resourceMap.getString("character."+arg2+".name") + "から盗むことを宣言しました";
            case SA_MAGICIAN_SWAP:
                return arg1 + "は魔法使いの能力で" + arg2 + "と手札を入れ替えました";
            case SA_MAGICIAN_EXCHANGE:
                return arg1 + "は魔法使いの能力でカードを" + arg2 + "枚山札と交換しました";
            case SA_WITCH:
                return arg1 + "は" + _resourceMap.getString("character."+arg2+".name") + "を魅了すると宣言しました";
            case SA_TAX_COLLECTOR:
                return arg1 + "は" + arg2 + "から収税吏の能力により金1を得ました";
            case SA_WIZARD_TAKE:
                return arg1 + "は" + arg2 + "から魔術師の能力でカードを1枚奪いました";
            case SA_EMPEROR:
                return arg1 + "は王冠を" + arg2 + "に渡しました";
            case SA_EMPEROR_TRIBUTE_GOLD:
                return arg1 + "は" + arg2 + "に金1を献上しました";
            case SA_EMPEROR_TRIBUTE_CARD:
                return arg1 + "は" + arg2 + "にカードを1枚献上しました";
            case SA_ABBOT:
                return arg1 + "は" + arg2 + "より僧院長の能力により金1を受け取りました";
            case SA_ALCHEMIST:
                return arg1 + "は錬金術師の能力により金" + arg2 + "を取り戻しました";
            case SA_ARTIST:
                return arg1 + "は" + arg2 + "を美化しました";
            case DA_GRAVEYARD:
                return arg1 + "は金1を支払い、墓場の能力で" + _resourceMap.getString("district."+arg2+".name") + "を手札に加えました";
            case DA_BELLTOWER:
                return arg1 + "は鐘楼の効果を使いました。 このゲーム中、鐘楼が破壊されない限り街の完成条件は" + arg2 + "件となります";
        }
        return coupleArgMessageEN(msgType, arg1, arg2);
    }


    public static GameMessage tripleArgMessage(ARGC3_MESSAGE msgType, String arg1, String arg2, String arg3, COLOR_TYPE type, int id) {

        Locale local = Locale.getDefault();

        if (local == Locale.JAPAN || local == Locale.JAPANESE) {
            return new GameMessage(tripleArgMessageJP(msgType, arg1, arg2, arg3), type, id);
        } else {
            return new GameMessage(tripleArgMessageEN(msgType, arg1, arg2, arg3), type, id);
        }
    }

    private static String tripleArgMessageEN(ARGC3_MESSAGE msgType, String arg1, String arg2, String arg3) {
        switch (msgType) {
                case SA_THIEF_STEAL:
                    return arg1 + " has stolen " + arg2 + " gold from " + arg3;
                case SA_WARLORD:
                    return arg1 + " has destroyed " + _resourceMap.getString("district."+arg3+".name") + " of " + arg2;
                case SA_WIZARD_TAKE_AND_BUILD:
                    return arg1 + " has taken " + _resourceMap.getString("district."+arg3+".name") + " from " + arg2 + " and built it with Wizard's ability";
                case DA_ARMORY:
                    return arg1 + " has used Armory to destory " + _resourceMap.getString("district."+arg3+".name") + " of " + arg2;

        }
        return "";
    }

    private static String tripleArgMessageJP(ARGC3_MESSAGE msgType, String arg1, String arg2, String arg3) {
        switch (msgType) {
                case SA_THIEF_STEAL:
                    return arg1 + "は" + arg3 + "から金"+ arg2 + "盗みました";
                case SA_WARLORD:
                    return arg1 + "は" + arg2 + "の" + _resourceMap.getString("district."+arg3+".name") + "を破壊しました";
                case SA_WIZARD_TAKE_AND_BUILD:
                    return arg1 + "は魔術師の能力で" + arg2 + "から" + _resourceMap.getString("district."+arg3+".name") + "を奪って建築しました";
                case DA_ARMORY:
                    return arg1 + "は武器庫を使って" + arg2 + "の" + _resourceMap.getString("district."+arg3+".name") + "を破壊しました";
        }
        return tripleArgMessageEN(msgType, arg1, arg2, arg3);
    }




    public static GameMessage quadArgMessage(ARGC4_MESSAGE msgType, String arg1, String arg2, String arg3, String arg4, COLOR_TYPE type, int id) {

        Locale local = Locale.getDefault();

        if (local == Locale.JAPAN || local == Locale.JAPANESE) {
            return new GameMessage(quadArgMessageJP(msgType, arg1, arg2, arg3, arg4), type, id);
        } else {
            return new GameMessage(quadArgMessageEN(msgType, arg1, arg2, arg3, arg4), type, id);
        }
    }

    private static String quadArgMessageEN(ARGC4_MESSAGE msgType, String arg1, String arg2, String arg3, String arg4) {
        switch (msgType) {
            case SA_DIPLOMAT:
                return arg1 + " has exchanged " + _resourceMap.getString("district."+arg2+".name") + " with " + arg3 + "'s " + _resourceMap.getString("district."+arg4+".name") + " with Diplomat's ability";
        }
        return "";
    }

    private static String quadArgMessageJP(ARGC4_MESSAGE msgType, String arg1, String arg2, String arg3, String arg4) {
        switch (msgType) {
            case SA_DIPLOMAT:
                return arg1 + "は外交官の能力で" + _resourceMap.getString("district."+arg2+".name") + "を" + arg3 + "の" + _resourceMap.getString("district."+arg4+".name") + "と交換しました";
        }
        return quadArgMessageEN(msgType, arg1, arg2, arg3, arg4);
    }
}
