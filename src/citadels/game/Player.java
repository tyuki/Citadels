/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package citadels.game;

import citadels.game.District.EFFECT;
import java.awt.Color;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Personal
 */
public class Player implements Serializable, Comparable<Player> {

    private int _playerID;
    private String _name;
    
    private List<District> _city;
    private List<District> _hand;
    private int _gold;
    private Character _character;
    private boolean _isKing;
    private Color _color;
    private boolean _completedFirst;

    private int _finalScore;
    public final boolean AI;

    public Player(String name, boolean AI, int id, Color color) {
        _name = name;
        _playerID = id;
        _color = color;
        _city = new ArrayList<District>();
        _hand = new ArrayList<District>();
        _completedFirst = false;
        _finalScore = 0;
        this.AI = AI;
    }

    public String getName() {
        return _name;
    }

    public void setGold(int gold) {
        _gold = gold;
    }

    public int getGold() {
        return _gold;
    }

    public void addGold(int gold) {
        _gold += gold;
    }

    public void setID(int id) {
        _playerID = id;
    }

    public int getID() {
        return _playerID;
    }

    public void setIsKing(boolean isKing) {
        _isKing = isKing;
    }

    public boolean getIsKing() {
        return _isKing;
    }

    public List<District> getCity() {
        return _city;
    }

    public List<District> getHand() {
        return _hand;
    }

    public void setHand(List<District> hand) {
        _hand = hand;
    }

    public void addDistrict(District district) {
        _city.add(district);
    }

    public void removeDistrict(District district) {
        _city.remove(district);
    }

    public void addHand(District district) {
        _hand.add(district);
    }

    public void removeHand(District district) {
        _hand.remove(district);
    }

    public Character getCharacter() {
        return _character;
    }

    public void setCharacter(Character character) {
        _character = character;
    }

    public void setColor(Color color) {
        _color = color;
    }

    public Color getColor() {
        return _color;
    }

    public void setCompletedFirst(boolean cf) {
        _completedFirst = cf;
    }

    public boolean getCompletedFirst() {
        return _completedFirst;
    }

    public void setFinalScore(int score) {
        _finalScore = score;
    }

    public int getFinalScore() {
        return _finalScore;
    }

    public boolean hasDistrictEffect(EFFECT effect) {
        for (District district : _city) {
            if (district.getEffect() == effect) return true;
        }

        return false;
    }

    public int compareTo(Player o) {
        if (this._finalScore > o._finalScore) {
            return -1;
        } else if (this._finalScore < o._finalScore) {
            return 1;
        } else {
            return 0;
        }
    }
}
