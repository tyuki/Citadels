/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package citadels.game;

import citadels.game.Exceptions.GameException;
import java.io.Serializable;

/**
 *
 * @author Personal
 */
public class Characters implements Serializable {
    private Character[] _characters;
    private boolean _has9th;
    private boolean[] _available;
    private boolean[] _revealed;
    private boolean[] _played;
    private int _assassinated;
    private int _thieved;
    private int _bewitched;
    private int _facedown;

    public Characters(boolean use9th, int[] expansionCards) throws GameException {
        _has9th = use9th;

        //Initialize characters and availability array
        if (use9th) {
            _characters = new Character[9];
            _available = new boolean[9];
            _revealed = new boolean[9];
            _played = new boolean[9];
        } else {
            _characters = new Character[8];
            _available = new boolean[8];
            _revealed = new boolean[8];
            _played = new boolean[8];
        }

        //Initialize each character
        for (int i = 0; i < _characters.length; i++) {
            boolean expansion = false;
            if (expansionCards[0] == i+1 || expansionCards[1] == i+1) expansion = true;
            _characters[i] = Character.initialize(i+1, expansion);
        }
        
        //initialize variables
        initializeForNewRound();
    }

    public void initializeForNewRound() {
        for (int i = 0; i < _characters.length; i++) {
            _available[i] = true;
            _revealed[i] = false;
            _played[i] = false;
        }

        _assassinated = -1;
        _thieved = -1;
        _bewitched = -1;
        _facedown = -1;
    }


    /**
     * Set ith character.
     * Indexed from 1 to 8or9
     *
     * @param i
     * @param character
     */
    public void setCharacter(int i, Character character) {
        _characters[i-1] = character;
    }

    /**
     * Get the ith character.
     * Indexed from 1 to 8or9
     *
     * @param i
     * @return
     */
    public Character getCharacter(int i) {
        return _characters[i-1];
    }

    /**
     * Set availability of ith character
     * Indexed from 1 to 8or9
     *
     * @param i
     * @param available
     */
    public void setIsAvailable(int i, boolean available) {
        _available[i-1] = available;
    }

    /**
     * Get the availability of ith character.
     * Indexed from 1 to 8or9
     *
     * @param i
     * @return
     */
    public boolean isAvailable(int i) {
        return _available[i-1];
    }

    /**
     * Set if the ith character is revealed.
     * Indexed from 1 to 8or9
     *
     * @param i
     * @param revealed
     */
    public void setIsRevealed(int i, boolean revealed) {
        _revealed[i-1] = revealed;
    }

    /**
     * Get if ith character is revealed.
     * Indexed from 1 to 8or9
     *
     * @param i
     * @return
     */
    public boolean isRevealed(int i) {
        return _revealed[i-1];
    }

    public void setIsPlayed(int i, boolean played) {
        _played[i-1] = played;
    }

    public boolean isPlayed(int i) {
        return _played[i-1];
    }


    public void setFaceDown(int i) {
        _facedown = i;
    }

    public int getFaceDown() {
        return _facedown;
    }

    public void setAssassinated(int i) {
        _assassinated = i;
    }

    public int getAssassinated() {
        return _assassinated;
    }

    public void setTheived(int i) {
        _thieved = i;
    }

    public int getThieved() {
        return _thieved;
    }
    
    public void setBeWitched(int i) {
        _bewitched = i;
    }

    public int getBeWitched() {
        return _bewitched;
    }

    public boolean has9th() {
        return _has9th;
    }

    public int getSize() {
        return _characters.length;
    }

 }
