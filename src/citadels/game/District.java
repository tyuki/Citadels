/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package citadels.game;

import java.io.Serializable;
import java.util.LinkedList;
import java.util.List;

/**
 *
 * @author Personal
 */
public class District implements Serializable {

    private String _name;
    private DISTRICT_COLOR _scoreColor;
    private DISTRICT_COLOR _incomeColor;
    private int _cost;
    private int _score;
    private EFFECT _effect;
    private boolean _beautified = false;
    private int _roundBuilt;
    private List<District> _museumCards;

    public static enum DISTRICT_COLOR {
        RED, BLUE, GREEN, YELLOW, PURPLE, WILD, NONE
    }

    public static enum EFFECT {
        NONE, SMITHY, LABORATORY, LIBRARY, GREATWALL, OBSERVATORY, GRAVEYARD, KEEP, UNIVERSITY, DRAGONGATE, HAUNTEDCITY, SCHOOLOFMAGIC,
        POORHOUSE, IMPERIALTREASURY, PARK, FACTORY, WISHINGWELL, MAPROOM, THRONEROOM,
        LIGHTHOUSE, BELLTOWER, MUSEUM, ARMORY, HOSPITAL, QUARRY
    }

    public District(String name, DISTRICT_COLOR scoreCol, DISTRICT_COLOR incomeCol, int cost, int score, EFFECT effect) {
        _name = name;
        _scoreColor = scoreCol;
        _incomeColor = incomeCol;
        _cost = cost;
        _score = score;
        _effect = effect;
        _museumCards = new LinkedList<District>();
    }

    public String getName() {
        return _name;
    }

    public DISTRICT_COLOR getScoreColor() {
        return _scoreColor;
    }

    public DISTRICT_COLOR getIncomeColor() {
        return _incomeColor;
    }

    public int getCost() {
        return _cost;
    }

    public int getScore() {
        return _score;
    }

    public EFFECT getEffect() {
        return _effect;
    }

    public void setBeautified(boolean beautified) {
        _beautified = beautified;
    }

    public boolean getBeautified() {
        return _beautified;
    }

    public void setRoundBuilt(int roundBuilt) {
        _roundBuilt = roundBuilt;
    }

    public int getRoundBuilt() {
        return _roundBuilt;
    }

    public void addMuseumCards(District card) {
        _museumCards.add(card);
    }

    public List<District> getMuseumCards() {
        return _museumCards;
    }

}
