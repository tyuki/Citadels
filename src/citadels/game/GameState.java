/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package citadels.game;

import citadels.control.CitadelsController;
import citadels.game.Character.CHARACTER;
import citadels.game.District.DISTRICT_COLOR;
import citadels.game.District.EFFECT;
import citadels.game.Exceptions.GameException;
import citadels.game.Exceptions.GameInProgressException;
import citadels.game.Exceptions.NotEnoughPlayerException;
import citadels.game.Exceptions.TooManyPlayersException;
import citadels.game.GameMessage.ARGC1_MESSAGE;
import citadels.game.GameMessage.ARGC2_MESSAGE;
import citadels.game.GameMessage.ARGC3_MESSAGE;
import citadels.game.GameMessage.COLOR_TYPE;
import java.awt.Color;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;;

/**
 *
 * @author Personal
 */
public class GameState implements Serializable {

    public static final int BUILDING_SCORE = 0;
    public static final int COMPLETE_BONUS = 1;
    public static final int OTHER_BONUS = 2;

    public static final int INITIAL_COINS = 2;
    public static final int INITIAL_CARDS = 4;

    private static final int MINIMUM_PLAYERS = 4;

    public static enum BUILD_QUERY_RESPONSE {
        NOT_YOUR_TURN,
        ACTION_NOT_TAKEN,
        CAN_BUILD,
        CAN_BUILD_LIGHTHOUSE,
        CAN_BUILD_BELLTOWER,
        BUILDING_LIMIT,
        INSUFFICIENT_GOLD,
        DUPLICATE_DISTRICT
    }

    public static enum DISTRICT_INFLUENCE_RESPONSE {
        DESTROY,
        NO_GOLD_TO_DESTROY,
        EXCHANGE,
        EXCHANGE_CANDIDATE_SELF,
        EXCHANGE_CANDIDATE_OTHER,
        EXCHANGE_NO_OWN_DISTRICT,
        EXCHANGE_DUPLICATE,
        CITY_COMPLETED,
        BEAUTIFY,
        ALREADY_BEAUTIFIED,
        NO_GOLD_TO_BEAUTIFY,
        BEAUTIFY_LIMIT,
        PROTECTED,
        KEEP,
        NONE
    }

    private Progress _progress = new Progress();
    private List<Player> _players  = new ArrayList<Player>();
    private Deck _deck = null;

    private List<Color> _playerColors = new ArrayList<Color>();
    private int _currentColor = 0;

    private int _IDCount = 0;
    private GameOptions _options = null;
    private Characters _characters;

    public GameState() {
        initializePlayerColors();
    }

//    public GameState(boolean use9th, boolean shortGame, int[] expansionCards, EFFECT[] expansionDistricts) {
//        try {
//            _deck = new Deck(expansionCards, expansionDistricts);
//            initialize(use9th, shortGame, expansionCards);
//            initializePlayerColors();
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//    }
//
//    public GameState(boolean use9th, boolean shortGame, int[] expansionCards, int randomCardNum) {
//        try {
//            _deck = new Deck(expansionCards, randomCardNum);
//            initialize(use9th, shortGame, expansionCards);
//            initializePlayerColors();
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//    }

    private void initialize(GameOptions options) {
        try {
            _options = options;
            _characters = new Characters(_options.use9th, _options.expansionCards);
            if (_options.randomDistrictsNum > 0) {
                _deck = new Deck(_options.expansionCards, _options.randomDistrictsNum);
            } else {
                _deck = new Deck(_options.expansionCards, _options.expansionDistricts);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void initializePlayerColors() {
            _playerColors.clear();
            _currentColor = 0;
            _playerColors.add(Color.decode("#EFA4FF"));
            _playerColors.add(Color.decode("#A4FFFF"));
            _playerColors.add(Color.decode("#A4A4FF"));
            _playerColors.add(Color.decode("#E1FFA4"));
            _playerColors.add(Color.decode("#FFE1A4"));
            _playerColors.add(Color.decode("#FFA4A4"));
            _playerColors.add(Color.decode("#FFFFA4"));
            _playerColors.add(Color.decode("#A4FFA4"));
            Collections.shuffle(_playerColors);
    }

    public void startGame(GameOptions options) throws NotEnoughPlayerException {
        //check for minimum number of players
        if (_players.size() < MINIMUM_PLAYERS) throw new NotEnoughPlayerException(MINIMUM_PLAYERS + " players are required");

        initialize(options);

        //shuffle players
        Collections.shuffle(_players);
        //Collections.reverse(_players);
        
        //assign initial coins and hands
        for (Player player : _players) {
            player.setGold(INITIAL_COINS);
            for (int i = 0; i < INITIAL_CARDS; i++) {
                player.addHand(_deck.drawCard());
            }
        }

        //set king to the first player
        _players.get(0).setIsKing(true);
        _progress.crownID = _players.get(0).getID();

        //mark as started
        _progress.started = true;

        prepareNewRound();
    }

    /**
     *
     */
    public void prepareNewRound() {

        //Reset each players character
        for (Player player : _players) {
            player.setCharacter(null);
        }
        //Pick characters to remove from selection
        //find out number of characters to remove
        int face_down = 1;
        int face_up;
        if (_options.use9th) {
            face_up = Math.max(7 - _players.size(), 0);
        } else {
            face_up = Math.max(6 - _players.size(), 0);
        }
        //pick the characters randomly
        //shuffle an array with all numbers
        List<Integer> select = new ArrayList<Integer>();
        for (int i = 1; i <= _characters.getSize(); i++) {
            select.add(i);
        }
        //shuffle and pick the first x characters
        Collections.shuffle(select);
        //reset current state of characters
        _characters.initializeForNewRound();
        //disable face_down cards
        for (int i = 0; i < face_down; i++) {
            _characters.setIsAvailable(select.get(i), false);
            _characters.setFaceDown(select.get(i));
        }
        //other face up cards
        for (int i = face_down; i < face_down+face_up; i++) {
            //4 is King/Emperor that cannot be face up
            if (select.get(i) == 4) {
                _characters.setIsAvailable(select.get(face_down+face_up), false);
                _characters.setIsRevealed(select.get(face_down+face_up), true);
            } else {
                _characters.setIsAvailable(select.get(i), false);
                _characters.setIsRevealed(select.get(i), true);
            }
        }

        //Increment round count
        _progress.round++;
        //set current selection to the king
        _progress.currentSelectionID = _progress.crownID;
        _progress.currentSelectionIndex = _players.indexOf(getPlayer(_progress.crownID));
        //clear round info
        _progress.clearRoundInfo();
    }

    /**
     * Returns an instance of Player that has the given ID.
     *
     * @param id
     * @return
     * @throws GameException
     */
    public Player getPlayer(int id) {
        for (Player player : _players) {
            if (player.getID() == id) return player;
        }

        return null;
    }

    /**
     * Returns list of plyaers
     *
     * @return
     */
    public List<Player> getPlayers() {
        return _players;
    }

    /**
     * Adds a new player to the game
     *
     * @param name
     */
    public int addPlayer(String name, boolean AI) throws TooManyPlayersException, GameInProgressException {

        //If the player is already in the game, treat as re-join
        for (Player player : _players) {
            if (player.getName().compareTo(name) == 0) return player.getID();
        }
        
        //If the game is started, new players can't join
        if (_progress.started) {
            throw new GameInProgressException("Game is already in progress");
        }

        //Check for numbers
        if (_options == null && _players.size() >= 8) {
            throw new TooManyPlayersException("Too many players");
        } else if (_options != null && ((!_options.use9th && _players.size() >= 7) ||
            (_options.use9th && _players.size() >= 8))) {
            throw new TooManyPlayersException("Too many players");
        }

        int id = _IDCount;
        _IDCount++;
        _players.add(new Player(name, AI, id, _playerColors.get(_currentColor)));
        _currentColor++;
        _currentColor %= _playerColors.size();
        return id;

    }

    public ServerMessage removePlayer(int playerID) {
        Player target = null;
        for (Player player : _players) {
            if (player.getID() == playerID) {
                target = player;
                break;
            }
        }

        if (target == null) return null;

        _players.remove(target);

        return ServerMessage.singleArgMessage(ServerMessage.ARGC1_MESSAGE.LEAVE, target.getName(), ServerMessage.COLOR_TYPE.DEFAULT);
    }

    /**
     * Returns the current state of the deck
     *
     * @return
     */
    public Deck getDeck() {
        return _deck;
    }

    /**
     * 
     * @return
     */
    public Characters getCharacters() {
        return _characters;
    }

    public Progress getProgress() {
        return _progress;
    }

    public List<GameMessage> setCharacter(int playerID, int character) {
        if (_progress.currentSelectionID == playerID) {
            _characters.setIsAvailable(character, false);
            getPlayer(playerID).setCharacter(_characters.getCharacter(character));
            //Increment state to the next player
            _progress.currentSelectionIndex++;
            _progress.currentSelectionIndex %= _players.size();
            _progress.currentSelectionID = _players.get(_progress.currentSelectionIndex).getID();
            //one before end and full player game, face down card can be used by the last player
            if (((!_options.use9th && _players.size() >= 7) ||
                (_options.use9th && _players.size() >= 8)) &&
                (_players.get((_progress.currentSelectionIndex + 1) % _players.size()).getIsKing())) {
                _characters.setIsAvailable(_characters.getFaceDown(), true);
                _characters.setFaceDown(-1);
            }
            List<GameMessage> res = new LinkedList<GameMessage>();
            res.add(GameMessage.singleArgMessage(ARGC1_MESSAGE.CHARACTER_SELECTED, getPlayer(playerID).getName(), COLOR_TYPE.PLAYER, playerID));
            //End of selection
            if (_players.get((_progress.currentSelectionIndex) % _players.size()).getIsKing()) {
                _progress.roleSelection = false;
                //find starting character
                _progress.currentCharacter = 0;
                _progress.currentCharacter = findNextCharacter();
                _progress.clearPlayerInfo();
                //Beginning of the turn handling
                res.addAll(beginningOfTurnHandling());
            }
            return res;
        }
        return null;
    }

    public List<GameMessage> actionCoins(int playerID) {
        if (!_progress.actionTaken && validateUnbewitchablePower(playerID)) {
            _progress.actionTaken = true;
            getPlayer(playerID).addGold(2);

            List<GameMessage> res = new LinkedList<GameMessage>();
            res.add(GameMessage.singleArgMessage(ARGC1_MESSAGE.ACTION_GOLD, getPlayer(playerID).getName(), COLOR_TYPE.PLAYER, playerID));
            res.addAll(endOfActionHandling(playerID));
            return res;
        }
        return null;
    }

    public List<GameMessage> actionCards(int playerID, int[] cards) {
        if (!_progress.actionTaken && validateUnbewitchablePower(playerID)) {
            _progress.actionTaken = true;
            
            List<District> selection = this.getActionCardsChoice(playerID);
            int keep = this.getActionCardsKeepNum(playerID);

            if (keep != cards.length) {
                System.out.println("keep num mismatch");
                return null;
            }

            //Draw the number of cards
            for (int i = 0; i < selection.size(); i++) {
                _deck.drawCard();
            }

            //Add the selected ones
            for (int i = 0; i < cards.length; i++) {
                getPlayer(playerID).addHand(selection.get(cards[i]));
            }

            List<GameMessage> res = new LinkedList<GameMessage>();
            res.add(GameMessage.singleArgMessage(ARGC1_MESSAGE.ACTION_CARDS, getPlayer(playerID).getName(), COLOR_TYPE.PLAYER, playerID));
            res.addAll(endOfActionHandling(playerID));
            return res;
        }
                System.out.println("action card error");

        return null;
    }

    private List<GameMessage> endOfActionHandling(int playerID) {
        List<GameMessage> res = new LinkedList<GameMessage>();

        //If bewitched, turn ends here
        if (_progress.currentCharacter == _characters.getBeWitched()) {
            res.add(GameMessage.singleArgMessage(ARGC1_MESSAGE.SA_BEWITCH_BEWITCHED, getPlayer(playerID).getName(), COLOR_TYPE.PLAYER, playerID));
            res.add(GameMessage.singleArgMessage(ARGC1_MESSAGE.SA_BEWITCH_RESUME_TURN, getPlayer(getOriginalPlayerIDofChar(CHARACTER.WITCH.order)).getName(), COLOR_TYPE.PLAYER, getOriginalPlayerIDofChar(CHARACTER.WITCH.order)));
        }

        //If assassinated and in hospital, turn ends here
        if (_progress.hospitalUsed) {
            res.addAll(endTurn(playerID));
            return res;
        }

        //Merchant, Architect, Abbot, Queen
        switch(_characters.getCharacter(_progress.currentCharacter).character) {
            case MERCHANT:
                res.add(merchantAbility(getEnpoweredPlayerIDofChar(_progress.currentCharacter)));
                break;
            case ARCHITECT:
                res.add(architectAbility(getEnpoweredPlayerIDofChar(_progress.currentCharacter)));
                break;
            case QUEEN:
                res.add(queenAbility(getEnpoweredPlayerIDofChar(_progress.currentCharacter), false));
                break;
            case EMPEROR:
                //If Emperor is bewitched, must use special ability
                if (_characters.getBeWitched() == CHARACTER.EMPEROR.order) {
                    _progress.useEmperorPower = true;
                }
                break;
        }

        return res;
    }

    public List<GameMessage> endTurn(int playerID) {
        //bewitching needs to be taken care of
        if (_progress.actionTaken && getEnpoweredPlayerIDofChar(_progress.currentCharacter) == playerID) {
            List<GameMessage> messages = new LinkedList<GameMessage>();
            messages.add(GameMessage.singleArgMessage(ARGC1_MESSAGE.END_TURN, getPlayer(playerID).getName(), COLOR_TYPE.PLAYER, playerID));
            //End of the turn handling
            messages.addAll(endOfTurnHandling());
            _progress.clearPlayerInfo();
            //Find next character
            int nextChar = findNextCharacter();
            if (nextChar < 10) {
                _progress.currentCharacter = nextChar;
                //Beginning of the turn handling
                messages.addAll(beginningOfTurnHandling());
            } else {
                messages.addAll(endOfRoundHandling());
                //End the game if city is completed
                if (_progress.cityCompleted) {
                   messages.addAll(endGame());
                //Otherwise, start next round
                } else {
                    prepareNewRound();
                   messages.add(GameMessage.singleArgMessage(ARGC1_MESSAGE.ROUND_START, Integer.toString(_progress.round), COLOR_TYPE.DEFAULT, 0));
                }
            }
            return messages;
        }
        return null;
    }

    private List<GameMessage> endOfTurnHandling() {
        List<GameMessage> res = new LinkedList<GameMessage>();

        //Tax collector goes first
        //if tax collector is played, and district has been built, pay one gold
        if (_characters.isPlayed(CHARACTER.TAX_COLLECTOR.order) &&
                _characters.getCharacter(CHARACTER.TAX_COLLECTOR.order).character == CHARACTER.TAX_COLLECTOR &&
                _progress.districtBuilt > 0) {
                //Tax collector
                res.add(taxCollectorAbility(getEnpoweredPlayerIDofChar(CHARACTER.TAX_COLLECTOR.order), getEnpoweredPlayerIDofChar(_progress.currentCharacter)));
        }
        //
        switch (_characters.getCharacter(_progress.currentCharacter).character) {
            //Alchemist receives gold spent on districts back
            case ALCHEMIST:
                res.add(alchemistAbility(getEnpoweredPlayerIDofChar(_progress.currentCharacter)));
                break;
        }

        //Building turn end abilities are only used when the end of the original player's turn in case of bewitch
        int originalID = getOriginalPlayerIDofChar(_progress.currentCharacter);
        int witchID    = getEnpoweredPlayerIDofChar(_progress.currentCharacter);
        if (originalID == witchID) {
            Player player = getPlayer(getEnpoweredPlayerIDofChar(_progress.currentCharacter));
            //Poor House
            if (player.hasDistrictEffect(EFFECT.POORHOUSE) && player.getGold() == 0) {
                player.addGold(1);
                res.add(GameMessage.singleArgMessage(ARGC1_MESSAGE.DA_POORHOUSE, player.getName(), COLOR_TYPE.PLAYER, player.getID()));
            }
            //Park
            if (player.hasDistrictEffect(EFFECT.PARK) && player.getHand().size() == 0) {
                player.addHand(_deck.drawCard());
                player.addHand(_deck.drawCard());
                res.add(GameMessage.singleArgMessage(ARGC1_MESSAGE.DA_PARK, player.getName(), COLOR_TYPE.PLAYER, player.getID()));
            }
        }

        //check for building completion
        int citySize = getPlayer(getEnpoweredPlayerIDofChar(_progress.currentCharacter)).getCity().size();
        boolean completed = citySize >= getCityCompleteCondition();
        //First player to complete
        if (completed && !_progress.cityCompleted) {
            _progress.cityCompleted = completed;
            getPlayer(getEnpoweredPlayerIDofChar(_progress.currentCharacter)).setCompletedFirst(true);
            res.add(GameMessage.singleArgMessage(ARGC1_MESSAGE.CITY_COMPLETED, getPlayer(getEnpoweredPlayerIDofChar(_progress.currentCharacter)).getName(), COLOR_TYPE.PLAYER, getEnpoweredPlayerIDofChar(_progress.currentCharacter)));
        }

        return res;
    }

    private List<GameMessage> beginningOfTurnHandling() {

        List<GameMessage> res = new LinkedList<GameMessage>();
        //Thief
        if (_progress.currentCharacter == _characters.getThieved()) {
            //Thief
            res.add(thiefAbility(getEnpoweredPlayerIDofChar(CHARACTER.THIEF.order)));
        }

        //Hospital
        if (_progress.hospitalUsed) {
            int playerID = this.getOriginalPlayerIDofChar(_progress.currentCharacter);
            res.add(GameMessage.singleArgMessage(ARGC1_MESSAGE.DA_HOSPITAL, getPlayer(playerID).getName(), COLOR_TYPE.PLAYER, playerID));
        }
        
        switch (_characters.getCharacter(_progress.currentCharacter).character) {
            case TAX_COLLECTOR:
                //Tax Collector
                if (_progress.didFirstCharacterBuild) {
                    //Take from Assassin/Witch
                   res.add(taxCollectorAbility(getEnpoweredPlayerIDofChar(_progress.currentCharacter), getEnpoweredPlayerIDofChar(CHARACTER.ASSASSIN.order)));
                }
                break;
            case KING:
                //Move the Crown
                res.addAll(moveCrown(getOriginalPlayerIDofChar(_progress.currentCharacter)));
                break;
            case EMPEROR:
                //If Emperor, must use special ability
                if (_characters.getBeWitched() != CHARACTER.EMPEROR.order) {
                    _progress.useEmperorPower = true;
                }
                break;
        }

        return res;
    }

    private List<GameMessage> endOfRoundHandling() {
        List<GameMessage> res = new LinkedList<GameMessage>();

        //If King was assassinated
        if (_characters.getCharacter(CHARACTER.KING.order).character == CHARACTER.KING &&
                _characters.getAssassinated() == CHARACTER.KING.order && !_characters.isPlayed(CHARACTER.KING.order)) {
                //Move the Crown
                res.addAll(moveCrown(getOriginalPlayerIDofChar(CHARACTER.KING.order)));
                //Queen
                res.add(queenAbility(getEnpoweredPlayerIDofChar(CHARACTER.QUEEN.order), true));
        }

        return res;
    }

    private List<GameMessage> endGame() {
        List<GameMessage> res = new LinkedList<GameMessage>();

        //End the game
        _progress.ended = true;

        for (Player player : _players) {
            player.setFinalScore(computeScore(player));
        }

        //Collections.sort(_players);

        res.add(GameMessage.singleArgMessage(ARGC1_MESSAGE.GAME_END, "", COLOR_TYPE.DEFAULT, 0));
        for (Player player : _players) {
            res.add(GameMessage.coupleArgMessage(ARGC2_MESSAGE.SCORE_REPORT, player.getName(), Integer.toString(player.getFinalScore()), COLOR_TYPE.DEFAULT, 0));
        }

        return res;
    }

    public int computeScore(Player player) {
        int[] score = computeScoreDetailed(player);

        int sum = 0;
        for (int e : score) {
            sum+=e;
        }

        return sum;
    }

    public int[] computeScoreDetailed(Player player) {
        int[] score = new int[]{0,0,0};

        boolean seenYellow = false;
        boolean seenBlue = false;
        boolean seenGreen = false;
        boolean seenRed = false;
        boolean seenPurple = false;
        int colorsSeen = 0;
        int wildCard = 0;
        //int
        for (District district : player.getCity()) {
            //row district score
            score[BUILDING_SCORE] += district.getScore();
            //+1 if beautified
            if (district.getBeautified()) score[OTHER_BONUS]++;
            //colors
            switch (district.getScoreColor()) {
                case YELLOW:
                    if (!seenYellow) {seenYellow=true; colorsSeen++;}
                    break;
                case BLUE:
                    if (!seenBlue) {seenBlue=true; colorsSeen++;}
                    break;
                case GREEN:
                    if (!seenGreen) {seenGreen=true; colorsSeen++;}
                    break;
                case RED:
                    if (!seenRed) {seenRed=true; colorsSeen++;}
                    break;
                case PURPLE:
                    if (!seenPurple) {seenPurple=true; colorsSeen++;}
                    break;
                case WILD:
                    if (district.getRoundBuilt() < _progress.round) {wildCard++;}
                    break;

            }
            
        }

        //+3 for completing all four colors
        if (colorsSeen + wildCard == 5) score[COMPLETE_BONUS]+=3;

        //First player to complete gets 4
        if (player.getCompletedFirst()) {
            score[COMPLETE_BONUS]+=4;
        //Others get 2 for completing in the final round
        } else if (player.getCity().size() >= getCityCompleteCondition()) {
            score[COMPLETE_BONUS]+=2;
        }

        //Building additions
        //Imperial Treasury
        if (player.hasDistrictEffect(EFFECT.IMPERIALTREASURY)) {
            //Get scores for gold
            score[OTHER_BONUS] += player.getGold();
        }

        //Wishing Well
        if (player.hasDistrictEffect(EFFECT.WISHINGWELL)) {
            //Count number of purple buildings
            int purpleNum = 0;
            for (District district : player.getCity()) {
                if (district.getEffect() != EFFECT.NONE) purpleNum++;
            }
            //Add score the number of purple districts - 1 (for itself)
            score[OTHER_BONUS] += Math.max(purpleNum - 1, 0);
        }

        //Map Room
        if (player.hasDistrictEffect(EFFECT.MAPROOM)) {
            //Get scores for cards in the hand
            score[OTHER_BONUS] += player.getHand().size();
        }

        //Museum
        if (player.hasDistrictEffect(EFFECT.MUSEUM)) {
            //Get score from the
            for (District district : player.getCity()) {
                if (district.getEffect() == EFFECT.MUSEUM) {
                    score[OTHER_BONUS] += district.getMuseumCards().size();
                    break;
                }
            }
        }


        return score;
    }

    public GameMessage earnDistrictIncome(int playerID) {

        if (!_progress.districtIncomeGained && validateBewitchablePower(playerID)) {
            _progress.districtIncomeGained = true;

            int income = 0;

            DISTRICT_COLOR charColor = _characters.getCharacter(_progress.currentCharacter).color;
            for (District district : getPlayer(playerID).getCity()) {
                if (district.getIncomeColor() == charColor || district.getIncomeColor() == DISTRICT_COLOR.WILD) {
                    income++;
                }
            }

            getPlayer(playerID).addGold(income);

            return GameMessage.coupleArgMessage(ARGC2_MESSAGE.DISTRICT_INCOME, getPlayer(playerID).getName(), Integer.toString(income), COLOR_TYPE.PLAYER, playerID);
        }

        return null;
        
    }

    public GameMessage buildDistrict(int playerID, int districtNum) {

        BUILD_QUERY_RESPONSE res = canBulildDistrict(playerID, districtNum);
        if (res == BUILD_QUERY_RESPONSE.CAN_BUILD ||
               res == BUILD_QUERY_RESPONSE.CAN_BUILD_LIGHTHOUSE ||
               res == BUILD_QUERY_RESPONSE.CAN_BUILD_BELLTOWER) {
            District dist = getPlayer(playerID).getHand().get(districtNum);
            dist.setRoundBuilt(_progress.round);
           getPlayer(playerID).addDistrict(dist);
           getPlayer(playerID).removeHand(dist);

           //pay for the district
           getPlayer(playerID).addGold(-dist.getCost());
           //If the district was puruple, and the player has Factory, give 1 gold back
           if (dist.getEffect() != EFFECT.NONE && getPlayer(playerID).hasDistrictEffect(EFFECT.FACTORY)) {
                getPlayer(playerID).addGold(1);
           }

           //update other info
           _progress.districtBuilt++;
           _progress.goldSpentOnDistrict+=dist.getCost();
           if (_progress.currentCharacter == 1) {
                _progress.didFirstCharacterBuild = true;
           }
            return GameMessage.coupleArgMessage(ARGC2_MESSAGE.BUILD_DISTRICT, getPlayer(playerID).getName(), dist.getName(), COLOR_TYPE.PLAYER, playerID);
        }

        return null;
    }

    private int getOriginalPlayerIDofChar(int character) {
       for (Player player : _players) {
           if (player.getCharacter().order == character) return player.getID();
       }

       return -1;
    }

    private int getEnpoweredPlayerIDofChar(int character) {

        if (_characters.getBeWitched() == character) {
            return getOriginalPlayerIDofChar(CHARACTER.WITCH.order);
        } else {
            return getOriginalPlayerIDofChar(character);
        }
    }

    private List<GameMessage> moveCrown(int newPlayerID) {
        if (_progress.crownID != newPlayerID && newPlayerID >= 0) {
            getPlayer(_progress.crownID).setIsKing(false);
            _progress.crownID = newPlayerID;
            getPlayer(newPlayerID).setIsKing(true);

            List<GameMessage> res = new LinkedList<GameMessage>();
            //message for moving crown
            res.add(GameMessage.singleArgMessage(ARGC1_MESSAGE.SA_KING, getPlayer(newPlayerID).getName(), COLOR_TYPE.PLAYER, newPlayerID));
            //Throne Room
            for (Player player : _players) {
                if (player.hasDistrictEffect(EFFECT.THRONEROOM)) {
                    //Give one gold
                    player.addGold(1);
                    res.add(GameMessage.singleArgMessage(ARGC1_MESSAGE.DA_THRONEROOM, player.getName(), COLOR_TYPE.PLAYER, player.getID()));
                    break;
                }
            }

            return res;
        }
        return new LinkedList<GameMessage>();
    }

    public GameMessage assassinAnnounce(int playerID, int target) {
        if (!_progress.special1used && validateUnbewitchablePower(playerID) && _progress.currentCharacter == CHARACTER.ASSASSIN.order) {
            _progress.special1used = true;
            _characters.setAssassinated(target);
            return GameMessage.coupleArgMessage(ARGC2_MESSAGE.SA_ASSASSIN, getPlayer(playerID).getName(), _characters.getCharacter(target).name, COLOR_TYPE.PLAYER, playerID);
        }
        return null;
    }

    public GameMessage thiefAnnounce(int playerID, int target) {
        if (!_progress.special1used && validateBewitchablePower(playerID) && _progress.currentCharacter == CHARACTER.THIEF.order) {
            _progress.special1used = true;
            _characters.setTheived(target);
            return GameMessage.coupleArgMessage(ARGC2_MESSAGE.SA_THIEF_ANNOUNCE, getPlayer(playerID).getName(), _characters.getCharacter(target).name, COLOR_TYPE.PLAYER, playerID);
        }
        return null;
    }

    public List<GameMessage> witchAnnounce(int playerID, int target) {
        if (!_progress.special1used && validateUnbewitchablePower(playerID) && _progress.currentCharacter == CHARACTER.WITCH.order) {
            _progress.special1used = true;
            _characters.setBeWitched(target);
            
            List<GameMessage> res = new LinkedList<GameMessage>();
            res.add(GameMessage.coupleArgMessage(ARGC2_MESSAGE.SA_WITCH, getPlayer(playerID).getName(), _characters.getCharacter(target).name, COLOR_TYPE.PLAYER, playerID));
            //Now the player must finish the turn
            System.out.println("Calling end of turn " + playerID);
            res.addAll(endTurn(playerID));
            return res;
        }
        return null;
    }

    public GameMessage thiefAbility(int playerID) {
        //verify
        if (this.getEnpoweredPlayerIDofChar(CHARACTER.THIEF.order) == playerID) {
            Player stolenPlayer = getPlayer(getOriginalPlayerIDofChar(_progress.currentCharacter));
            int goldTaken = stolenPlayer.getGold();
            //move all the gold to the thief
            getPlayer(playerID).addGold(goldTaken);
            stolenPlayer.addGold(-goldTaken);

            return GameMessage.tripleArgMessage(GameMessage.ARGC3_MESSAGE.SA_THIEF_STEAL, getPlayer(playerID).getName(), Integer.toString(goldTaken), stolenPlayer.getName(), COLOR_TYPE.PLAYER, playerID);
        }
        return null;
    }

    public GameMessage magicianAbilitySwap(int playerID, int targetID) {
        if (!_progress.special1used && !_progress.special2used && validateBewitchablePower(playerID) && _progress.currentCharacter == CHARACTER.MAGICIAN.order) {
            _progress.special1used = true;
            _progress.special2used = true;
            //Swap hand completely
            List<District> handPlayer = getPlayer(playerID).getHand();
            List<District> handTarget = getPlayer(targetID).getHand();
            getPlayer(playerID).setHand(handTarget);
            getPlayer(targetID).setHand(handPlayer);
            return GameMessage.coupleArgMessage(ARGC2_MESSAGE.SA_MAGICIAN_SWAP, getPlayer(playerID).getName(), getPlayer(targetID).getName(), COLOR_TYPE.PLAYER, playerID);
        }
        return null;
    }

    public GameMessage magicianAbilityExchange(int playerID, int[] cards) {
        if (!_progress.special1used && !_progress.special2used && validateBewitchablePower(playerID) && _progress.currentCharacter == CHARACTER.MAGICIAN.order) {
            _progress.special1used = true;
            _progress.special2used = true;

            List<District> hand = getPlayer(playerID).getHand();
            //pick those that are discarded
            List<District> discards = new ArrayList<District>();
            for (int i = 0; i < cards.length; i++) {
                discards.add(hand.get(cards[i]));
            }

            //Remove them from the hand
            hand.removeAll(discards);

            //Put them back into the deck
            _deck.addToTheBottom(discards);

            //Draw new cards
            for (int i = 0; i < cards.length; i++) {
                hand.add(_deck.drawCard());
            }


            return GameMessage.coupleArgMessage(ARGC2_MESSAGE.SA_MAGICIAN_EXCHANGE, getPlayer(playerID).getName(), Integer.toString(cards.length), COLOR_TYPE.PLAYER, playerID);
        }
        return null;
    }

    public GameMessage merchantAbility(int playerID) {

        if (!_progress.special1used && validateBewitchablePower(playerID) && _progress.currentCharacter == CHARACTER.MERCHANT.order) {
            _progress.special1used = true;
            getPlayer(playerID).addGold(1);
            return GameMessage.singleArgMessage(ARGC1_MESSAGE.SA_MERCHANT, getPlayer(playerID).getName(), COLOR_TYPE.PLAYER, playerID);
        }
        return null;
    }

    public GameMessage architectAbility(int playerID) {
        if (!_progress.special1used && validateBewitchablePower(playerID) && _progress.currentCharacter == CHARACTER.ARCHITECT.order) {
            _progress.special1used = true;
            getPlayer(playerID).addHand(_deck.drawCard());
            getPlayer(playerID).addHand(_deck.drawCard());
            return GameMessage.singleArgMessage(ARGC1_MESSAGE.SA_ARCHITECT, getPlayer(playerID).getName(), COLOR_TYPE.PLAYER, playerID);
        }
        return null;
    }

    public List<GameMessage> warlordAbility(int playerID, int targetPlayerID, int targetDistrict) {
        if (!_progress.special1used && validateBewitchablePower(playerID) && _progress.currentCharacter == CHARACTER.WARLORD.order) {
            //Verify
            if (canInfluenceDistrict(playerID, targetPlayerID, targetDistrict) == DISTRICT_INFLUENCE_RESPONSE.DESTROY) {
                _progress.special1used = true;
                //Pay the cost
                int cost = this.computeCostToDestroy(targetPlayerID, targetDistrict);
                getPlayer(playerID).addGold(-cost);
                //Remove the district
                District target = getPlayer(targetPlayerID).getCity().get(targetDistrict);
                getPlayer(targetPlayerID).removeDistrict(target);
                //cancel belltowers, power if used
                if (target.getEffect() == EFFECT.BELLTOWER) _progress.bellTowerUsed = false;
                //Create message
                List<GameMessage> res = new LinkedList<GameMessage>();
                res.add(GameMessage.tripleArgMessage(GameMessage.ARGC3_MESSAGE.SA_WARLORD, getPlayer(playerID).getName(), getPlayer(targetPlayerID).getName(), target.getName(), COLOR_TYPE.PLAYER, playerID));

                //Check if someone has graveyard
                boolean hasGraveyard = false;
                int graveYardID = -1;
                for (Player player : _players) {
                    //skip your self
                    if (player.getID() == playerID) continue;
                    if (player.hasDistrictEffect(EFFECT.GRAVEYARD)) {
                        //And has at least 1 gold
                        if (player.getGold() > 0) {
                            hasGraveyard = true;
                            graveYardID = player.getID();
                        }
                        break;
                    }
                }

                //if there is a grave yard
                if (hasGraveyard && graveYardID >= 0) {
                    //Setup for asking graveyard
                    _deck.setGraveyardStop(target);
                    _progress.graveYardID = graveYardID;
                    return res;
                //If there is no graveyard, put the card back to the bottom and end the turn
                } else {
                    //Put it back to the deck
                    _deck.addToTheBottom(target);

                    //Now the player must finish the turn
                    res.addAll(endTurn(playerID));
                    return res;
                }
            }
        }
        return null;
    }

    public GameMessage queenAbility(int playerID, boolean atEnd) {
        //verify
        if (!_progress.special1used && validateBewitchablePower(playerID) && _progress.currentCharacter == CHARACTER.QUEEN.order) {
            _progress.special1used = true;

            //If 4th Character is not the king, do nothing
            if (_characters.getCharacter(CHARACTER.KING.order).character != CHARACTER.KING) return null;

            //Check if King is neighbor
            int kingIndex = -1;
            int playerIndex = -1;
            for (int i = 0; i < _players.size(); i++) {
                if (_players.get(i).getCharacter().character == CHARACTER.KING) kingIndex = i;
                if (_players.get(i).getID() == playerID) playerIndex = i;
            }

            //check if they are neighbors and King is used
            boolean isNeighbor = kingIndex >= 0 && (((kingIndex+1)%_players.size()==playerIndex) || ((playerIndex+1)%_players.size()==kingIndex));

            //If not neighbor do nothing
            if (!isNeighbor) return null;
            
            //After action
            if (!atEnd) {
                //If king was played
                if (_characters.isPlayed(CHARACTER.KING.order)) {
                    //Add 3gold
                    getPlayer(playerID).addGold(3);
                    return GameMessage.singleArgMessage(ARGC1_MESSAGE.SA_QUEEN, getPlayer(playerID).getName(), COLOR_TYPE.PLAYER, playerID);
                }
            //End of game
            } else {
                //If king was assassinated
                if (_characters.getAssassinated() == CHARACTER.KING.order && !_characters.isPlayed(CHARACTER.KING.order)) {
                    //Add 3gold
                    getPlayer(playerID).addGold(3);
                    return GameMessage.singleArgMessage(ARGC1_MESSAGE.SA_QUEEN, getPlayer(playerID).getName(), COLOR_TYPE.PLAYER, playerID);
                }
            }
            

        }

        return null;
    }

    public GameMessage taxCollectorAbility(int playerID, int victimID) {
        //verify
        if (this.getEnpoweredPlayerIDofChar(CHARACTER.TAX_COLLECTOR.order) == playerID && playerID != victimID) {
            //Collect if the victim has gold
            if (getPlayer(victimID).getGold() > 0) {
                getPlayer(playerID).addGold(1);
                getPlayer(victimID).addGold(-1);
                return GameMessage.coupleArgMessage(GameMessage.ARGC2_MESSAGE.SA_TAX_COLLECTOR, getPlayer(playerID).getName(), getPlayer(victimID).getName(), COLOR_TYPE.PLAYER, playerID);
            }
        }
        return null;
    }

    public GameMessage wizardAbility(int playerID, int targetID, int targetCard, boolean build) {
        //verify
        if (!_progress.special1used && validateBewitchablePower(playerID) && _progress.currentCharacter == CHARACTER.WIZARD.order) {
            //take the card
            District cardTaken = getPlayer(targetID).getHand().get(targetCard);
            getPlayer(targetID).removeDistrict(cardTaken);
            //Build the taken district right away
            if (build) {
                //pay for it
                getPlayer(playerID).addGold(-cardTaken.getCost());
                getPlayer(playerID).addDistrict(cardTaken);
                return GameMessage.tripleArgMessage(ARGC3_MESSAGE.SA_WIZARD_TAKE_AND_BUILD, getPlayer(playerID).getName(), getPlayer(targetID).getName(), cardTaken.getName(), COLOR_TYPE.PLAYER, playerID);
            //Add to the hand
            } else {
                getPlayer(playerID).addHand(cardTaken);
                return GameMessage.coupleArgMessage(ARGC2_MESSAGE.SA_WIZARD_TAKE, getPlayer(playerID).getName(), getPlayer(targetID).getName(), COLOR_TYPE.PLAYER, playerID);
            }
        }
        return null;
    }

    public List<GameMessage> emperorAbility(int playerID, int newCrownID) {
        //verify
        if (!_progress.special1used && _progress.useEmperorPower && validateBewitchablePower(playerID) && _progress.currentCharacter == CHARACTER.EMPEROR.order) {
            _progress.useEmperorPower = false;
            //If the new Crown has coin/cards, take rewads
            int gold = getPlayer(newCrownID).getGold();
            int numCards = getPlayer(newCrownID).getHand().size();

            List <GameMessage> res = new LinkedList<GameMessage>();
            res.add(GameMessage.coupleArgMessage(ARGC2_MESSAGE.SA_EMPEROR, getPlayer(playerID).getName(), getPlayer(newCrownID).getName(), COLOR_TYPE.PLAYER, newCrownID));

            //When both are 0
            if (gold == 0 && numCards == 0) {
                //Emperor gets nothing
            //If there is no card, get gold
            } else if (numCards == 0) {
                //Take Gold
                _progress.crownedByEmperorID = newCrownID;
                res.add(tributeToEmperorGold(newCrownID));
            //If only one card available, no choice but to give that card
            } else if (gold == 0 && numCards == 1) {
                //Take Gold
                _progress.crownedByEmperorID = newCrownID;
                res.add(tributeToEmperorCard(newCrownID, 0));
            //It is their choice if undecidable
            } else {
                _progress.crownedByEmperorID = newCrownID;
            }

            //Move the crown
            List<GameMessage> moveCrownRes = moveCrown(newCrownID);
            //Throne room ability part is added
            if (moveCrownRes.size() == 2) {
                res.add(moveCrownRes.get(1));
            }

            return res;
        }
        return null;
    }

    public GameMessage tributeToEmperorGold(int playerID) {
        //verify
        if (_progress.crownedByEmperorID == playerID) {
            _progress.crownedByEmperorID = -1;

            Player crown = getPlayer(playerID);
            Player emperor = getPlayer(getEnpoweredPlayerIDofChar(CHARACTER.EMPEROR.order));

            //Give emperor 1 gold
            emperor.addGold(1);
            crown.addGold(-1);

            return GameMessage.coupleArgMessage(ARGC2_MESSAGE.SA_EMPEROR_TRIBUTE_GOLD, crown.getName(), emperor.getName(), COLOR_TYPE.PLAYER, emperor.getID());
        }
        return null;
    }

    public GameMessage tributeToEmperorCard(int playerID, int cardNum) {
        //verify
        if (_progress.crownedByEmperorID == playerID) {
            _progress.crownedByEmperorID = -1;

            Player crown = getPlayer(playerID);
            Player emperor = getPlayer(getEnpoweredPlayerIDofChar(CHARACTER.EMPEROR.order));
            District card = crown.getHand().get(cardNum);

            //Give emperor the card
            emperor.addHand(card);
            crown.removeHand(card);

            return GameMessage.coupleArgMessage(ARGC2_MESSAGE.SA_EMPEROR_TRIBUTE_CARD, crown.getName(), emperor.getName(), COLOR_TYPE.PLAYER, emperor.getID());
        }
        return null;
    }

    public GameMessage abbotAbility(int playerID) {
        //verify
        if (!_progress.special1used && validateBewitchablePower(playerID) && _progress.currentCharacter == CHARACTER.ABBOT.order) {
            _progress.special1used = true;
            //Check who has the most money
            //calculate max
            int max = 0;
            for (Player player : _players) {
                max = Math.max(player.getGold(), max);
            }
            //Find the player
            Player maxPlayer = null;
            int count = 0;
            for (Player player : _players) {
                if (player.getGold() == max) {
                    maxPlayer = player;
                    count++;
                }
            }

            //If there is no tie and, the max player is not yourself
            if (count == 1 && maxPlayer.getID() != playerID) {
                //Take one gold
                getPlayer(playerID).addGold(1);
                maxPlayer.addGold(-1);
                return GameMessage.coupleArgMessage(ARGC2_MESSAGE.SA_ABBOT, getPlayer(playerID).getName(), maxPlayer.getName(), COLOR_TYPE.PLAYER, playerID);
            } else {
                return GameMessage.singleArgMessage(ARGC1_MESSAGE.SA_ABBOT_FAILED, getPlayer(playerID).getName(), COLOR_TYPE.PLAYER, playerID);
            }
        }
        return null;
    }

    public GameMessage alchemistAbility(int playerID) {
        //verify
        if (!_progress.special1used && validateBewitchablePower(playerID) && _progress.currentCharacter == CHARACTER.ALCHEMIST.order) {
            _progress.special1used = true;
            getPlayer(playerID).addGold(_progress.goldSpentOnDistrict);
            return GameMessage.coupleArgMessage(ARGC2_MESSAGE.SA_ALCHEMIST, getPlayer(playerID).getName(), Integer.toString(_progress.goldSpentOnDistrict), COLOR_TYPE.PLAYER, playerID);
        }
        return null;
    }

    public GameMessage navigatorAbilityGold(int playerID) {
        //verify
        if (!_progress.special1used && !_progress.special2used && validateBewitchablePower(playerID) && _progress.currentCharacter == CHARACTER.NAVIGATOR.order) {
            _progress.special1used = true;
            _progress.special2used = true;
            getPlayer(playerID).addGold(4);
            return GameMessage.singleArgMessage(ARGC1_MESSAGE.SA_NAVIGATOR_GOLD, getPlayer(playerID).getName(), COLOR_TYPE.PLAYER, playerID);
        }
        return null;
    }

    public GameMessage navigatorAbilityCards(int playerID) {
        //verify
        if (!_progress.special1used && !_progress.special2used && validateBewitchablePower(playerID) && _progress.currentCharacter == CHARACTER.NAVIGATOR.order) {
            _progress.special1used = true;
            _progress.special2used = true;
            getPlayer(playerID).addHand(_deck.drawCard());
            getPlayer(playerID).addHand(_deck.drawCard());
            getPlayer(playerID).addHand(_deck.drawCard());
            getPlayer(playerID).addHand(_deck.drawCard());
            return GameMessage.singleArgMessage(ARGC1_MESSAGE.SA_NAVIGATOR_CARDS, getPlayer(playerID).getName(), COLOR_TYPE.PLAYER, playerID);
        }
        return null;
    }

    public List<GameMessage> diplomatAbility(int playerID, int playerDistrict, int targetID, int targetDistrict) {
        //verify
        if (!_progress.special1used && validateBewitchablePower(playerID) && _progress.currentCharacter == CHARACTER.DIPLOMAT.order) {
            _progress.special1used = true;

            int cost = computeCostToExchange(playerID, playerDistrict, targetID, targetDistrict);

            //Pay the difference
            getPlayer(playerID).addGold(-cost);
            //Exchange districts
            District give = getPlayer(playerID).getCity().get(playerDistrict);
            District take = getPlayer(targetID).getCity().get(targetDistrict);
            getPlayer(playerID).removeDistrict(give);
            getPlayer(targetID).removeDistrict(take);
            getPlayer(playerID).addDistrict(take);
            getPlayer(targetID).addDistrict(give);
            //Create message
            List<GameMessage> res = new LinkedList<GameMessage>();
            res.add(GameMessage.quadArgMessage(GameMessage.ARGC4_MESSAGE.SA_DIPLOMAT, getPlayer(playerID).getName(), give.getName(), getPlayer(targetID).getName(), take.getName(), COLOR_TYPE.PLAYER, playerID));
            //Now the player must finish the turn
            res.addAll(endTurn(playerID));
            return res;
        }

        return null;
    }

    public GameMessage artistAbility(int playerID, int district) {
        //verify
        if (!_progress.special1used && validateBewitchablePower(playerID) && _progress.currentCharacter == CHARACTER.ARTIST.order) {
            //first beautify marks special2, second one marks 1
            if (_progress.special1used) {
                if (_progress.special2used) {
                    _progress.special1used = true;
                } else {
                    _progress.special2used = true;
                }
            }
            //pay 1
            getPlayer(playerID).addGold(-1);
            //beautify
            getPlayer(playerID).getCity().get(district).setBeautified(true);

            return GameMessage.coupleArgMessage(ARGC2_MESSAGE.SA_ARTIST, getPlayer(playerID).getName(), getPlayer(playerID).getCity().get(district).getName(), COLOR_TYPE.PLAYER, playerID);
        }
        return null;
    }



    public GameMessage smithyAbility(int playerID) {
        if (canUseDistrictAbilities(playerID)[CitadelsController.DISTICT_SMITHY]) {
            _progress.smithyUsed = true;
            //pay 2 gold
            getPlayer(playerID).addGold(-2);
            //and get 3 cards
            getPlayer(playerID).addHand(_deck.drawCard());
            getPlayer(playerID).addHand(_deck.drawCard());
            getPlayer(playerID).addHand(_deck.drawCard());

            return GameMessage.singleArgMessage(ARGC1_MESSAGE.DA_SMITHY, getPlayer(playerID).getName(), COLOR_TYPE.PLAYER, playerID);
        }
        return null;
    }

    public GameMessage laboratoryAbility(int playerID, int discard) {
        if (canUseDistrictAbilities(playerID)[CitadelsController.DISTICT_LABORATORY]) {
            _progress.laboratoryUsed = true;
            //Discard a card
            District discarded = getPlayer(playerID).getHand().get(discard);
            getPlayer(playerID).removeHand(discarded);
            //put it back to the deck
            _deck.addToTheBottom(discarded);
            //Get a gold
            getPlayer(playerID).addGold(1);

            return GameMessage.singleArgMessage(ARGC1_MESSAGE.DA_LABORATORY, getPlayer(playerID).getName(), COLOR_TYPE.PLAYER, playerID);
        }
        return null;
    }

    public List<GameMessage> graveyardAbility(int playerID, boolean use) {
        //Verify
        if (_progress.graveYardID == playerID) {
            _progress.graveYardID = -1;

            List<GameMessage> res = new LinkedList<GameMessage>();
            //Used
            if (use) {
                //Put the card into the hand
                getPlayer(playerID).addHand(_deck.getGraveyardStop());
                res.add(GameMessage.coupleArgMessage(ARGC2_MESSAGE.DA_GRAVEYARD, getPlayer(playerID).getName(), _deck.getGraveyardStop().getName(), COLOR_TYPE.PLAYER, playerID));
                _deck.setGraveyardStop(null);
            //Not used
            } else {
                //District card put back to the bootm
                _deck.addToTheBottom(_deck.getGraveyardStop());
                _deck.setGraveyardStop(null);
            }
            //End warlords turn
            res.addAll(endTurn(getEnpoweredPlayerIDofChar(CHARACTER.WARLORD.order)));

        }
        return null;
    }

    public List<GameMessage> lighthouseAbility(int playerID, int buildNum, int takeNum) {

        if (canBulildDistrict(playerID, buildNum) == BUILD_QUERY_RESPONSE.CAN_BUILD_LIGHTHOUSE) {
            List<GameMessage> res = new LinkedList<GameMessage>();

            //Build first
            res.add(buildDistrict(playerID, buildNum));

            //Then take the card and shuffle
            District taken = _deck.drawCard(takeNum);
            getPlayer(playerID).addHand(taken);
            _deck.shuffle();
            
            res.add(GameMessage.singleArgMessage(ARGC1_MESSAGE.DA_LIGHTHOUSE, getPlayer(playerID).getName(), COLOR_TYPE.PLAYER, playerID));

            return res;
        }

        return null;
    }


    public GameMessage museumAbility(int playerID, int museumNum, int discard) {
        if (canUseDistrictAbilities(playerID)[CitadelsController.DISTICT_MUSEUM]) {
            _progress.museumUsed = true;
            District museum = getPlayer(playerID).getCity().get(museumNum);
            if (museum.getEffect() != EFFECT.MUSEUM) return null;

            District dcard = getPlayer(playerID).getHand().get(discard);
            museum.addMuseumCards(dcard);
            getPlayer(playerID).getHand().remove(dcard);

            return GameMessage.singleArgMessage(ARGC1_MESSAGE.DA_MUSEUM, getPlayer(playerID).getName(), COLOR_TYPE.PLAYER, playerID);
        }

        return null;
    }

    public GameMessage armoryAbility(int playerID, int targetID, int targetDistrict) {
        if (canUseArmory(playerID, targetID, targetDistrict)) {
            //Destroy target
            District destroyed = getPlayer(targetID).getCity().get(targetDistrict);
            getPlayer(targetID).removeDistrict(destroyed);
            _deck.addToTheBottom(destroyed);
            //cancel belltowers, power if used
            if (destroyed.getEffect() == EFFECT.BELLTOWER) _progress.bellTowerUsed = false;
            //Destroy armory
            District armory = null;
            for (District dist : getPlayer(playerID).getCity()) {
                if (dist.getEffect() == EFFECT.ARMORY) {
                    armory = dist;
                }
            }
            if (armory != null) getPlayer(playerID).removeDistrict(armory);


            return GameMessage.tripleArgMessage(ARGC3_MESSAGE.DA_ARMORY, getPlayer(playerID).getName(), getPlayer(targetID).getName(), destroyed.getName(), COLOR_TYPE.PLAYER, playerID);
        }
        return null;
    }

    public List<GameMessage> belltowerAbility(int playerID, int district) {

        if (canBulildDistrict(playerID, district) == BUILD_QUERY_RESPONSE.CAN_BUILD_BELLTOWER) {
            List<GameMessage> res = new LinkedList<GameMessage>();

            //Build first
            res.add(buildDistrict(playerID, district));

            //Flag for shorter game
            _progress.bellTowerUsed = true;
            res.add(GameMessage.coupleArgMessage(ARGC2_MESSAGE.DA_BELLTOWER, getPlayer(playerID).getName(), Integer.toString(getCityCompleteCondition()), COLOR_TYPE.PLAYER, playerID));

            return res;
        }
        return null;
    }


    private int findNextCharacter() {
            int nextChar = 10;
            //Find next character
            int hospitalChar = -1;
            for (Player player : _players) {
                //later than the current character, and not assassinated
                if (player.getCharacter().order > _progress.currentCharacter &&
                    player.getCharacter().order != _characters.getAssassinated()) {
                    nextChar = Math.min(player.getCharacter().order, nextChar);
                }
                //latr than the current character, assassinated, with a hospital
                if (player.getCharacter().order > _progress.currentCharacter &&
                    player.getCharacter().order == _characters.getAssassinated() &&
                    player.hasDistrictEffect(EFFECT.HOSPITAL)) {
                    nextChar = Math.min(player.getCharacter().order, nextChar);
                    hospitalChar = player.getCharacter().order;
                }
            }

            if (nextChar > 0 && nextChar < 10) {
                _characters.setIsPlayed(nextChar, true);
                //If the assassinated but with hospital is selected, set the flag on
                if (hospitalChar == nextChar) {
                    _progress.hospitalUsed = true;
                }
            }

            return nextChar;
    }

    private boolean validateBewitchablePower(int playerID) {
        //check playerID is legal
        if (playerID < 0) return false;

        //Check if bewitched
        if (_characters.getBeWitched() == _progress.currentCharacter) {
            //check if player is the witch
            if (1 == getPlayer(playerID).getCharacter().order) {
                return true;
            }
        //else use the normal validation
        } else {
            return validateUnbewitchablePower(playerID);
        }

        
        return false;
    }

    private boolean validateUnbewitchablePower(int playerID) {
        //check playerID is legal
        if (playerID < 0) return false;
        //check if current character matches players character
        if (_progress.currentCharacter == getPlayer(playerID).getCharacter().order) {
            return true;
        }

        return false;
    }


    /**
     * Retruns the set of cards to choose from for an action
     *
     * @param playerID
     * @return
     */
    public List<District> getActionCardsChoice(int playerID) {
        List<District> list = new LinkedList<District>();
        //First two cards
        list.add(_deck.peek(0));
        list.add(_deck.peek(1));
        //+1 if you have observatory
        if (getPlayer(playerID).hasDistrictEffect(EFFECT.OBSERVATORY)) list.add(_deck.peek(2));

        return list;
    }

    /**
     * Return how many cards can be kept
     *
     * @param playerID
     * @return
     */
    public int getActionCardsKeepNum(int playerID) {
        if (getPlayer(playerID).hasDistrictEffect(EFFECT.LIBRARY)) return 2;

        return 1;
    }


    public boolean isMyTurn(int playerID) {

        //check if game is in play
        if (!_progress.started || _progress.ended || _progress.roleSelection) return false;

        //Bewitched is a special case
        if (_progress.currentCharacter == _characters.getBeWitched()) {
            //Bewtiched character
            if (_progress.currentCharacter == getPlayer(playerID).getCharacter().order) {
                return !_progress.actionTaken;
            //Witch
            } else if (getPlayer(playerID).getCharacter().character == CHARACTER.WITCH) {
                return _progress.actionTaken;
            } else {
                return false;
            }
        } else {
            return _progress.currentCharacter == getPlayer(playerID).getCharacter().order;
        }
    }

    /**
     * Returns true if no command has been used by this player since the control has moved.
     *
     * @param playerID
     * @return
     */
    public boolean isFirstTurn(int playerID) {
        if (!isMyTurn(playerID)) return false;

        boolean[] SAs = canUseSpecialAbilities(playerID);
        boolean[] DAs = canUseDistrictAbilities(playerID);

        //Witch resuming from bewitch is a special case
         if (_progress.currentCharacter == _characters.getBeWitched() &&
             getPlayer(playerID).getCharacter().character == CHARACTER.WITCH) {
             //Some characters use SA at the beginning of resume (end of action)
             boolean SAalreadyUsed = false;
             CHARACTER character= getPlayer(playerID).getCharacter().character;
             switch (character) {
                 case EMPEROR:
                 case MERCHANT:
                 case ARCHITECT:
                 case QUEEN:
                     SAalreadyUsed = true;
                     System.out.println(_progress.districtBuilt + " == 0 && " + SAs[3] + " && " + DAs[0]+" && "+DAs[1]+" && "+DAs[2]);
                     break;
                 default:
                     break;
             }
             if (SAalreadyUsed) {
                 return (_progress.districtBuilt == 0 && SAs[3] && DAs[0] && DAs[1] && DAs[2]);
             } else {
                 return (_progress.districtBuilt == 0 && SAs[0] && SAs[1] && SAs[2] && SAs[3] && DAs[0] && DAs[1] && DAs[2]);
             }
         }

        boolean SAblocked = false;
        //No special abilities if you are being bewitched
        if (getPlayer(playerID).getCharacter().order == _characters.getBeWitched()) SAblocked = true;
        //No special abilities if hospital is used
        if (_progress.hospitalUsed) SAblocked= true;
        //Witch, Warlord, Diplomat has SABlocked until action is used
        if (getPlayer(playerID).getCharacter().character == CHARACTER.WITCH ||
                getPlayer(playerID).getCharacter().character == CHARACTER.WARLORD ||
                getPlayer(playerID).getCharacter().character == CHARACTER.DIPLOMAT) {
            SAblocked = true;
        }

        //Other cases
        //Special ability blocked by Witch or Assassin+Hospital
        if (SAblocked) {
            return !_progress.actionTaken;
        //Normal case
        } else {
             return (!_progress.actionTaken && _progress.districtBuilt == 0 && SAs[0] && SAs[1] && SAs[2] && SAs[3] && DAs[0] && DAs[1] && DAs[2]);
        }
    }

    public boolean[] canUseSpecialAbilities(int playerID) {
        boolean canUse = isMyTurn(playerID);

        //No special abilities if you are being bewitched
        if (getPlayer(playerID).getCharacter().order == _characters.getBeWitched()) canUse = false;
        //No special abilities if hospital is used
        if (_progress.hospitalUsed) canUse = false;

        //Special restrictions
        switch (_characters.getCharacter(_progress.currentCharacter).character) {
     //can be only used after action, before building
            case NAVIGATOR:
                if (_progress.districtBuilt > 0) {
                    canUse= false;
                }
            //can be only used after action
            case WARLORD:
            case DIPLOMAT:
                if (!_progress.actionTaken) {
                    canUse = false;
                }
                break;
            case WITCH:
                //Must be after action, before building
                if (!_progress.actionTaken) canUse = false;
                if (_progress.districtBuilt > 0)  canUse = false;
                break;
        }

        boolean[] SAs = new boolean[]{canUse & !_progress.special1used, canUse & !_progress.special2used, canUse & !_progress.special3used, canUse & !_progress.districtIncomeGained};
        return SAs;
    }

    public boolean[] canUseDistrictAbilities(int playerID) {
        boolean canUse = isMyTurn(playerID);
        //No special abilities if you are being bewitched
        if (getPlayer(playerID).getCharacter().order == _characters.getBeWitched()) canUse = false;
        //No special abilities if hospital is used
        if (_progress.hospitalUsed) canUse = false;

        boolean[] DAs = new boolean[3];
        
        
        //Smithy needs at least 2 gold
        DAs[CitadelsController.DISTICT_SMITHY] = !_progress.smithyUsed && getPlayer(playerID).getGold() > 1 && canUse;
        //laboratry needs at least one card
        DAs[CitadelsController.DISTICT_LABORATORY] = !_progress.laboratoryUsed && getPlayer(playerID).getHand().size() > 0 && canUse;
        //museum needs at least one card
        DAs[CitadelsController.DISTICT_MUSEUM] = !_progress.museumUsed && getPlayer(playerID).getHand().size() > 0 && canUse;

        return DAs;
    }

    
    public boolean isThisUsableMuseum(int playerID, int district) {
        boolean abilityUsable = canUseDistrictAbilities(playerID)[CitadelsController.DISTICT_MUSEUM];
        //Check if Museum is usable
        if (!abilityUsable) return false;

        //Check if this district is the museum
        return getPlayer(playerID).getCity().get(district).getEffect() == EFFECT.MUSEUM;
    }

    public boolean canUseArmory(int playerID, int targetPlayerID, int targetDistrict) {
        boolean canUse = isMyTurn(playerID);

        //No special abilities if you are being bewitched
        if (getPlayer(playerID).getCharacter().order == _characters.getBeWitched()) return false;
        //No special abilities if hospital is used
        if (_progress.hospitalUsed) return false;

        //Can't destroy your own city, and has to have Armory
        canUse &= (playerID != targetPlayerID) && getPlayer(playerID).hasDistrictEffect(EFFECT.ARMORY);
        //The city cannot be completed
        canUse &= getPlayer(targetPlayerID).getCity().size() <  getCityCompleteCondition();

        return canUse;
    }

    public BUILD_QUERY_RESPONSE canBuild(int playerID) {

        //Check if its the correct turn
        if (getEnpoweredPlayerIDofChar(_progress.currentCharacter) != playerID) return BUILD_QUERY_RESPONSE.NOT_YOUR_TURN;

        //Check if action has taken
        if (!_progress.actionTaken) {
           return BUILD_QUERY_RESPONSE.ACTION_NOT_TAKEN;
        }

        CHARACTER playedChar = _characters.getCharacter(_progress.currentCharacter).character;

        //Building Limit
        //Wizard has special building limit
        if (playedChar == CHARACTER.WIZARD) {
            //If at least 1 district has been built, and the ability is not used
            if (_progress.districtBuilt > 0 && !_progress.special2used)  {
                return BUILD_QUERY_RESPONSE.BUILDING_LIMIT;
            }
            //If at least 2 district is build
            if (_progress.districtBuilt > 1) {
                return BUILD_QUERY_RESPONSE.BUILDING_LIMIT;
            }
        //Architect has special building limit
        } else if (playedChar  == CHARACTER.ARCHITECT) {
            //If at least 3 district is build
            if (_progress.districtBuilt > 2) {
                return BUILD_QUERY_RESPONSE.BUILDING_LIMIT;
            }
        //Navigator cannot build any districts if special ability has been used
        } else if (playedChar == CHARACTER.NAVIGATOR && (_progress.special1used || _progress.special2used)) {
                return BUILD_QUERY_RESPONSE.BUILDING_LIMIT;
        //Witch cannot build any districts if special ability has been used
        } else if (playedChar == CHARACTER.WITCH && _progress.special1used) {
                return BUILD_QUERY_RESPONSE.BUILDING_LIMIT;
        //Others have normal building limit
        } else{
            //If at least 1 district is build
            if (_progress.districtBuilt > 0) {
                return BUILD_QUERY_RESPONSE.BUILDING_LIMIT;
            }
        }

        return BUILD_QUERY_RESPONSE.CAN_BUILD;
    }

    /**
     * Checks if a district can be built by the player.
     *
     * @param playerID
     * @param districtNum Index of the target district in player's hand
     * @return
     */
    public BUILD_QUERY_RESPONSE canBulildDistrict(int playerID, int districtNum) {

        BUILD_QUERY_RESPONSE baseCheck = canBuild(playerID);
        if (baseCheck != BUILD_QUERY_RESPONSE.CAN_BUILD) {
            return baseCheck;
        }

        District targetDistrict = getPlayer(playerID).getHand().get(districtNum);

        //Duplicate building
        if (isDuplicateRuleViolated(getPlayer(playerID), targetDistrict)) {
                return BUILD_QUERY_RESPONSE.DUPLICATE_DISTRICT;
        }

        //Cost check
        //Factory
        if (getPlayer(playerID).hasDistrictEffect(EFFECT.FACTORY) && targetDistrict.getEffect() != EFFECT.NONE) {
            if (getPlayer(playerID).getGold() < targetDistrict.getCost() - 1) {
                return BUILD_QUERY_RESPONSE.INSUFFICIENT_GOLD;
            }
        } else {
            //Normal
            if (getPlayer(playerID).getGold() < targetDistrict.getCost()) {
                return BUILD_QUERY_RESPONSE.INSUFFICIENT_GOLD;
            }
        }

        if (targetDistrict.getEffect() == EFFECT.LIGHTHOUSE) {
            return BUILD_QUERY_RESPONSE.CAN_BUILD_LIGHTHOUSE;
        }

        if (targetDistrict.getEffect() == EFFECT.BELLTOWER) {
            return BUILD_QUERY_RESPONSE.CAN_BUILD_BELLTOWER;
        }

        return BUILD_QUERY_RESPONSE.CAN_BUILD;
    }


    private boolean isDuplicateRuleViolated(Player player, District district) {

        CHARACTER playedChar = _characters.getCharacter(_progress.currentCharacter).character;

        //Duplicate building
        //Wizard do not get affected
        if (playedChar != CHARACTER.WIZARD) {
            //check if it will be an duplicate when added
            boolean isDuplicate = false;
            for (District cd : player.getCity()) {
                if (cd.getName().compareTo(district.getName()) == 0) {
                    isDuplicate = true;
                    break;
                }
            }

            //If there is a quarry, one duplicate is allowed
            if (player.hasDistrictEffect(EFFECT.QUARRY)) {
                int duplicateCount = 0;
                for (int i = 0; i < player.getCity().size(); i++) {
                    for (int j = i+1; j < player.getCity().size(); j++) {
                        if (player.getCity().get(i).getName().compareTo(player.getCity().get(j).getName()) == 0) {
                            duplicateCount++;
                            break;
                        }
                    }
                }
                if (duplicateCount > 0) {
                    return true;
                } else {
                    return false;
                }
            }

            return isDuplicate;
        }
        return false;
    }

    /**
     * Returns players capability of influencing districts
     *
     * @param playerID
     * @return
     */
    public DISTRICT_INFLUENCE_RESPONSE canInfluenceDistrict(int playerID) {
        Character current = getCurrentCharacter();

        //Nothing can be done if in hospital
        if (_progress.hospitalUsed) return DISTRICT_INFLUENCE_RESPONSE.NONE;
        
        //Characters that can influence districts
        if (current.character == CHARACTER.WARLORD && _progress.actionTaken || current.character == CHARACTER.DIPLOMAT && _progress.actionTaken || current.character == CHARACTER.ARTIST ) {
            //If the current character is bewitched
            if (_characters.getBeWitched() == current.order) {
                //If player is the witch
                if (getPlayer(playerID).getCharacter().character == CHARACTER.WITCH) {
                    switch (current.character) {
                        case WARLORD:
                            return DISTRICT_INFLUENCE_RESPONSE.DESTROY;
                        case DIPLOMAT:
                            return DISTRICT_INFLUENCE_RESPONSE.EXCHANGE;
                        case ARTIST:
                            return DISTRICT_INFLUENCE_RESPONSE.BEAUTIFY;
                    }
                }
            //normal case
            } else {
                switch (getPlayer(playerID).getCharacter().character) {
                    case WARLORD:
                        return DISTRICT_INFLUENCE_RESPONSE.DESTROY;
                    case DIPLOMAT:
                        return DISTRICT_INFLUENCE_RESPONSE.EXCHANGE;
                    case ARTIST:
                        return DISTRICT_INFLUENCE_RESPONSE.BEAUTIFY;
                }
            }
        }

        return DISTRICT_INFLUENCE_RESPONSE.NONE;
    }

    public DISTRICT_INFLUENCE_RESPONSE canInfluenceDistrict(int playerID, int targetPlayerID, int targetDistrict) {
        DISTRICT_INFLUENCE_RESPONSE baseRes = canInfluenceDistrict(playerID);

        //If nothing can be done, no need to look at the district
        if (baseRes == DISTRICT_INFLUENCE_RESPONSE.NONE) return baseRes;

        //Warlord and Diplomat have similar constraints
        if (baseRes == DISTRICT_INFLUENCE_RESPONSE.DESTROY || baseRes == DISTRICT_INFLUENCE_RESPONSE.EXCHANGE) {
            //Completed city cannot be destroyed or exchanged
            if (getPlayer(targetPlayerID).getCity().size() >=  getCityCompleteCondition()) return DISTRICT_INFLUENCE_RESPONSE.CITY_COMPLETED;
            //If target is Bishop, you can't destory or exchange
            if (_characters.isPlayed(CHARACTER.BISHOP.order) && _characters.getCharacter(CHARACTER.BISHOP.order).character == CHARACTER.BISHOP) {
                if (this.getEnpoweredPlayerIDofChar(CHARACTER.BISHOP.order) == targetPlayerID) {
                    return DISTRICT_INFLUENCE_RESPONSE.PROTECTED;
                }
            }

            //Keep cannot be destroyed or exchanged
            if (playerID != targetPlayerID && getPlayer(targetPlayerID).getCity().get(targetDistrict).getEffect() == EFFECT.KEEP) {
                return DISTRICT_INFLUENCE_RESPONSE.KEEP;
            }

            //Warlord specific constaints
            if (baseRes == DISTRICT_INFLUENCE_RESPONSE.DESTROY) {
                //You can't destroy your own
                if (playerID == targetPlayerID) return DISTRICT_INFLUENCE_RESPONSE.NONE;
                //Calculate the cost
                int cost = computeCostToDestroy(targetPlayerID, targetDistrict);
                if (cost > getPlayer(playerID).getGold()) return DISTRICT_INFLUENCE_RESPONSE.NO_GOLD_TO_DESTROY;

                return DISTRICT_INFLUENCE_RESPONSE.DESTROY;

            //Diplomat specific constraints
            } else if (baseRes == DISTRICT_INFLUENCE_RESPONSE.EXCHANGE) {
                //Exchange can be done if you have at least one district
                if (getPlayer(playerID).getCity().size() == 0) {
                    return DISTRICT_INFLUENCE_RESPONSE.EXCHANGE_NO_OWN_DISTRICT;
                }
                //Your own district
                if (playerID == targetPlayerID) return DISTRICT_INFLUENCE_RESPONSE.EXCHANGE_CANDIDATE_SELF;

                //You can't have duplicate districts
                if (isDuplicateRuleViolated(getPlayer(playerID), getPlayer(targetPlayerID).getCity().get(targetDistrict))) {
                        return DISTRICT_INFLUENCE_RESPONSE.EXCHANGE_DUPLICATE;
                }

                //Else other
                return DISTRICT_INFLUENCE_RESPONSE.EXCHANGE_CANDIDATE_OTHER;
            }
            
            //
        //Artist has his own constraint
        } else if (baseRes == DISTRICT_INFLUENCE_RESPONSE.BEAUTIFY) {
            //Not your district
            if (playerID != targetPlayerID) return DISTRICT_INFLUENCE_RESPONSE.NONE;

            //Already beautified
            if (getPlayer(playerID).getCity().get(targetDistrict).getBeautified()) return DISTRICT_INFLUENCE_RESPONSE.ALREADY_BEAUTIFIED;

            //If beautified twice already
            if (_progress.special1used && _progress.special2used) return DISTRICT_INFLUENCE_RESPONSE.BEAUTIFY_LIMIT;
        }

        

        return canInfluenceDistrict(playerID);
    }

    public int computeCostToDestroy(int playerID, int districtID) {
        //You need cost - 1 to destroy
        int cost = getPlayer(playerID).getCity().get(districtID).getCost() - 1;
        //Beautify adds 1 cost
        if (getPlayer(playerID).getCity().get(districtID).getBeautified()) cost++;
        //Great wall adds 1 cost
        if (getPlayer(playerID).hasDistrictEffect(EFFECT.GREATWALL)) cost++;
        
        return cost;
    }

    public int computeCostToExchange(int playerID, int playerDistrict, int targetID, int targetDistrict) {
        int cost = computeCostToDestroy(targetID, targetDistrict) - computeCostToDestroy(playerID, playerDistrict);

        //Great Wall only counts for cost to take
        if (getPlayer(playerID).hasDistrictEffect(EFFECT.GREATWALL)) cost++;

        return Math.max(cost, 0);
    }

    public int getCityCompleteCondition() {
        int con = 8;

        if (_options.shortGame) con--;
        if (_progress.bellTowerUsed) con--;

        return con;
    }

    public Character getCurrentCharacter() {
        try {
            return _characters.getCharacter(_progress.currentCharacter);
        } catch (Exception e) {
            return null;
        }
    }
}
