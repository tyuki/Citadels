/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package citadels.game;

import java.io.Serializable;

/**
 *
 * @author Personal
 */
public class Progress implements Serializable {

    public boolean started;
    public boolean ended;
    public int round;
    public int currentCharacter;
    public int currentSelectionIndex;
    public int currentSelectionID;
    public int crownID;
    public boolean roleSelection;
    public boolean cityCompleted;
    public boolean bellTowerUsed;

    public boolean useEmperorPower;
    public int crownedByEmperorID;
    public boolean didFirstCharacterBuild;
    public int graveYardID;

    public boolean actionTaken;
    public boolean districtIncomeGained;
    public boolean special1used;
    public boolean special2used;
    public boolean special3used;
    public boolean smithyUsed;
    public boolean laboratoryUsed;
    public boolean museumUsed;
    public boolean hospitalUsed;
    public int districtBuilt;
    public int goldSpentOnDistrict;

    public Progress() {
        started = false;
        ended = false;
        round = 0;
        currentCharacter = 0;
        currentSelectionIndex = 0;
        currentSelectionID = 0;
        crownID = 0;
        roleSelection = false;
        cityCompleted = false;
        bellTowerUsed = false;

        useEmperorPower = false;
        crownedByEmperorID = -1;
        didFirstCharacterBuild = false;

        clearPlayerInfo();
    }

    public void clearPlayerInfo() {
        actionTaken = false;
        districtIncomeGained = false;
        special1used = false;
        special2used = false;
        special3used = false;
        smithyUsed = false;
        laboratoryUsed = false;
        museumUsed = false;
        hospitalUsed = false;
        districtBuilt = 0;
        goldSpentOnDistrict = 0;
    }

    public void clearRoundInfo() {
        //mark as selection phase
        roleSelection = true;
        //no character in play yet
        currentCharacter = 0;
        //flag for tax collector
        didFirstCharacterBuild = false;
        //flag for emperor power
        useEmperorPower = false;
        crownedByEmperorID = -1;
        //graveyard
        graveYardID = -1;
    }
}
