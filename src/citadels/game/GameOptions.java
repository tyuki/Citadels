/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package citadels.game;

import citadels.game.District.EFFECT;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 *
 * @author Personal
 */
public class GameOptions implements Serializable {

    public final boolean use9th;
    public final boolean shortGame;
    public final int[] expansionCards;
    public final EFFECT[] expansionDistricts;
    public final int randomDistrictsNum;

    public GameOptions(boolean use9th, boolean shortGame, int[] expansionCards, EFFECT[] expansionDistricts) {
        this.use9th = use9th;
        this.shortGame = shortGame;
        this.expansionCards = expansionCards;
        this.expansionDistricts = expansionDistricts;
        randomDistrictsNum = 0;
    }

    public GameOptions(boolean use9th, boolean shortGame, int[] expansionCards, int randomDistricts) {
        this.use9th = use9th;
        this.shortGame = shortGame;
        this.expansionCards = expansionCards;
        this.expansionDistricts = new EFFECT[]{};
        randomDistrictsNum = randomDistricts;
    }

    public static int[] selectExpansionCharactersRandomly(boolean use9th, int num) {
        int[] expansionCards = new int[2];

        //Use shuffling as randomness
        List<Integer> candidates = new ArrayList<Integer>();

        for (int i = 1; i <= 8; i++) {
            candidates.add(i);
        }
        if (use9th) {
            candidates.add(9);
        }

        Collections.shuffle(candidates);

        for (int i = 0; i < num; i++) {
            expansionCards[i] = candidates.get(i);
        }

        return expansionCards;
    }
}
