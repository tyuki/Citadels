/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package citadels.game;

import citadels.CitadelsApp;
import java.util.Locale;

/**
 *
 * @author Personal
 */
public class ServerMessage {

     public static enum COLOR_TYPE {
        DEFAULT,
        ERROR,
        EMPHASIZE
    }

    public static enum ARGC1_MESSAGE {
        CONNECTED,
        DISCONNECTED,
        JOINED,
        LEAVE,
        RESET,
        ERROR
    }


    private static org.jdesktop.application.ResourceMap _resourceMap;

    static {
        _resourceMap = org.jdesktop.application.Application.getInstance(citadels.CitadelsApp.class).getContext().getResourceMap(CitadelsApp.class);
    }

    public final String string;
    public final COLOR_TYPE colorType;

    public ServerMessage(String string, COLOR_TYPE colorType) {
        this.string = string;
        this.colorType = colorType;
    }

    public static ServerMessage singleArgMessage(ARGC1_MESSAGE msgType, String arg1, COLOR_TYPE type) {

        Locale local = Locale.getDefault();

        if (local == Locale.JAPAN || local == Locale.JAPANESE) {
            return new ServerMessage(singleArgMessageJP(msgType, arg1), type);
        } else {
            return new ServerMessage(singleArgMessageEN(msgType, arg1), type);
        }
    }

    private static String singleArgMessageEN(ARGC1_MESSAGE msgType, String arg1) {
        switch (msgType) {
            case CONNECTED:
                return arg1 + " has connected";
            case DISCONNECTED:
                return arg1 + " has diconnected";
            case JOINED:
                return arg1 + " has joined the game";
            case LEAVE:
                return arg1 + " has left the game";
            case RESET:
                return "Game Reset";
            case ERROR:
                return arg1;
        }

        return "";
    }


    private static String singleArgMessageJP(ARGC1_MESSAGE msgType, String arg1) {
        switch (msgType) {
            case CONNECTED:
                return arg1 + "が接続しました";
            case DISCONNECTED:
                return arg1 + "との接続が切断されました";
            case JOINED:
                return arg1 + "がゲームに参加しました";
            case LEAVE:
                return arg1 + "がゲームから切断しました";
            case RESET:
                return "ゲームがリセットされました";
            case ERROR:
                return arg1;
        }
        return singleArgMessageEN(msgType, arg1);
    }
}
