/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package citadels.game;

import citadels.game.Character.CHARACTER;
import citadels.game.District.DISTRICT_COLOR;
import citadels.game.District.EFFECT;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.LinkedList;

/**
 *
 */
public class Deck implements Serializable {
    private List<District> _deck;
    private District _graveyardStop;

    public Deck(int[] expansionCards, EFFECT[] expansionDistricts) {
        initialize(expansionCards, expansionDistricts);
    }

    public Deck(int[] expansionCards, int randomCardNum) {


        List<EFFECT> exDistEffects = new ArrayList<EFFECT>() {
            {
                add(EFFECT.POORHOUSE);
                add(EFFECT.IMPERIALTREASURY);
                add(EFFECT.PARK);
                add(EFFECT.FACTORY);
                add(EFFECT.WISHINGWELL);
                add(EFFECT.MAPROOM);
                add(EFFECT.THRONEROOM);
                add(EFFECT.LIGHTHOUSE);
                add(EFFECT.BELLTOWER);
                add(EFFECT.MUSEUM);
                add(EFFECT.ARMORY);
                add(EFFECT.QUARRY);
            }
        };

        //contain assassin
        boolean hasAssassin = true;
        for (int exp : expansionCards) {
            if (exp == CHARACTER.ASSASSIN.order) hasAssassin = false;
        }

        //When there is assassin, add hospital
        if (hasAssassin) {
            exDistEffects.add(EFFECT.HOSPITAL);
        }

        //Shuffle
        Collections.shuffle(exDistEffects);

        EFFECT[] cards = new EFFECT[randomCardNum];

        for (int i = 0; i < Math.min(cards.length, exDistEffects.size()); i++) {
            cards[i] = exDistEffects.get(i);
        }

        initialize(expansionCards, cards);
    }

    /**
     * Initializes the set of cards used in Citadels
     * The cards are shuffled in the end
     */
    public void initialize(int[] expansionCards, EFFECT[] expansionDistricts) {
        _deck = new LinkedList<District>();

        //Yellow
        _deck.add(new District("manor", DISTRICT_COLOR.YELLOW, DISTRICT_COLOR.YELLOW, 3, 3, EFFECT.NONE));
        _deck.add(new District("manor", DISTRICT_COLOR.YELLOW, DISTRICT_COLOR.YELLOW, 3, 3, EFFECT.NONE));
        _deck.add(new District("manor", DISTRICT_COLOR.YELLOW, DISTRICT_COLOR.YELLOW, 3, 3, EFFECT.NONE));
        _deck.add(new District("manor", DISTRICT_COLOR.YELLOW, DISTRICT_COLOR.YELLOW, 3, 3, EFFECT.NONE));
        _deck.add(new District("manor", DISTRICT_COLOR.YELLOW, DISTRICT_COLOR.YELLOW, 3, 3, EFFECT.NONE));
        _deck.add(new District("castle", DISTRICT_COLOR.YELLOW, DISTRICT_COLOR.YELLOW, 4, 4, EFFECT.NONE));
        _deck.add(new District("castle", DISTRICT_COLOR.YELLOW, DISTRICT_COLOR.YELLOW, 4, 4, EFFECT.NONE));
        _deck.add(new District("castle", DISTRICT_COLOR.YELLOW, DISTRICT_COLOR.YELLOW, 4, 4, EFFECT.NONE));
        _deck.add(new District("castle", DISTRICT_COLOR.YELLOW, DISTRICT_COLOR.YELLOW, 4, 4, EFFECT.NONE));
        _deck.add(new District("palace", DISTRICT_COLOR.YELLOW, DISTRICT_COLOR.YELLOW, 5, 5, EFFECT.NONE));
        _deck.add(new District("palace", DISTRICT_COLOR.YELLOW, DISTRICT_COLOR.YELLOW, 5, 5, EFFECT.NONE));
        _deck.add(new District("palace", DISTRICT_COLOR.YELLOW, DISTRICT_COLOR.YELLOW, 5, 5, EFFECT.NONE));
        
        //Blue
        _deck.add(new District("temple", DISTRICT_COLOR.BLUE, DISTRICT_COLOR.BLUE, 1, 1, EFFECT.NONE));
        _deck.add(new District("temple", DISTRICT_COLOR.BLUE, DISTRICT_COLOR.BLUE, 1, 1, EFFECT.NONE));
        _deck.add(new District("temple", DISTRICT_COLOR.BLUE, DISTRICT_COLOR.BLUE, 1, 1, EFFECT.NONE));
        _deck.add(new District("church", DISTRICT_COLOR.BLUE, DISTRICT_COLOR.BLUE, 2, 2, EFFECT.NONE));
        _deck.add(new District("church", DISTRICT_COLOR.BLUE, DISTRICT_COLOR.BLUE, 2, 2, EFFECT.NONE));
        _deck.add(new District("church", DISTRICT_COLOR.BLUE, DISTRICT_COLOR.BLUE, 2, 2, EFFECT.NONE));
        _deck.add(new District("monastery", DISTRICT_COLOR.BLUE, DISTRICT_COLOR.BLUE, 3, 3, EFFECT.NONE));
        _deck.add(new District("monastery", DISTRICT_COLOR.BLUE, DISTRICT_COLOR.BLUE, 3, 3, EFFECT.NONE));
        _deck.add(new District("monastery", DISTRICT_COLOR.BLUE, DISTRICT_COLOR.BLUE, 3, 3, EFFECT.NONE));
        _deck.add(new District("cathedral", DISTRICT_COLOR.BLUE, DISTRICT_COLOR.BLUE, 5, 5, EFFECT.NONE));
        _deck.add(new District("cathedral", DISTRICT_COLOR.BLUE, DISTRICT_COLOR.BLUE, 5, 5, EFFECT.NONE));

        //Green
        _deck.add(new District("tavern", DISTRICT_COLOR.GREEN, DISTRICT_COLOR.GREEN, 1, 1, EFFECT.NONE));
        _deck.add(new District("tavern", DISTRICT_COLOR.GREEN, DISTRICT_COLOR.GREEN, 1, 1, EFFECT.NONE));
        _deck.add(new District("tavern", DISTRICT_COLOR.GREEN, DISTRICT_COLOR.GREEN, 1, 1, EFFECT.NONE));
        _deck.add(new District("tavern", DISTRICT_COLOR.GREEN, DISTRICT_COLOR.GREEN, 1, 1, EFFECT.NONE));
        _deck.add(new District("tavern", DISTRICT_COLOR.GREEN, DISTRICT_COLOR.GREEN, 1, 1, EFFECT.NONE));
        _deck.add(new District("tradingpost", DISTRICT_COLOR.GREEN, DISTRICT_COLOR.GREEN, 2, 2, EFFECT.NONE));
        _deck.add(new District("tradingpost", DISTRICT_COLOR.GREEN, DISTRICT_COLOR.GREEN, 2, 2, EFFECT.NONE));
        _deck.add(new District("tradingpost", DISTRICT_COLOR.GREEN, DISTRICT_COLOR.GREEN, 2, 2, EFFECT.NONE));
        _deck.add(new District("market", DISTRICT_COLOR.GREEN, DISTRICT_COLOR.GREEN, 2, 2, EFFECT.NONE));
        _deck.add(new District("market", DISTRICT_COLOR.GREEN, DISTRICT_COLOR.GREEN, 2, 2, EFFECT.NONE));
        _deck.add(new District("market", DISTRICT_COLOR.GREEN, DISTRICT_COLOR.GREEN, 2, 2, EFFECT.NONE));
        _deck.add(new District("market", DISTRICT_COLOR.GREEN, DISTRICT_COLOR.GREEN, 2, 2, EFFECT.NONE));
        _deck.add(new District("docks", DISTRICT_COLOR.GREEN, DISTRICT_COLOR.GREEN, 3, 3, EFFECT.NONE));
        _deck.add(new District("docks", DISTRICT_COLOR.GREEN, DISTRICT_COLOR.GREEN, 3, 3, EFFECT.NONE));
        _deck.add(new District("docks", DISTRICT_COLOR.GREEN, DISTRICT_COLOR.GREEN, 3, 3, EFFECT.NONE));
        _deck.add(new District("harbor", DISTRICT_COLOR.GREEN, DISTRICT_COLOR.GREEN, 4, 4, EFFECT.NONE));
        _deck.add(new District("harbor", DISTRICT_COLOR.GREEN, DISTRICT_COLOR.GREEN, 4, 4, EFFECT.NONE));
        _deck.add(new District("harbor", DISTRICT_COLOR.GREEN, DISTRICT_COLOR.GREEN, 4, 4, EFFECT.NONE));
        _deck.add(new District("townhall", DISTRICT_COLOR.GREEN, DISTRICT_COLOR.GREEN, 5, 5, EFFECT.NONE));
        _deck.add(new District("townhall", DISTRICT_COLOR.GREEN, DISTRICT_COLOR.GREEN, 5, 5, EFFECT.NONE));

        //Red
        _deck.add(new District("watchtower", DISTRICT_COLOR.RED, DISTRICT_COLOR.RED, 1, 1, EFFECT.NONE));
        _deck.add(new District("watchtower", DISTRICT_COLOR.RED, DISTRICT_COLOR.RED, 1, 1, EFFECT.NONE));
        _deck.add(new District("watchtower", DISTRICT_COLOR.RED, DISTRICT_COLOR.RED, 1, 1, EFFECT.NONE));
        _deck.add(new District("prison", DISTRICT_COLOR.RED, DISTRICT_COLOR.RED, 2, 2, EFFECT.NONE));
        _deck.add(new District("prison", DISTRICT_COLOR.RED, DISTRICT_COLOR.RED, 2, 2, EFFECT.NONE));
        _deck.add(new District("prison", DISTRICT_COLOR.RED, DISTRICT_COLOR.RED, 2, 2, EFFECT.NONE));
        _deck.add(new District("battlefield", DISTRICT_COLOR.RED, DISTRICT_COLOR.RED, 3, 3, EFFECT.NONE));
        _deck.add(new District("battlefield", DISTRICT_COLOR.RED, DISTRICT_COLOR.RED, 3, 3, EFFECT.NONE));
        _deck.add(new District("battlefield", DISTRICT_COLOR.RED, DISTRICT_COLOR.RED, 3, 3, EFFECT.NONE));
        _deck.add(new District("fortress", DISTRICT_COLOR.RED, DISTRICT_COLOR.RED, 5, 5, EFFECT.NONE));
        _deck.add(new District("fortress", DISTRICT_COLOR.RED, DISTRICT_COLOR.RED, 5, 5, EFFECT.NONE));

        //Purple
        _deck.add(new District("keep", DISTRICT_COLOR.PURPLE, DISTRICT_COLOR.PURPLE, 3, 3, EFFECT.KEEP));
        _deck.add(new District("keep", DISTRICT_COLOR.PURPLE, DISTRICT_COLOR.PURPLE, 3, 3, EFFECT.KEEP));
        _deck.add(new District("observatory", DISTRICT_COLOR.PURPLE, DISTRICT_COLOR.PURPLE, 5, 5, EFFECT.OBSERVATORY));
        _deck.add(new District("smithy", DISTRICT_COLOR.PURPLE, DISTRICT_COLOR.PURPLE, 5, 5, EFFECT.SMITHY));
        _deck.add(new District("laboratory", DISTRICT_COLOR.PURPLE, DISTRICT_COLOR.PURPLE, 5, 5, EFFECT.LABORATORY));
        _deck.add(new District("library", DISTRICT_COLOR.PURPLE, DISTRICT_COLOR.PURPLE, 6, 6, EFFECT.LIBRARY));
        _deck.add(new District("greatwall", DISTRICT_COLOR.PURPLE, DISTRICT_COLOR.PURPLE, 6, 6, EFFECT.GREATWALL));
        _deck.add(new District("hauntedcity", DISTRICT_COLOR.WILD, DISTRICT_COLOR.PURPLE, 2, 2, EFFECT.HAUNTEDCITY));
        _deck.add(new District("schoolofmagic", DISTRICT_COLOR.PURPLE, DISTRICT_COLOR.WILD, 6, 6, EFFECT.SCHOOLOFMAGIC));
        _deck.add(new District("university", DISTRICT_COLOR.PURPLE, DISTRICT_COLOR.PURPLE, 6, 8, EFFECT.UNIVERSITY));
        _deck.add(new District("dragongate", DISTRICT_COLOR.PURPLE, DISTRICT_COLOR.PURPLE, 6, 8, EFFECT.DRAGONGATE));

        //No graveyard if Diplomat is used
        boolean hasDiplomat = false;
        for (int exp : expansionCards) {
            if (exp == CHARACTER.DIPLOMAT.order) hasDiplomat = true;
        }
        if (!hasDiplomat) {
            _deck.add(new District("graveyard", DISTRICT_COLOR.PURPLE, DISTRICT_COLOR.PURPLE, 5, 5, EFFECT.GRAVEYARD));
        }

        //expansion districts
        for (EFFECT card : expansionDistricts) {
            switch (card) {
                case POORHOUSE:
                    _deck.add(new District("poorhouse", DISTRICT_COLOR.PURPLE, DISTRICT_COLOR.PURPLE, 5, 5, EFFECT.POORHOUSE));
                    break;
                case IMPERIALTREASURY:
                    _deck.add(new District("imperialtreasury", DISTRICT_COLOR.PURPLE, DISTRICT_COLOR.PURPLE, 4, 4, EFFECT.IMPERIALTREASURY));
                    break;
                case PARK:
                    _deck.add(new District("park", DISTRICT_COLOR.PURPLE, DISTRICT_COLOR.PURPLE, 6, 6, EFFECT.PARK));
                    break;
                case FACTORY:
                    _deck.add(new District("factory", DISTRICT_COLOR.PURPLE, DISTRICT_COLOR.PURPLE, 6, 6, EFFECT.FACTORY));
                    break;
                case WISHINGWELL:
                    _deck.add(new District("wishingwell", DISTRICT_COLOR.PURPLE, DISTRICT_COLOR.PURPLE, 5, 5, EFFECT.WISHINGWELL));
                    break;
                case MAPROOM:
                    _deck.add(new District("maproom", DISTRICT_COLOR.PURPLE, DISTRICT_COLOR.PURPLE, 5, 5, EFFECT.MAPROOM));
                    break;
                case THRONEROOM:
                    _deck.add(new District("throneroom", DISTRICT_COLOR.PURPLE, DISTRICT_COLOR.PURPLE, 6, 6, EFFECT.THRONEROOM));
                    break;
                case HOSPITAL:
                    _deck.add(new District("hospital", DISTRICT_COLOR.PURPLE, DISTRICT_COLOR.PURPLE, 6, 6, EFFECT.HOSPITAL));
                    break;
                case ARMORY:
                    _deck.add(new District("armory", DISTRICT_COLOR.PURPLE, DISTRICT_COLOR.PURPLE, 3, 3, EFFECT.ARMORY));
                    break;
                case MUSEUM:
                    _deck.add(new District("museum", DISTRICT_COLOR.PURPLE, DISTRICT_COLOR.PURPLE, 4, 4, EFFECT.MUSEUM));
                    break;
                case LIGHTHOUSE:
                    _deck.add(new District("lighthouse", DISTRICT_COLOR.PURPLE, DISTRICT_COLOR.PURPLE, 3, 3, EFFECT.LIGHTHOUSE));
                    break;
                case QUARRY:
                    _deck.add(new District("quarry", DISTRICT_COLOR.PURPLE, DISTRICT_COLOR.PURPLE, 5, 5, EFFECT.QUARRY));
                    break;
                case BELLTOWER:
                    _deck.add(new District("belltower", DISTRICT_COLOR.PURPLE, DISTRICT_COLOR.PURPLE, 5, 5, EFFECT.BELLTOWER));
                    break;
            }
        }

        //Shuffle
        Collections.shuffle(_deck);
        //Collections.reverse(_deck);
    }

    public void shuffle() {
        Collections.shuffle(_deck);
    }

    /**
     * Draws a card from the deck.
     *
     * @return Card
     */
    public District drawCard()  {
        District card = _deck.get(0);
        _deck.remove(0);
        return card;
    }

    public District drawCard(int loc) {
        District card = _deck.get(loc);
        _deck.remove(loc);
        return card;
    }

    /**
     * Adds a card given to the bottom of the deck.
     *
     * @param card card to add
     */
    public void addToTheBottom(District card) {
        card.setBeautified(false);
        card.setRoundBuilt(0);

        _deck.add(card);
    }

    public void addToTheBottom(List<District> cards) {
        for (District card : cards) {
            this.addToTheBottom(card);
        }
    }

    public District peek(int i) {
        return _deck.get(i);
    }

    public void setGraveyardStop(District district) {
        if (district != null) {
            district.setBeautified(false);
            district.setRoundBuilt(0);
            district.getMuseumCards().clear();
        }

        _graveyardStop = district;
    }

    public District getGraveyardStop() {
        return _graveyardStop;
    }

    public List<District> getAllCards() {
        return _deck;
    }
}
