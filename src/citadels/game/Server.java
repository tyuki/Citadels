/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package citadels.game;

import citadels.game.Exceptions.GameException;
import citadels.CitadelsApp;
import citadels.ai.AIContainer;
import citadels.game.District.EFFECT;
import citadels.game.Exceptions.GameInProgressException;
import citadels.game.Exceptions.NotEnoughPlayerException;
import citadels.game.Exceptions.TooManyPlayersException;
import citadels.game.GameMessage.ARGC1_MESSAGE;
import citadels.game.GameMessage.COLOR_TYPE;
import java.awt.Color;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InterruptedIOException;
import java.io.PrintStream;
import java.net.BindException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.LinkedList;
import java.util.List;
import java.util.Observable;
import java.util.Observer;
import javax.swing.JOptionPane;
import javax.swing.text.SimpleAttributeSet;
import javax.swing.text.StyleConstants;
import network.Message;
import network.MessageConnection;

/**
 *
 * @author Personal
 */
public class Server implements Observer, Runnable {

    private static String INFO_NAME = "name";
    private static String INFO_ID = "id";
    
    private org.jdesktop.application.ResourceMap _resourceMap = org.jdesktop.application.Application.getInstance(citadels.CitadelsApp.class).getContext().getResourceMap(CitadelsApp.class);

    private List<MessageConnection> _connections = new LinkedList<MessageConnection>();

    private GameState _game = new GameState();

    private Thread _thread;
    private int _port;

    private boolean _bindException = false;
    private boolean _otherException = false;
    private boolean _isRunning = false;
    private boolean _isActive = false;

    private List<String> _connectedNames = new LinkedList<String>();

    public Server(int port) {
        _port = port;
    }

//    public Server(int port, boolean use9th, boolean shortGame, int[] expansionCards, EFFECT[] expansionDistricts) {
//        _port = port;
//        _game = new GameState(use9th, shortGame, expansionCards, expansionDistricts);
//        _resourceMap = org.jdesktop.application.Application.getInstance(citadels.CitadelsApp.class).getContext().getResourceMap(CitadelsApp.class);
//        _connectedNames = new LinkedList<String>();
//        _connections = new LinkedList<MessageConnection>();
//    }
//
//    public Server(int port, boolean use9th, boolean shortGame, int[] expansionCards, int randomCardNum) {
//        _port = port;
//        _game = new GameState(use9th, shortGame, expansionCards, randomCardNum);
//        _resourceMap = org.jdesktop.application.Application.getInstance(citadels.CitadelsApp.class).getContext().getResourceMap(CitadelsApp.class);
//        _connectedNames = new LinkedList<String>();
//        _connections = new LinkedList<MessageConnection>();
//    }

    public boolean start() {
        _thread = new Thread(this);
        _thread.start();
        try {
            Thread.sleep(500);
        } catch (InterruptedException ex) {
        }
        if (_bindException || _otherException) {
            return false;
        }

        return true;
    }

    public void stop() {
        _isActive = false;
        try {
            for (MessageConnection connection : _connections) {
                connection.close();
            }
        } catch (Exception e){}
        System.out.println("stopped'");
    }

    public void run() {
        try {
            ServerSocket socket = new ServerSocket(_port, 10);
            socket.setSoTimeout(2000);
            _isRunning = true;
            _isActive = true;
            while (_isActive) {
                try {
                    Socket connectionSocket = socket.accept();
                    MessageConnection connection = new MessageConnection(this, connectionSocket);
                    _connections.add(connection);
                } catch (InterruptedIOException iie) {
                    this.broadcast(new Message(Message.COMMAND_SETUP, Message.CHECKALIVE, 0));
                }
            }
        } catch (BindException be) {
            _bindException = true;
            JOptionPane.showMessageDialog(null, _resourceMap.getString("error.PortUsed"));
        } catch (IOException ex) {
            JOptionPane.showMessageDialog(null, _resourceMap.getString("error.OtherException") + "\n" + ex.getMessage());
            ex.printStackTrace();
        } finally {
            _isRunning = false;
        }
    }

    public boolean startGame(GameOptions options) {
        try {
            _game.startGame(options);
            sendGameMessage(GameMessage.singleArgMessage(ARGC1_MESSAGE.ROUND_START,  Integer.toString(_game.getProgress().round), COLOR_TYPE.DEFAULT, 0));
            broadcast(new Message(Message.COMMAND_GAME, Message.ENTIRE_GAME_STATE, _game));
            return true;
        } catch (NotEnoughPlayerException nep) {
//            JOptionPane.showMessageDialog(null, _resourceMap.getString("error.NotEnoughPlayer"));
            sendServerMessage(ServerMessage.singleArgMessage(ServerMessage.ARGC1_MESSAGE.ERROR, _resourceMap.getString("error.NotEnoughPlayer"), ServerMessage.COLOR_TYPE.ERROR));
            broadcast(new Message(Message.COMMAND_GAME, Message.ENTIRE_GAME_STATE, _game));
        } catch (GameException ge) {
//            JOptionPane.showMessageDialog(null, _resourceMap.getString("error.OtherException") + "\n" + ge.getMessage());
            sendServerMessage(ServerMessage.singleArgMessage(ServerMessage.ARGC1_MESSAGE.ERROR, _resourceMap.getString("error.OtherException") + "\n" + ge.getMessage(), ServerMessage.COLOR_TYPE.ERROR));
            broadcast(new Message(Message.COMMAND_GAME, Message.ENTIRE_GAME_STATE, _game));
        }
        return false;
            
    }

    public boolean isRunning() {
        return _isRunning;
    }

    public int getPort() {
        return _port;
    }

    public synchronized void update(Observable o, Object arg) {
        if (o instanceof MessageConnection && arg instanceof Message) {
            try {
                messageHandling((MessageConnection)o, (Message)arg);
            } catch (Exception e) {
                JOptionPane.showMessageDialog(null, _resourceMap.getString("error.OtherException") + "\n" + e.getMessage());
                e.printStackTrace();
                try {
                    File dump = new File("./dump" + System.currentTimeMillis());
                    FileOutputStream ofs = new FileOutputStream(dump);
                    e.printStackTrace(new PrintStream(ofs));
                    ofs.close();
                } catch (Exception ex) {}
            }
        }
    }


    private void messageHandling(MessageConnection connection, Message msg) {
        try {
            switch (msg.getCommandType()) {
                case Message.COMMAND_SETUP:
                    setupMessageHandling(connection, msg);
                    break;
                case Message.COMMAND_CHAT:
                    SimpleAttributeSet attr = null;
                    if (connection.getInfo(INFO_ID)!=null) {
                        attr = new SimpleAttributeSet();
                        StyleConstants.setForeground(attr, _game.getPlayer((Integer) connection.getInfo(INFO_ID)).getColor());
                    }
                    broadcast(new Message(Message.COMMAND_CHAT, Message.USER_MESSAGE, new Object[]{connection.getInfo(INFO_NAME) + ":" + msg.getItems()[0], attr}));
                    break;
                case Message.COMMAND_GAME:
                    gameMessageHandling(connection, msg);
                    break;
                case Message.COMMAND_ERROR:
                    break;
                default:
                    JOptionPane.showMessageDialog(null, _resourceMap.getString("error.UnhandledCommandType") + ":" + msg.getCommandType());
                    break;
            }
        } catch (IOException ioe) {
            sendServerMessage(handleDisconnectedConnection(connection));
            ioe.printStackTrace();
            try {
                File dump = new File("./dump" + System.currentTimeMillis());
                FileOutputStream ofs = new FileOutputStream(dump);
                ioe.printStackTrace(new PrintStream(ofs));
                ofs.close();
            } catch (FileNotFoundException ex) {
            } catch (Exception e) {}
        }
    }

    private void setupMessageHandling(MessageConnection connection, Message msg) throws IOException {
        switch (msg.getMessageType()) {
            //Connection Request
            case Message.CONNECT:
                boolean ack = false;
                String name = (String)msg.getItems()[0];
                //Check if name exists
                synchronized(this) {
                    if (!_connectedNames.contains(name)) {
                        ack = true;
                        _connectedNames.add(name);
                        connection.setInfo(INFO_NAME, name);

                    }
                }
                //Send the response back
                connection.write(new Message(Message.COMMAND_SETUP, Message.CONNECT, ack));
                //If the connection was accepted
                if (ack) {
                    sendServerMessage(ServerMessage.singleArgMessage(ServerMessage.ARGC1_MESSAGE.CONNECTED, name, ServerMessage.COLOR_TYPE.DEFAULT));
                    //send the current state of the game
                    if (_game != null) {
                        connection.write(new Message(Message.COMMAND_GAME, Message.ENTIRE_GAME_STATE, _game));
                    }
                } else {
                    connection.close();
                    _connections.remove(connection);
                }
                broadcast(new Message(Message.COMMAND_SETUP, Message.CONNECTED_NAMES, _connectedNames));
               break;
            case Message.JOIN:
                try {
                    //Attempt to add a player
                    int id = _game.addPlayer((String)connection.getInfo(INFO_NAME), (Boolean)msg.getItems()[0]);
                    //If successful, broad cast the result
                    sendServerMessage(ServerMessage.singleArgMessage(ServerMessage.ARGC1_MESSAGE.JOINED, (String)connection.getInfo(INFO_NAME), ServerMessage.COLOR_TYPE.DEFAULT));
                    //send the player ID
                    connection.write(new Message(Message.COMMAND_SETUP, Message.JOIN, id));
                    connection.setInfo(INFO_ID, id);
                    //Send the new game state
                    broadcast(new Message(Message.COMMAND_GAME, Message.ENTIRE_GAME_STATE, _game));
                } catch (GameInProgressException gip) {
                    connection.write(new Message(Message.COMMAND_ERROR, Message.ERROR_DIALOG, _resourceMap.getString("error.GameInProgress")));
                } catch (TooManyPlayersException ge) {
                    connection.write(new Message(Message.COMMAND_ERROR, Message.ERROR_DIALOG, _resourceMap.getString("error.TooManyPlayers")));
                }
                break;
            case Message.LEAVE:
                _connections.remove(connection);
                _connectedNames.remove((String)connection.getInfo(INFO_NAME));
                 int id = id = (Integer)msg.getItems()[0];
                sendServerMessage(_game.removePlayer(id));
                broadcast(new Message(Message.COMMAND_GAME, Message.ENTIRE_GAME_STATE, _game));
                broadcast(new Message(Message.COMMAND_SETUP, Message.CONNECTED_NAMES, _connectedNames));
                break;
            case Message.RESET:
                _game = new GameState();
                AIContainer.reset();
                broadcast(new Message(Message.COMMAND_SETUP, Message.RESET, _game));
                sendServerMessage(ServerMessage.singleArgMessage(ServerMessage.ARGC1_MESSAGE.RESET, (String)connection.getInfo(INFO_NAME), ServerMessage.COLOR_TYPE.DEFAULT));
                broadcast(new Message(Message.COMMAND_GAME, Message.ENTIRE_GAME_STATE, _game));
                break;
            case Message.START:
                startGame((GameOptions)msg.getItems()[0]);
                break;
            case Message.ADD_AI:
                AIContainer.addAI(getPort());
                break;
            case Message.REMOVE_AI:
                AIContainer.removeAI();
                break;
            default:
                JOptionPane.showMessageDialog(null, _resourceMap.getString("error.UnhandledServerMessageType") + ":" + msg.getMessageType());
                break;
        }
    }

    private void gameMessageHandling(MessageConnection connection, Message msg) throws IOException {

        int id;
        String gameMessage;
        
        switch (msg.getMessageType()) {
           //Character Selected
            case Message.SELECT_CHARACTER:
                id = (Integer)msg.getItems()[0];
                int selection = (Integer)msg.getItems()[1];
                sendGameMessage (_game.setCharacter(id, selection));
                break;
            //Action coins
            case Message.ACTION_GOLD:
                id = (Integer)msg.getItems()[0];
                sendGameMessage(_game.actionCoins(id));
                break;
            //Action cards
            case Message.ACTION_CARDS:
                id = (Integer)msg.getItems()[0];
                int[] actCards = (int[])msg.getItems()[1];
                sendGameMessage(_game.actionCards(id, actCards));
                break;
            //Turn end
            case Message.END_TURN:
                id = (Integer)msg.getItems()[0];
                //Take care of end of the turn/start of the turn
                sendGameMessage(_game.endTurn(id));
                break;
            //District Income
            case Message.DISTRICT_INCOME:
                id = (Integer)msg.getItems()[0];
                sendGameMessage(_game.earnDistrictIncome(id));
                break;
           //Build district
            case Message.BUILD_DISTRICT:
                id = (Integer)msg.getItems()[0];
                int districtNum = (Integer)msg.getItems()[1];
                sendGameMessage(_game.buildDistrict(id, districtNum));
                break;
            //Assassine ability
            case Message.SA_ASSASSIN:
                id = (Integer)msg.getItems()[0];
                int assassinTarget = (Integer)msg.getItems()[1];
                sendGameMessage (_game.assassinAnnounce(id, assassinTarget));
                break;
            //Thief ability
            case Message.SA_THIEF:
                id = (Integer)msg.getItems()[0];
                int thiefTarget = (Integer)msg.getItems()[1];
                sendGameMessage (_game.thiefAnnounce(id, thiefTarget));
                break;
            //Magician ability, swap with another player
            case Message.SA_MAGICIAN_SWAP:
                id = (Integer)msg.getItems()[0];
                int swapTarget = (Integer)msg.getItems()[1];
                sendGameMessage (_game.magicianAbilitySwap(id, swapTarget));
                break;
            //Magician ability, exchange card with the deck
            case Message.SA_MAGICIAN_EXCHANGE:
                id = (Integer)msg.getItems()[0];
                int[] discards = (int[])msg.getItems()[1];
                sendGameMessage (_game.magicianAbilityExchange(id, discards));
                break;
            //merchant ability
            case Message.SA_MERCHANT:
                id = (Integer)msg.getItems()[0];
                sendGameMessage (_game.merchantAbility(id));
                break;
            case Message.SA_WARLORD:
                id = (Integer)msg.getItems()[0];
                int WLtargetID = (Integer)msg.getItems()[1];
                int WLtargetDistrict = (Integer)msg.getItems()[2];
                sendGameMessage (_game.warlordAbility(id, WLtargetID, WLtargetDistrict));
                break;
            //Witch ability
            case Message.SA_WITCH:
                id = (Integer)msg.getItems()[0];
                int witchTarget = (Integer)msg.getItems()[1];
                sendGameMessage (_game.witchAnnounce(id, witchTarget));
                break;
            //Wizard ability
            case Message.SA_WIZARD:
                id = (Integer)msg.getItems()[0];
                int wizardTargetID = (Integer)msg.getItems()[1];
                int wizardTargetDistrict = (Integer)msg.getItems()[2];
                boolean wizardBuild = (Boolean)msg.getItems()[3];
                sendGameMessage (_game.wizardAbility(id, wizardTargetID, wizardTargetDistrict, wizardBuild));
                break;
            case Message.SA_EMPEROR_KING:
                id = (Integer)msg.getItems()[0];
                int newKing = (Integer)msg.getItems()[1];
                sendGameMessage (_game.emperorAbility(id, newKing));
                break;
            case Message.SA_EMPEROR_TRIBUTE_CARD:
                id = (Integer)msg.getItems()[0];
                int cardNum = (Integer)msg.getItems()[1];
                sendGameMessage (_game.tributeToEmperorCard(id, cardNum));
                break;
            case Message.SA_EMPEROR_TRIBUTE_GOLD:
                id = (Integer)msg.getItems()[0];
                sendGameMessage (_game.tributeToEmperorGold(id));
                break;
            //Abbot ability
            case Message.SA_ABBOT:
                id = (Integer)msg.getItems()[0];
                sendGameMessage (_game.abbotAbility(id));
                break;
            case Message.SA_NAVIGATOR_GOLD:
                id = (Integer)msg.getItems()[0];
                sendGameMessage (_game.navigatorAbilityGold(id));
                break;
            case Message.SA_NAVIGATOR_CARDS:
                id = (Integer)msg.getItems()[0];
                sendGameMessage (_game.navigatorAbilityCards(id));
                break;
            case Message.SA_DIPLOMAT:
                id = (Integer)msg.getItems()[0];
                int districtIndex = (Integer)msg.getItems()[1];
                int otherPlayerID = (Integer)msg.getItems()[2];
                int otherDistrictIndex = (Integer)msg.getItems()[3];
                sendGameMessage (_game.diplomatAbility(id, districtIndex, otherPlayerID, otherDistrictIndex));
                break;
            case Message.SA_ARTIST:
                id = (Integer)msg.getItems()[0];
                int beautifyTarget = (Integer)msg.getItems()[1];
                sendGameMessage (_game.artistAbility(id, beautifyTarget));
                break;
            case Message.DA_SMITHY:
                id = (Integer)msg.getItems()[0];
                sendGameMessage (_game.smithyAbility(id));
                break;
            case Message.DA_LABORATORY:
                id = (Integer)msg.getItems()[0];
                int discard = (Integer)msg.getItems()[1];
                sendGameMessage (_game.laboratoryAbility(id, discard));
                break;
            case Message.DA_GRAVEYARD:
                id = (Integer)msg.getItems()[0];
                boolean use = (Boolean)msg.getItems()[1];
                sendGameMessage (_game.graveyardAbility(id, use));
                break;
            case Message.DA_LIGHTHOUSE:
                id = (Integer)msg.getItems()[0];
                int buildNum = (Integer)msg.getItems()[1];
                int takeNum = (Integer)msg.getItems()[2];
                sendGameMessage (_game.lighthouseAbility(id, buildNum, takeNum));
                break;
            case Message.DA_MUSEUM:
                id = (Integer)msg.getItems()[0];
                int museum = (Integer)msg.getItems()[1];
                int discardM = (Integer)msg.getItems()[2];
                sendGameMessage (_game.museumAbility(id, museum, discardM));
                break;
            case Message.DA_ARMORY:
                id = (Integer)msg.getItems()[0];
                int armTargetID = (Integer)msg.getItems()[1];
                int armTargetDistrict = (Integer)msg.getItems()[2];
                sendGameMessage (_game.armoryAbility(id, armTargetID, armTargetDistrict));
                break;
            case Message.DA_BELLTOWER:
                id = (Integer)msg.getItems()[0];
                int buildB = (Integer)msg.getItems()[1];
                sendGameMessage (_game.belltowerAbility(id, buildB));
                break;
            default:
                JOptionPane.showMessageDialog(null, _resourceMap.getString("error.UnhandledServerMessageType") + ":" + msg.getMessageType());
                break;
        }
        broadcast(new Message(Message.COMMAND_GAME, Message.ENTIRE_GAME_STATE, _game));
    }

    private synchronized void broadcast(Message msg) {
        List<MessageConnection> disconnected = new LinkedList<MessageConnection>();
        for (MessageConnection connection : _connections) {
            try {
                connection.write(msg);
            } catch (Exception e) {
                disconnected.add(connection);
            }
        }

        //Remove later
        List<ServerMessage> messages = new LinkedList<ServerMessage>();
        for (MessageConnection con : disconnected) {
                messages.add(handleDisconnectedConnection(con));
        }

        //Send all messages after disconnect has been handled
        for (ServerMessage message : messages) {
            sendServerMessage(message);
        }

    }

    private void sendGameMessage(List<GameMessage> msgs) {
        if (msgs == null) return;
        for (GameMessage msg : msgs) {
            sendGameMessage(msg);
        }
    }

    private void sendGameMessage(GameMessage message) {
        if (message != null) {
            SimpleAttributeSet attr = new SimpleAttributeSet();
            StyleConstants.setFontSize(attr, 12);
            switch (message.colorType) {
                case DEFAULT:
                    broadcast(new Message(Message.COMMAND_CHAT, Message.GAME_MESSAGE, new Object[]{message.string, attr}));
                    break;
                case PLAYER:
                    StyleConstants.setForeground(attr, _game.getPlayer(message.colorID).getColor());
                    broadcast(new Message(Message.COMMAND_CHAT, Message.GAME_MESSAGE, new Object[]{message.string, attr}));
                    break;
                case EMPHASIZE:
                    StyleConstants.setBold(attr, true);
                    broadcast(new Message(Message.COMMAND_CHAT, Message.GAME_MESSAGE, new Object[]{message.string, attr}));
                    break;
                default:
                    broadcast(new Message(Message.COMMAND_CHAT, Message.GAME_MESSAGE, new Object[]{message.string, attr}));
                    break;
            }
        }
    }
    
    private void sendServerMessage(ServerMessage message) {
        if (message != null) {
            SimpleAttributeSet attr = null;
            switch (message.colorType) {
                case DEFAULT:
                    broadcast(new Message(Message.COMMAND_CHAT, Message.GAME_MESSAGE, new Object[]{message.string, null}));
                    break;
                case EMPHASIZE:
                    attr = new SimpleAttributeSet();
                    StyleConstants.setBold(attr, true);
                    broadcast(new Message(Message.COMMAND_CHAT, Message.GAME_MESSAGE, new Object[]{message.string, attr}));
                    break;
                case ERROR:
                    attr = new SimpleAttributeSet();
                    StyleConstants.setBold(attr, true);
                    StyleConstants.setForeground(attr, Color.RED);
                    broadcast(new Message(Message.COMMAND_CHAT, Message.GAME_MESSAGE, new Object[]{message.string, attr}));
                    break;
            }
        }
    }

    private synchronized ServerMessage handleDisconnectedConnection(MessageConnection connection) {
        String name = (String)connection.getInfo(INFO_NAME);
        if (name != null) {
            _connectedNames.remove(name);
        }
        connection.close();
        _connections.remove(connection);

        broadcast(new Message(Message.COMMAND_SETUP, Message.CONNECTED_NAMES, _connectedNames));
        //Send message
        return ServerMessage.singleArgMessage(ServerMessage.ARGC1_MESSAGE.DISCONNECTED, name, ServerMessage.COLOR_TYPE.ERROR);
    }
}
