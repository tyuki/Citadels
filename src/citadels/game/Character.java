/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package citadels.game;

import citadels.game.Exceptions.GameException;
import citadels.game.District.DISTRICT_COLOR;
import java.io.Serializable;

/**
 *
 * @author Personal
 */
public class Character implements Serializable {

    public static enum CHARACTER {
        ASSASSIN(1), THIEF(2), MAGICIAN(3), KING(4), BISHOP(5), MERCHANT(6), ARCHITECT(7), WARLORD(8), QUEEN(9),
        WITCH(1), TAX_COLLECTOR(2), WIZARD(3), EMPEROR(4), ABBOT(5), ALCHEMIST(6), NAVIGATOR(7), DIPLOMAT(8), ARTIST(9);

        public final int order;
        
        CHARACTER(int order) {
            this.order = order;
        }
    }

//    public static final int ASSASSIN_ORDER = 1;
//    public static final int WITCH_ORDER = 1;
//    public static final int THIEF_ORDER = 2;
//    public static final int TAX_COLLECTOR_ORDER = 2;
//    public static final int MAGICIAN_ORDER = 3;
//    public static final int WIZARD_ORDER = 3;
//    public static final int KING_ORDER = 4;
//    public static final int EMPEROR_ORDER = 4;
//    public static final int BISHOP_ORDER = 5;
//    public static final int ABBOT_ORDER = 5;
//    public static final int MERCHANT_ORDER = 6;
//    public static final int ALCHEMIST_ORDER = 6;
//    public static final int ARCHITECT_ORDER = 7;
//    public static final int NAVIGATOR_ORDER = 7;
//    public static final int WARLORD_ORDER = 8;
//    public static final int DIPLOMAT_ORDER = 8;
//    public static final int QUEEN_ORDER = 9;
//    public static final int ARTIST_ORDER = 9;

    public final int order;
    public final CHARACTER character;
    public final DISTRICT_COLOR color;
    public final String name;
    public final String cardName;

    public static Character initialize(int num, boolean expansion) throws GameException {
        switch(num) {
            case 1:
                if (!expansion) return new Character(CHARACTER.ASSASSIN, "assassin");
                if (expansion) return new Character(CHARACTER.WITCH, "witch");
                break;
            case 2:
                if (!expansion) return new Character(CHARACTER.THIEF, "thief");
                if (expansion) return new Character(CHARACTER.TAX_COLLECTOR, "taxcollector");
                break;
            case 3:
                if (!expansion) return new Character(CHARACTER.MAGICIAN, "magician");
                if (expansion) return new Character(CHARACTER.WIZARD, "wizard");
                break;
            case 4:
                if (!expansion) return new Character(CHARACTER.KING, "king");
                if (expansion) return new Character(CHARACTER.EMPEROR, "emperor");
                break;
            case 5:
                if (!expansion) return new Character(CHARACTER.BISHOP, "bishop");
                if (expansion) return new Character(CHARACTER.ABBOT, "abbot");
                break;
            case 6:
                if (!expansion) return new Character(CHARACTER.MERCHANT, "merchant");
                if (expansion) return new Character(CHARACTER.ALCHEMIST, "alchemist");
                break;
            case 7:
                if (!expansion) return new Character(CHARACTER.ARCHITECT, "architect");
                if (expansion) return new Character(CHARACTER.NAVIGATOR, "navigator");
                break;
            case 8:
                if (!expansion) return new Character(CHARACTER.WARLORD, "warlord");
                if (expansion) return new Character(CHARACTER.DIPLOMAT, "diplomat");
                break;
            case 9:
                if (!expansion) return new Character(CHARACTER.QUEEN, "queen");
                if (expansion) return new Character(CHARACTER.ARTIST, "artist");
                break;

        }
        throw new GameException("Invalid character");
    }
    
    public Character(CHARACTER character, String name) {
        this.character = character;
        this.name = name;
        cardName = name + "_card";

        switch (character) {
            case ASSASSIN:
            case WITCH:
                color = DISTRICT_COLOR.NONE;
                break;
            case THIEF:
            case TAX_COLLECTOR:
                color = DISTRICT_COLOR.NONE;
                break;
            case MAGICIAN:
            case WIZARD:
                color = DISTRICT_COLOR.NONE;
                break;
            case KING:
            case EMPEROR:
                color = DISTRICT_COLOR.YELLOW;
                break;
            case BISHOP:
            case ABBOT:
                color = DISTRICT_COLOR.BLUE;
                break;
            case MERCHANT:
                color = DISTRICT_COLOR.GREEN;
                break;
            case ALCHEMIST:
                color = DISTRICT_COLOR.NONE;
                break;
            case ARCHITECT:
            case NAVIGATOR:
                color = DISTRICT_COLOR.NONE;
                break;
            case WARLORD:
            case DIPLOMAT:
                color = DISTRICT_COLOR.RED;
                break;
            case QUEEN:
            case ARTIST:
                color = DISTRICT_COLOR.NONE;
                break;
            default:
                color = DISTRICT_COLOR.NONE;
                break;

        }

        order = character.order;
    }

}
