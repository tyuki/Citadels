/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package sound;

import java.io.File;
import java.io.IOException;

import javax.sound.sampled.*;

public class SoundPlayer implements LineListener {

    private Clip _clip;

    public SoundPlayer(File file) throws UnsupportedAudioFileException, IOException {
        AudioInputStream input = null;
        try {
            //open wav
            input = AudioSystem.getAudioInputStream(file);
            DataLine.Info info = new DataLine.Info(Clip.class, input.getFormat());
            //create clip
            _clip = (Clip) AudioSystem.getLine(info);
            _clip.addLineListener(this);
            _clip.open(input);
            input.close();
        } catch (LineUnavailableException ex) {
            ex.printStackTrace();
           input.close();
        } catch (IOException ex) {
            ex.printStackTrace();
           input.close();
           throw ex;
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void play(float volume) {
        if (volume < 1.0f) {
            FloatControl control = (FloatControl)_clip.getControl(FloatControl.Type.MASTER_GAIN);
            control.setValue((float)Math.log10(volume) * 20);
        }
        _clip.start();
    }

    public void update(LineEvent event) {
        if (event.getType() == LineEvent.Type.STOP) {
            Clip clip = (Clip) event.getSource();
            clip.stop();
            clip.setFramePosition(0);
        }
    }


}

