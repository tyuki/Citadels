/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package sound;


import java.io.*;
import java.util.HashMap;

public class SoundContainer {

	private static HashMap<Object, SoundPlayer> _files = new HashMap<Object, SoundPlayer>();

	public static void addFile(Object key, String file) {
		try {
			File soundFile = new File(file);
			SoundPlayer sound = new SoundPlayer(soundFile);
			_files.put(key, sound);
		} catch (Exception fnfe) {
                    fnfe.printStackTrace();
		}
	}

	public static void playFile(Object key, float volume) {
            try {
		if (_files.containsKey(key)) {
			((SoundPlayer)_files.get(key)).play(volume);
		}
            } catch (Exception e) {e.printStackTrace();}
	}

}

