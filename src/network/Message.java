package network;

import java.io.Serializable;
import java.util.Collection;

public class Message implements Serializable {

    public static final int COMMAND_SETUP = 1;
    public static final int COMMAND_CHAT = 2;
    public static final int COMMAND_GAME = 3;
    public static final int COMMAND_ERROR = 4;

    public static final int CONNECT = 100;
    public static final int JOIN = 110;
    public static final int LEAVE = 111;
    public static final int RESET = 112;
    public static final int START = 113;
    public static final int ADD_AI = 114;
    public static final int REMOVE_AI = 115;
    public static final int CHECKALIVE = 120;
    public static final int CONNECTED_NAMES = 130;

    public static final int SERVER_MESSAGE = 200;
    public static final int GAME_MESSAGE = 201;
    public static final int USER_MESSAGE = 202;

    public static final int ENTIRE_GAME_STATE = 300;

    public static final int SELECT_CHARACTER = 310;

    public static final int ACTION_GOLD = 320;
    public static final int ACTION_CARDS = 321;
    public static final int BUILD_DISTRICT = 322;
    public static final int DISTRICT_INCOME = 323;

    public static final int SA_ASSASSIN = 330;
    public static final int SA_THIEF = 331;
    public static final int SA_MAGICIAN_SWAP = 332;
    public static final int SA_MAGICIAN_EXCHANGE = 333;
    public static final int SA_MERCHANT = 334;
    public static final int SA_ARCHITECT = 335;
    public static final int SA_WARLORD = 336;
    public static final int SA_QUEEN = 337;

    public static final int SA_WITCH = 340;
    public static final int SA_WIZARD = 341;
    public static final int SA_EMPEROR_KING = 342;
    public static final int SA_EMPEROR_TRIBUTE_CARD = 343;
    public static final int SA_EMPEROR_TRIBUTE_GOLD = 344;
    public static final int SA_ABBOT = 345;
    public static final int SA_NAVIGATOR_GOLD = 346;
    public static final int SA_NAVIGATOR_CARDS = 347;
    public static final int SA_DIPLOMAT = 348;
    public static final int SA_ARTIST = 349;

    public static final int DA_SMITHY = 350;
    public static final int DA_LABORATORY = 351;
    public static final int DA_GRAVEYARD = 352;
    public static final int DA_LIGHTHOUSE = 353;
    public static final int DA_MUSEUM = 354;
    public static final int DA_ARMORY = 355;
    public static final int DA_BELLTOWER = 356;

    public static final int END_TURN = 360;

    public static final int ERROR_DIALOG = 400;

    //Typet
    private int _commandType; //Connection Type
    private int _messageType; //Message Type
    private long _time;
    private Object[] _items;

    public Message (int type, int type2, Object item) {
            _commandType = type;
            _messageType = type2;
            _items = new Object[]{item};
            _time = System.currentTimeMillis();
    }

    public Message (int type, int type2, Object[] items) {
            _commandType = type;
            _messageType = type2;
            _items = items;
            _time = System.currentTimeMillis();
    }

    public Message (int type, int type2, Collection<Object> collection) {
            _commandType = type;
            _messageType = type2;
            _items = collection.toArray();
            _time = System.currentTimeMillis();
    }

    public int getCommandType() {
            return _commandType;
    }

    public int getMessageType() {
            return _messageType;
    }

    public Object[] getItems() {
            return _items;
    }

    public int getLength() {
            return _items.length;
    }

    public long getTime() {
            return _time;
    }

    public void setTime(long time) {
            _time = time;
    }

    public void setItems(Object[] items) {
            _items = items;
    }

    public void setItems(Collection<Object> items) {
            _items = items.toArray();
    }

    @Override
    public String toString() {
            String items = "";
            for (int i = 0; i < _items.length; i++) {
                    items += _items[i] + " ";
            }
            return "Type: " + _commandType + _messageType + " items  " + items;
    }
}