package network;
import java.io.*;
import java.net.*;
import java.util.HashMap;
import java.util.Observable;
import java.util.Observer;



public class MessageConnection extends Observable implements Runnable {
	
	public static final String CONNECTION_ESTABLISHED = "TCPConnection:Connection Established";
	public static final String CONNECTION_CLOSED      = "TCPConnection:Connection Closed";
	public static final String CONNECTION_EXCEPTION      = "TCPConnection:Connection Exception";
	public static final int CONNECTION_TIMEOUT = 5000;
	
	protected boolean _isStopping = false;
	protected Socket _connection;
	protected ObjectOutputStream _output;
	protected ObjectInputStream _input;
	protected Thread _thread = null;
	protected HashMap<String, Object> _connectionInfo;
	
	/**
	 * Constructor with observer and host:port.
	 * 
	 * @param observer Observer that should be notified when a message is received.
	 * @param host hostname/IP of the destination
	 * @param port port of the destination to connect
	 * @throws UnknownHostException
	 * @throws IOException
	 */
	public MessageConnection (Observer observer, String host, int port) throws UnknownHostException, IOException {
		_connection = new Socket();
		InetSocketAddress endpoint = new InetSocketAddress(host, port);
		_connection.connect(endpoint, CONNECTION_TIMEOUT);
		startUp(observer);
	}
	
	/**
	 * Constructor using existing connection
	 * 
	 * @param observer Observer that should be notified when a message is received.
	 * @param connection existing TCP socket connection
	 * @throws IOException
	 */
	public MessageConnection (Observer observer, Socket connection) throws IOException {
		_connection = connection;
		startUp(observer);
	}

	public void run() {
		setChanged();
		notifyObservers(CONNECTION_ESTABLISHED);
		while (!_isStopping) {
			try {
				Object msg = read();
				if (msg != null) {
					setChanged();
					notifyObservers(msg);
				}
			} catch (EOFException eof) {
				close();
			} catch (SocketException sce) {
				_isStopping = true;
			} catch (IOException ex) {
				ex.printStackTrace();
				setChanged();
				notifyObservers(CONNECTION_EXCEPTION);
				cleanUp();
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		}
		setChanged();
		notifyObservers(CONNECTION_CLOSED);
	}
	
	public void close() {
		try {
			_output.flush();
		} catch (IOException e) {
		} catch (NullPointerException npe) {
		}
		_isStopping = true;
		cleanUp();
	}
	
	public InetAddress getInetAddress() {
		if (_connection != null) {
			return _connection.getInetAddress();
		}
		
		return null;
		
	}
	
	public String getHost() {
		return _connection.getInetAddress().getHostName()+":"+_connection.getPort();
	}
	
	public void setInfo(String name, Object item) {
		_connectionInfo.put(name, item);
	}
	
	public Object getInfo(String name) {
		return _connectionInfo.get(name);
	}
	
	public Message read() throws IOException {
		if (_connection == null) return null;

		try {
			//if (_input == null && _connection != null) {
			if (_input == null && _connection != null) {
				_input = new ObjectInputStream(_connection.getInputStream());
			}
			return (Message) _input.readObject();
		} catch (ClassNotFoundException e) {
                    e.printStackTrace();
                    return null;
		} catch (ClassCastException e) {
                    e.printStackTrace();
                    return null;
                } catch (InvalidObjectException e) {
                    e.printStackTrace();
                    return null;
                }
	}
	
	public void write(Message msg) throws IOException {
		if (_output == null) {
			_output = new ObjectOutputStream(_connection.getOutputStream());
		}
		_output.writeObject((Object)msg);
                _output.flush();
                _output.reset();
	}
	

	private void startUp(Observer observer) throws IOException {
		_connectionInfo = new HashMap<String, Object>();
		if (observer != null) {
			this.addObserver(observer);
		}
	    _thread = new Thread(this);
	    _thread.start();
	}
	
	private void cleanUp() {
		try {
			if (_connection != null) {
				_connection.close();
				_connection = null;
			}
			if (_output != null) {
				_output.close();
				_output = null;
			}
			if (_input != null) {
				_input.close();
				_input = null;
			}
		} catch (IOException ioe) {}
	}
	
    @Override
    public String toString() {
            if (_connection == null) {
                    return "not connected";
            }
            return _connection.getInetAddress().getHostAddress() + "(" + _connection.getInetAddress().getHostName() + ")";
    }
}
