/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package network;

import citadels.CitadelsApp;
import java.io.IOException;
import java.net.ConnectException;
import java.net.UnknownHostException;
import java.util.Observer;
import javax.swing.JOptionPane;

/**
 *
 * @author Personal
 */
public class Client {

    private org.jdesktop.application.ResourceMap _resourceMap;

    private Observer _observer;
    private String _host;
    private int _port;
    private MessageConnection _connection;
    private boolean _connected = false;

    public Client(Observer observer, String hostname, int port) {
        _observer = observer;
        _host = hostname;
        _port = port;
        _resourceMap = org.jdesktop.application.Application.getInstance(citadels.CitadelsApp.class).getContext().getResourceMap(CitadelsApp.class);
    }

    public boolean connect() {
        try {
            _connection = new MessageConnection(_observer, _host, _port);
            _connected = true;
        } catch (UnknownHostException ex) {
            JOptionPane.showMessageDialog(null, _resourceMap.getString("error.UnknownHost"));
            return false;
        } catch (ConnectException ce) {
            JOptionPane.showMessageDialog(null, _resourceMap.getString("error.ConnectException"));
            return false;
        } catch (IOException ex) {
            JOptionPane.showMessageDialog(null, _resourceMap.getString("error.OtherException") + "\n" + ex.getMessage());
            ex.printStackTrace();
            return false;
        }
        return true;
    }

    public void disconnect() {
        _connection.close();
        _connected = false;
    }

    public void write(Message msg) throws IOException {
        if (_connected) {
            _connection.write(msg);
        }
    }

    public boolean isConnected() {
        return _connected;
    }



}
