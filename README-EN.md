# Citadels

Citadels is a game by Bruno Faidutti. Players compete with each other trying to finish their own stronghold - citadel - before others. The unique game play is built around various characters with special abilities. Each player takes turn selecting a character to play, only giving partial information about the choice of characters to others. The complex and interesting game play comes from guessing and bluffing the changing roles.

The Java implementation is based on the original game plus its Dark City expansion. Everything except one district card that doesn't make sense for online play is implemented.

### Requirements
Java Runtime Environment 1.8 or higher

### Install/Build
Pre-built distribution: simply double click on Citadels.jar to start the game.
Latest version: https://gitlab.com/tyuki/Citadels/tags/v1.02

If you want to build yourself, you should be able to open the top-level directory as a project in Net Beans. In fact, NetBeans IDE discontinued the GUI development environment that was used to make this app. You will have to use NetBeans 7 to edit the GUI. 

### How to Play

The intended audience is those who are already familiar with the game. Please refer to other sources for rules of the game.

#### Interface

* Once the game has started, go to the top-left menu and select 'Start Sever' or 'Connect to Server'. One of the players must host the game.
* After all players have joined the game, the host can select 'Start Game' from the menu. If you want to add AI players, you can also do that from the menu before your start.
* If you lose connection in the middle, you should be able to get back to the game by connecting again with the same player name.
* Most of the controls are through clicking cards, districts, or buttons that show up in the bottom-right.
* The expansion cards/rules can be selected as you start the server.

#### Details

* All discards go to the bottom of the deck.
* The crown still moves even if the King is assassinated.
* When Diplomat is in play, Graveyard is removed from the deck.
* Tax Collector effect happens as the first event after a player declares the end of his/her turn. This means that it happens before Alchemist or Poor House takes effect.
* Queen is useless when King is replaced by Emperor, and thus is not a recommended combination.
* Hospital is useless when Assassin is replaced by Witch. it will excluded from random selection in this case.

### Notes

I implemented Citadels to test GUI development in NetBeans IDE before working on a more complex game ([Ursuppe](https://gitlab.com/tyuki/Ursuppe)). It does work pretty well as a tool to play online with your friends that already know the original game. However, I don't have any intention to work on this project aside from simple bug fixes. 

The motivation for me to implement Citadels was to play with my friends living in different countries. I decided to make it available on gitlab as I was cleaning up my old projects (this code comes from 2010 or so.)

The application supports English and Japanese. It should switch to Japanese when the locale is JP. If someone wants to add support for different languages, it should not be too difficult: the property files in ''citadels.resources'' is where the texts are defined.

### License
MIT

Source code is made publicly available at GitLab:
  https://gitlab.com/tyuki/Citadels

